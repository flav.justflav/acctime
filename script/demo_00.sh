clear
# demo acctime

# Start clock ref
gkrellm --geometry +3006+430 &

# Load the module
insmod kacctime_x86_64-LNX418.ko kacctime_log_mode=1

# Creat cgroup for demo
mkdir /sys/fs/cgroup/systemd/1_acc

# Add the cgroup in timespace 3 (for demo)
echo -n "ADD 3 1_acc" > /sys/kernel/debug/kacctime/acc_cgroup

# Check
cat /sys/kernel/debug/kacctime/acc_cgroup 

cat /sys/kernel/debug/kacctime/ts?

# Start clock time space 3
(echo $BASHPID > /sys/fs/cgroup/systemd/1_acc/tasks ; gkrellm --geometry +3006+500) &

# Change speed of time :
echo "0,5,1" > /sys/kernel/debug/kacctime/ts3

# Check
cat /sys/kernel/debug/kacctime/ts?

# test sleep
date ; FLAV_TIME=3 sleep 5 ; date 

# test date
date ; FLAV_TIME=3 date

# reset time space
echo "0,0,0" > /sys/kernel/debug/kacctime/ts3

# shift date
echo "3600,5,1" > /sys/kernel/debug/kacctime/ts3

# Change speed
echo "0,10,1" > /sys/kernel/debug/kacctime/ts3
## pause 2

echo "0,60,1" > /sys/kernel/debug/kacctime/ts3
## pause 2

echo "0,120,1" > /sys/kernel/debug/kacctime/ts3
## pause 2

echo "0,1,1" > /sys/kernel/debug/kacctime/ts3
## pause 2

# move cgroup
mkdir /sys/fs/cgroup/systemd/2_acc
echo -n "ADD 2 2_acc" > /sys/kernel/debug/kacctime/acc_cgroup

# change speed ts2
echo "14400,5,1" > /sys/kernel/debug/kacctime/ts2

# Check
cat /sys/kernel/debug/kacctime/ts?

# move clock to ts2
tail -n 1 /sys/fs/cgroup/systemd/1_acc/tasks > /sys/fs/cgroup/systemd/2_acc/tasks

## pause 2

# unload
rmmod kacctime_x86_64_LNX418

# kill clock
ps aux | grep gkrellm | grep -v grep | awk '{print $2}' | xargs kill

# remove cgroup
rmdir /sys/fs/cgroup/systemd/2_acc
rmdir /sys/fs/cgroup/systemd/1_acc
