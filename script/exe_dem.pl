#!/usr/bin/perl

use strict;
use warnings;
use Time::HiRes;
use Term::ANSIColor;

my $file = $ARGV[0] || "./demo_00.sh";

my $ps = color("cyan") . "acctime# " . color("reset");
my $rate0 = 50000;
my $rate1 = 10 * $rate0;

open(DEMF, $file) or die "$0: $!\n";
my @cmd = <DEMF>;
close(DEMF);

$| = 1;

for my $line (@cmd) {
    chomp($line);

    if ($line eq "") {
	print "\n";
	sleep(1);
	next;
    }

    if ($line =~ m/^## pause (\d+)/) {
	sleep (0+$1);
	next;
    }

    if ($line =~ m/^## stop/) {
	exit;
    }
    
    if (substr($line, 0, 2) eq "# ") {
	aff_type($line, 0, "yellow");
	next;
    }
    exe_cmd($line);
}

sub exe_cmd
{
    my $line = shift;

    aff_type($line, $rate0, undef, $ps);
    system("$line");
}

sub aff_type
{
    my $line = shift;
    my $rate = shift || $rate1;
    my $color = shift || "reset";
    my $prefix = shift || '';

    my @phrase = split('', $line);

    print $prefix;
    print color($color);
    for my $mot (@phrase) {
	print $mot . "";
	Time::HiRes::usleep($rate0 * (.2+rand(3)));
    }
    print color("reset");
    print "\n";
    Time::HiRes::usleep($rate);
}
