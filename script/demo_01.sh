clear
# demo acctime statx 

# Start clock ref
gkrellm --geometry +3006+430 &

# Load the module
# add kacctime_open_dirbase parameter : all file in this directory
# will be affected by the module via statx syscall

insmod kacctime_x86_64-LNX418.ko kacctime_log_mode=1 kacctime_open_dirbase=/tmp/tst_statx

# setup time space :
# ts1 : forward 10 min in future
echo -n "600,1,1" > /sys/kernel/debug/kacctime/ts1

FLAV_TIME=1 date ; date 


# ts1 : backward 2h in past
echo -n "-7200,1,1" > /sys/kernel/debug/kacctime/ts2

FLAV_TIME=2 date ; date 


# create file:
FLAV_TIME=2 touch /tmp/tst_statx/D

FLAV_TIME=1 touch /tmp/tst_statx/C
## pause 2

FLAV_TIME=1 touch /tmp/tst_statx/D

FLAV_TIME=2 touch /tmp/tst_statx/C
## pause 2


# listing
 ls -l /tmp/tst_statx/

# nothing is affected
## pause 2

# check
FLAV_TIME=1 stat  /tmp/tst_statx/D
FLAV_TIME=2 stat  /tmp/tst_statx/D

## pause 2

FLAV_TIME=1 stat  /tmp/tst_statx/C
FLAV_TIME=2 stat  /tmp/tst_statx/C
## pause 4

# kill clock
ps aux | grep gkrellm | grep -v grep | awk '{print $2}' | xargs kill

# unload
rmmod kacctime_x86_64_LNX418
