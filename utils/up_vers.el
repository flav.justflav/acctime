(defun up_vers ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (re-search-forward "\[\[:digit:\]\]\\{4\\}-\[\[:digit:\]\]\\{2\\}-\[\[:digit:\]\]\\{2\\}\[\[:blank:\]\]\[\[:digit:\]\]\\{2\\}:\[\[:digit:\]\]\\{2\\}:\[\[:digit:\]\]\\{2\\}" nil t)
    (replace-match
     (format-time-string "%Y-%m-%d %H:%M:%S");")
     )
    )
  (save-buffer)
  )
