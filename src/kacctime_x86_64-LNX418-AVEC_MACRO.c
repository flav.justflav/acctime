/**
 * kacctime module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <fastraud@itsgroup.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018-     Luc Plisson <lplisson@itsgroup.fr>
 *
 * 2022-09-12 : v.81 -> lnx 4.18 / for DollarU
*/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/debugfs.h>
#include <linux/slab.h>

MODULE_VERSION("418 2022-09-12 15:07:34 .81");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <flav@itsgroup.com>");
MODULE_DESCRIPTION("AccTime LKM");

#define KACCTIME_DEBUGFS_ROOT	"kacctime"
#define KACCTIME_SYSCALL_SYM	"sys_call_table"


#define CHAR_END_STR	'\0'
#define CHAR_EQUAL	'='


#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

/* syscall tables pointers / proto syscall*/
typedef asmlinkage long (*sys_call_ptr_t)(const struct pt_regs *);
static sys_call_ptr_t *kacctime_syscall_table;

#define	KACCTIME_MAX_SPACE	1

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
	struct timespec	mono_delta;
};

static struct kacctime_timespace kacctime_timespace_tab[KACCTIME_MAX_SPACE];


sys_call_ptr_t ori_nanosleep;
sys_call_ptr_t ori_clock_gettime;
sys_call_ptr_t ori_gettimeofday;
sys_call_ptr_t ori_time;
sys_call_ptr_t ori_alarm;

/* Log */
static long     kacctime_log_mode = 0;
module_param(kacctime_log_mode, long, 0660);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/
#define TMP_BUF_MAX_SIZE        1024

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
static __inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}
/* fin log */


/* macro de calcul du temps */

#define kacctime_timespec_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) + \
			(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec); \
		fl_res->tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res->tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) - \
			(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec); \
		fl_res->tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res->tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_mul(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * f; \
		fl_res->tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res->tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_div(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
	  								\
		inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) / f; \
		fl_res->tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res->tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) \
			* n / d;					\
		fl_res->tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res->tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) + \
			(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec); \
		fl_res->tv_usec = inter % USEC_PER_SEC;			\
		fl_res->tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) - \
			(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec); \
		fl_res->tv_usec = inter % USEC_PER_SEC;			\
		fl_res->tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_mul(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * f; \
		fl_res->tv_usec = inter % USEC_PER_SEC;			\
		fl_res->tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_div(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) / f; \
		fl_res->tv_usec = inter % USEC_PER_SEC;			\
		fl_res->tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec)	\
			* n / d;					\
		fl_res->tv_usec = inter % USEC_PER_SEC;			\
		fl_res->tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

/* fin macro tmps */

/* time space param */
static long ts0[4] = {
	0, /* ref time */
	0, /* decalage */
	0, /* multi */
	0  /* div */
};

static int arr_argc = 0;

module_param_array(ts0, long, &arr_argc, 0660); 
MODULE_PARM_DESC(ts0, "ref,dec,mul,div");
/* fin time space param */

/** kacctime_seek_timespace - 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 */

static __inline__ void read_param(void) {
	int i = 0;
	kacctime_timespace_tab[i].ref.tv_sec = ts0[0];
	kacctime_timespace_tab[i].ref.tv_nsec = 0;
	kacctime_timespace_tab[i].dec.tv_sec = ts0[1];
	kacctime_timespace_tab[i].dec.tv_nsec = 0;
	kacctime_timespace_tab[i].multi = ts0[2];
	kacctime_timespace_tab[i].divid = ts0[3];
}

static int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;

	read_param();

	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_level(0, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_level(0, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
		  
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_level(0, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}
				if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
					kacctime_log_level(0, "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_level(5, "space -> [%d]\n", space);
				kfree(buf);
				if (kacctime_timespace_tab[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	if (kacctime_timespace_tab[0].multi == 0)    
		return (-1);
	return (-1);
}

/**
   fin  recherche timespace 
*/

/* debugfs */
struct dentry *kacctime_debugfs = NULL;

static void kacctime_mk_debugfs(void)
{
	kacctime_debugfs = debugfs_create_dir(KACCTIME_DEBUGFS_ROOT, NULL);
}

static void kacctime_rm_debugfs(void)
{
	debugfs_remove_recursive(kacctime_debugfs);
}
/* fin debugfs */

/* fct de temps */

static long kacctime_gettimeofday(const struct pt_regs *regs)
{
	struct timeval *tv = (struct timeval *) regs->di;
	struct timeval ref;
	struct timeval dec;
	struct timeval *pref;
	struct timeval *pdec;
	int multi = 1;
	int divid = 1;

	int space = kacctime_seek_timespace();

	if (space < 0) {
		return (ori_gettimeofday(regs));
	}

	do_gettimeofday(tv);

	ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	ref.tv_usec = kacctime_timespace_tab[space].ref.tv_nsec / 1000;
	dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	dec.tv_usec = kacctime_timespace_tab[space].dec.tv_nsec / 1000;
	pref = &ref;
	pdec = &dec;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_log_level(0, "[%d] GETTIMEOFDAY ori (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);

	kacctime_timeval_sub(tv, pref, tv);
	kacctime_timeval_frac(tv, multi, divid, tv);
	kacctime_timeval_add(tv, pref, tv);
	kacctime_timeval_add(tv, pdec, tv);

	kacctime_log_level(0, "[%d] GETTIMEOFDAY kt  (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);
	
	return 0;
}

static long kacctime_nanosleep(const struct pt_regs *regs)
{
	struct timespec *rqtp = (struct timespec *) regs->di;
	int space = kacctime_seek_timespace();
	int multi = 1;
	int divid = 1;

	if (space < 0) {
		return ori_nanosleep(regs);
	}

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_log_level(0, "[%d] NANOSLEEP OLD (%lld.%.9ld)\n",
			   space, rqtp->tv_sec, rqtp->tv_nsec);

	kacctime_timespec_frac(rqtp, divid, multi, rqtp);

	kacctime_log_level(0, "NANOSLEEP NEW (%lld.%.9ld)\n",
			   rqtp->tv_sec, rqtp->tv_nsec);

	return ori_nanosleep(regs);
}

static long kacctime_clock_gettime(const struct pt_regs *regs)
{
	clockid_t clk_id = regs->di;
	struct timespec *tp = (struct timespec *) regs->si;
	long ret = 0;
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;
	int space = kacctime_seek_timespace();

	if (clk_id != CLOCK_REALTIME) {
	kacctime_log_level(1, "CLOCK_GETTIME: %d\n", clk_id);
		return ori_clock_gettime(regs);		
	}

	if (space < 0) {
		return ori_clock_gettime(regs);
	}

	kacctime_log_level(0, "[%d] OLD CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	ret = ori_clock_gettime(regs);

	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(tp, pref, tp);
	kacctime_timespec_frac(tp, multi, divid, tp);
	kacctime_timespec_add(tp, pref, tp);
	kacctime_timespec_add(tp, pdec, tp);

	kacctime_log_level(0, "[%d] NEW CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	return (ret);
}

static time_t kacctime_time(const struct pt_regs *regs)
{
	time_t *t = (time_t *)regs->di;
	time_t t_ori;
	int space = kacctime_seek_timespace();
	struct timespec *pref;
	struct timespec *pdec;
	struct timespec64 tp;
	struct timespec *ptp;

	int multi = 1;
	int divid = 1;

	if (space < 0) {
		return ori_time(regs);
	}

	ktime_get_real_ts64(&tp);
	t_ori = tp.tv_sec;

	ptp = (struct timespec *)&tp;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(ptp, pref, ptp);
	kacctime_timespec_frac(ptp, multi, divid, ptp);
	kacctime_timespec_add(ptp, pref, ptp);
	kacctime_timespec_add(ptp, pdec, ptp);

	kacctime_log_level(0, "[%d] TIME:[old=%ld:new=%ld]\n",
			   space, t_ori, ptp->tv_sec);

	if (t != NULL) {
		*t = ptp->tv_sec;
	}
	return (ptp->tv_sec);
}

static long kacctime_alarm(const struct pt_regs *regs)
{
	unsigned int sec = regs->di;
	unsigned int *psec = (unsigned int *)&(regs->di);
	struct timespec nsec;
	struct timespec *pnsec;
	int multi;
	int divid;

	int space = kacctime_seek_timespace();

	if (space < 0)
		return (ori_alarm(regs));

	kacctime_log_level(0, "[%d] TIME: os=%d\n", space, sec); 

	nsec.tv_sec = sec;
	nsec.tv_nsec = 0;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	pnsec = &nsec;
	kacctime_timespec_frac(pnsec, divid, multi, pnsec);

	kacctime_log_level(0, "[%d] TIME: os=%d / ns=%ld\n",
			   space, sec, nsec.tv_sec);

	return (ori_alarm(regs));

	if (nsec.tv_sec < 1) {
		*psec = 1;
	} else {
		*psec = nsec.tv_sec;
	}
	return (ori_alarm(regs));
}

/* fin des fct de temps */

static void kacctime_override_syscall(void)
{
	unsigned long cr0;

	kacctime_syscall_table = (sys_call_ptr_t *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_level(0, "sym addr : [%pK]\n", kacctime_syscall_table);

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	ori_nanosleep = kacctime_syscall_table[__NR_nanosleep];
	kacctime_syscall_table[__NR_nanosleep] = kacctime_nanosleep;

	ori_gettimeofday = kacctime_syscall_table[__NR_gettimeofday];
	kacctime_syscall_table[__NR_gettimeofday] = kacctime_gettimeofday;

	ori_clock_gettime = kacctime_syscall_table[__NR_clock_gettime];
	kacctime_syscall_table[__NR_clock_gettime] = kacctime_clock_gettime;

	ori_time = kacctime_syscall_table[__NR_time];
	kacctime_syscall_table[__NR_time] = kacctime_time;

	ori_alarm = kacctime_syscall_table[__NR_alarm];
	kacctime_syscall_table[__NR_alarm] = kacctime_alarm;

	write_cr0(cr0);
	return;
}

static void kacctime_pullback_syscall(void)
{
	unsigned long cr0;

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	kacctime_syscall_table[__NR_nanosleep] = ori_nanosleep;
	kacctime_log_level(0, "getback : nanosleep\n");

	kacctime_syscall_table[__NR_gettimeofday] = ori_gettimeofday;
	kacctime_log_level(0, "getback : gettimeofday\n");

	kacctime_syscall_table[__NR_clock_gettime] = ori_clock_gettime;
	kacctime_log_level(0, "getback : clock_gettime\n");

	kacctime_syscall_table[__NR_time] = ori_time;
	kacctime_log_level(0, "getback : time\n");

	kacctime_syscall_table[__NR_alarm] = ori_alarm;
	kacctime_log_level(0, "getback : alarm\n");

	write_cr0(cr0);
	return;
}
/* fin override syscall */


/* Main */
static int __init kacctime_init(void)
{
//	struct timespec64 ts;

	kacctime_log_level(0, "init kacctime_log_mode: %d\n",
			   kacctime_log_mode);
//	ktime_get_real_ts64(&ts);
//	kacctime_log_level(0, "0-> %ld / %ld\n", ts.tv_sec, ts.tv_nsec);

	kacctime_mk_debugfs();
	kacctime_override_syscall();
	
//	usleep_range(3000000, 3000001);

//	ktime_get_real_ts64(&ts);
//	kacctime_log_level(0, "1-> %ld / %ld\n", ts.tv_sec, ts.tv_nsec);
	return (0);
}

static void __exit kacctime_exit(void)
{
	kacctime_pullback_syscall();
	kacctime_rm_debugfs();
	kacctime_log_level(0, "exit kacctime_log_mode: %d\n",
			   kacctime_log_mode);
}

module_init(kacctime_init);
module_exit(kacctime_exit);
