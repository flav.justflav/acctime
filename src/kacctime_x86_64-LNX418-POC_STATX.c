/**
 * kacctime module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <flav@its-bo.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018     Luc Plisson <lplisson@itsgroup.fr>
 *
 * 2022-09-12 : v.81 -> lnx 4.18 / for DollarU -> pour test 
 * 2022-09-12 : v.82 -> lnx 4.18 / for DollarU / debugfs 
 * 2022-12-13 : v.90 -> lnx 4.18 / tst futex (realtime) -- @flav
 * 2022-12-29 : v.95 -> lnx 3.10 / 4.18 -- @flav
 * 2023-02-05 : v.96 -> add statx syscall -- @flav
*/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/uio.h>
#include <linux/futex.h>

MODULE_VERSION("2023-02-05 11:07:10 .96");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <flav@its-bo.fr>");
MODULE_DESCRIPTION("AccTime LKM");

#define KACCTIME_SYSCALL_SYM	"sys_call_table"

#define CHAR_END_STR	'\0'
#define CHAR_EQUAL	'='

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

/* syscall tables pointers / proto syscall*/
static unsigned long **kacctime_syscall_table;

#define	KACCTIME_MAX_SPACE	1


/* the write_cr0 function cannot be used because of the sensitive cr0 bits pinned by the security issue */
static inline void __write_cr0(unsigned long cr0) 
{ 
	asm volatile("mov %0,%%cr0" : "+r"(cr0) : : "memory"); 
}

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
};

static struct kacctime_timespace kacctime_timespace_tab[KACCTIME_MAX_SPACE];

static atomic_t kacctime_sleep_count = ATOMIC_INIT(0);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
#warning "SYSCALL WRAPPER"
static asmlinkage long (*ori_nanosleep)(const struct pt_regs *);
static asmlinkage long (*ori_clock_gettime)(const struct pt_regs *);
static asmlinkage long (*ori_gettimeofday)(const struct pt_regs *);
static asmlinkage long (*ori_time)(const struct pt_regs *);
static asmlinkage long (*ori_alarm)(const struct pt_regs *);
static asmlinkage long (*ori_select)(const struct pt_regs *);
static asmlinkage long (*ori_futex)(const struct pt_regs *);
static asmlinkage long (*ori_statx)(const struct pt_regs *);
#else
#warning "NO SYSCALL WRAPPER (310)"
static asmlinkage long (*ori_nanosleep)(const struct timespec *, struct timespec *);
static asmlinkage long (*ori_clock_gettime)(clockid_t, struct timespec *);
static asmlinkage long (*ori_gettimeofday)(struct timeval *, struct timezone *);
static asmlinkage long (*ori_time)(time_t *);
static asmlinkage long (*ori_alarm)(unsigned int);
static asmlinkage long (*ori_select)(int, fd_set *, fd_set *, fd_set *, struct timeval *);
static asmlinkage long (*ori_futex)(int *, int , int , struct timespec *, int *, int);
static asmlinkage long (*ori_statx)(int dirfd, const char *pathname, int flags,
				    unsigned int mask, struct statx *statxbuf);
#endif
/* debugfs */
#define KACCTIME_DEBUGFS_ROOT		"kacctime"
#define DEBUGFS_MAX_FILENAME_LEN	64
#define DEBUGFS_MAX_FILE_SIZE		131072		/* 128 ko */
//#define DEBUGFS_MAX_FILE_SIZE		1024

static struct dentry *kacctime_debugfs = NULL;

/*
 * sysfs pour la partie statx
 */
#define KACCTIME_MAX_FILE_STATX	1024
#define KACCTIME_SYSFS_BASENAME "kts"
#define KACCTIME_SYSFS_BASEDIR	"kts_vec"

struct kacctime_statxbuf {
	char pathname[1024];
	struct statx_timestamp kts_atime;  /* Last access */
	struct statx_timestamp kts_btime;  /* Creation */
	struct statx_timestamp kts_ctime;  /* Last status change */
	struct statx_timestamp kts_mtime;  /* Last modification */
};

struct kacctime_statxbuf kacctime_statxbuf_vec[KACCTIME_MAX_FILE_STATX+1];

static int kts_attr_max = 0;

static struct kobj_attribute kts_attribute[KACCTIME_MAX_FILE_STATX+1];
static struct attribute *kts_attrs[KACCTIME_MAX_FILE_STATX+1];
static struct attribute_group kts_attr_group;
static struct kobject *kts_kobj;
static struct kobject kacctime_module_kobj;

/* fin sysfs */

/* Log */
static long     kacctime_log_mode = 0;
module_param(kacctime_log_mode, long, 0660);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/
#define TMP_BUF_MAX_SIZE        1024

#define LOG_NO			(0 << 0)
#define LOG_INIT		(1 << 0)
#define LOG_FUNC		(1 << 1)
#define LOG_CLOCK_GETTIME	(1 << 2)
#define LOG_FUTEX		(1 << 3)
#define LOG_NANO		(1 << 4)
#define LOG_STATX		(1 << 5)

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

#define kacctime_log_flags(...)	kacctime_log_func_flags(__func__, __LINE__, \
							current->comm,	\
							current->pid,	\
							__VA_ARGS__)

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
static __inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}

static __inline__ void kacctime_log_func_flags(const char *func,
					       int line, char *comm,
					       int pid, int flags, char *fmt,
					       ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if ((kacctime_log_mode & flags) == 0)
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}
/* fin log */


/* fct de calcul du temps */

static __inline__ void kacctime_timespec_add(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;					
	
	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) +
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_sub(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) -
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_mul(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}				

static __inline__ void kacctime_timespec_div(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{							
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) / f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_frac(struct timespec *fl_op1,
					      int n, int d,
					      struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * n / d;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_virt_to_real(int space, struct timespec *vt)
{
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;

	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(vt, pref, vt);
	kacctime_timespec_sub(vt, pdec, vt);
	kacctime_timespec_frac(vt, divid, multi, vt);
	kacctime_timespec_add(vt, pref, vt);
}

static __inline__ void kacctime_real_to_virt(int space, struct timespec *vt)
{
/* à tester */
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;

	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(vt, pref, vt);
	kacctime_timespec_frac(vt, multi, divid, vt);
	kacctime_timespec_add(vt, pref, vt);
	kacctime_timespec_add(vt, pdec, vt);
}

static __inline__ void kacctime_timeval_add(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) +
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_sub(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) -
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
		fl_res->tv_usec = inter % USEC_PER_SEC;
		fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_mul(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_div(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) / f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_frac(struct timeval *fl_op1,
					     int n, int d,
					     struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * n / d;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

/* fin fct calcul tmps */

/* time space param */
static long ts0[4] = {
	0, /* ref time */
	0, /* decalage */
	0, /* multi */
	0  /* div */
};

static int arr_argc = 0;

module_param_array(ts0, long, &arr_argc, 0660); 
MODULE_PARM_DESC(ts0, "ref,dec,mul,div");
/* fin time space param */

/**
 * fractionner les sleep
 */ 

#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

static int	kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;
module_param(kacctime_sleep_frac_interval, int, 0660);
MODULE_PARM_DESC(kacctime_sleep_frac_interval, "interval de subdivision");

/** kacctime_seek_timespace - 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 */

static void read_param(void) {
	int i = 0;
	struct timespec sref;
	struct timespec sdec;
	struct timespec64 rnow;
	struct timespec vnow;

	int smulti = kacctime_timespace_tab[i].multi;
	int sdivid = kacctime_timespace_tab[i].divid;

	int nmulti = ts0[2];
	int ndivid = ts0[3];
	long ref = ts0[0];
	long dec = ts0[1];

	if (ref != 0) {
		kacctime_timespace_tab[i].ref.tv_sec = ref;
		kacctime_timespace_tab[i].dec.tv_sec = dec;
		kacctime_timespace_tab[i].multi = nmulti;
		kacctime_timespace_tab[i].divid = ndivid;

		return;
	}

	if (nmulti < 1) {
		nmulti = 1;
	}
	if (ndivid < 1) {
		ndivid = 1;
	}

	if (smulti < 1) {
		smulti = 1;
	}
	if (sdivid < 1) {
		sdivid = 1;
	}

	sref.tv_sec = kacctime_timespace_tab[i].ref.tv_sec;
	/* sref.tv_nsec = 0; */
	sref.tv_nsec = kacctime_timespace_tab[i].ref.tv_nsec;

	sdec.tv_sec = kacctime_timespace_tab[i].dec.tv_sec;
	/* sdec.tv_nsec = 0; */
	sdec.tv_nsec = kacctime_timespace_tab[i].dec.tv_nsec;

	/* nref.tv_sec = ts0[0]; */
	/* nref.tv_nsec = 0; */
	/* ndec.tv_sec = ts0[1]; */
	/* ndec.tv_nsec = 0; */

	if ((smulti == nmulti) &&    /* ts0[0] == 0 && ts0[1] == 0 */
	    (sdivid == ndivid)) {
		return;
	}

	ktime_get_real_ts64(&rnow);
	kacctime_log_flags(LOG_INIT,
			   "ts0 changed, recalcul [%ld,%ld](%d/%d)\n",
			   rnow.tv_sec, rnow.tv_nsec, smulti, sdivid);
	
	kacctime_log_flags(LOG_INIT, "s=>[%ld,%ld] (%d/%d)",
			   sref.tv_sec, sdec.tv_sec, smulti, sdivid);
	kacctime_log_flags(LOG_INIT, "n=>[%ld,%ld] (%d/%d)",
			   ts0[0], 0, nmulti, ndivid);
	
	vnow.tv_sec = rnow.tv_sec;
	vnow.tv_nsec = rnow.tv_nsec;
	
	kacctime_timespec_sub(&vnow, &sref, &vnow);
	kacctime_timespec_frac(&vnow, smulti, sdivid, &vnow);
	kacctime_timespec_add(&vnow, &sref, &vnow);
	kacctime_timespec_add(&vnow, &sdec, &vnow);
	
	kacctime_log_flags(LOG_INIT,"ts0 changed, recalcul... [%ld,%ld]\n",
			   vnow.tv_sec, vnow.tv_nsec);		
	
	kacctime_timespace_tab[i].ref.tv_sec = rnow.tv_sec;
	ts0[0] = rnow.tv_sec;
	kacctime_timespace_tab[i].ref.tv_nsec = rnow.tv_nsec;
	kacctime_timespec_sub(&vnow, (struct timespec *)&rnow,
			      &(kacctime_timespace_tab[i].dec));
	ts0[1] = kacctime_timespace_tab[i].dec.tv_sec;
	kacctime_timespace_tab[i].multi = nmulti;
	kacctime_timespace_tab[i].divid = ndivid;
}

static int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;

	read_param();

	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_flags(LOG_INIT, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_flags(LOG_INIT, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
		  
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_flags(LOG_INIT, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}
				kfree(buf);
				return 0; /* only deal with ts0 */

				if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
					kacctime_log_flags(LOG_INIT,
							   "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_level(5, "space -> [%d]\n", space);
				kfree(buf);
				if (kacctime_timespace_tab[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	if (kacctime_timespace_tab[0].multi == 0)    
		return (-1);
	return (-1);
}

/**
   fin  recherche timespace 
*/

/* fct de temps */
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static asmlinkage long kacctime_gettimeofday(const struct pt_regs *regs)
#else
static asmlinkage long kacctime_gettimeofday(struct timeval *ptv,
					     struct timezone *ptz)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	struct timeval *tv = (struct timeval *) regs->di;
#else
	struct timeval *tv = ptv;
#endif
	struct timeval ref;
	struct timeval dec;
	int multi = 1;
	int divid = 1;

	int space = kacctime_seek_timespace();

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_gettimeofday(regs));
#else
		return (ori_gettimeofday(ptv, ptz));
#endif
	}

	do_gettimeofday(tv);

	ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	ref.tv_usec = kacctime_timespace_tab[space].ref.tv_nsec / 1000;
	dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	dec.tv_usec = kacctime_timespace_tab[space].dec.tv_nsec / 1000;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_log_flags(LOG_FUNC, "[%d] GETTIMEOFDAY ori (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);

	kacctime_timeval_sub(tv, &ref, tv);
	kacctime_timeval_frac(tv, multi, divid, tv);
	kacctime_timeval_add(tv, &ref, tv);
	kacctime_timeval_add(tv, &dec, tv);

	kacctime_log_flags(LOG_FUNC, "[%d] GETTIMEOFDAY kt  (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);
	
	return 0;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_futex(const struct pt_regs *regs)
#else
static long kacctime_futex(int *uaddr, int pfutex_op, int val,
			   struct timespec *ptimeout, int *uaddr2,
			   int val3)
#endif
{
	int space = kacctime_seek_timespace();

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	int futex_op = regs->si;
#else
	int futex_op = pfutex_op;
#endif

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_futex(regs);
#else
		return ori_futex(uaddr, pfutex_op, val, ptimeout, uaddr2, val3);
#endif
	}

	if (((futex_op & FUTEX_WAIT_BITSET) != 0) &&
	    ((futex_op & FUTEX_CLOCK_REALTIME) != 0)) {
		struct timespec timeout;
		int r;

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		if ((void *)regs->r10 == NULL) {
			return ori_futex(regs);
		}
#else
		if (ptimeout == NULL) {
			return ori_futex(uaddr, pfutex_op, val,
					 ptimeout, uaddr2, val3);
		}
#endif

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_from_user(&timeout, (struct timespec *) regs->r10,
				   sizeof(timeout));
#else
		r = copy_from_user(&timeout, ptimeout, sizeof(timeout));
#endif
		if (r) {
			pr_warn("copy from user err\n");
			return -ENOMEM;
		}

		kacctime_log_flags(LOG_FUTEX, "futex VIRT %d: (%ld / %ld)\n",
				   futex_op, timeout.tv_sec, timeout.tv_nsec);

		kacctime_virt_to_real(space, &timeout);

		kacctime_log_flags(LOG_FUTEX, "futex REAL %d: (%ld / %ld)\n",
				   futex_op, timeout.tv_sec, timeout.tv_nsec);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_to_user((struct timespec *) regs->r10, &timeout,
				 sizeof(timeout));
#else
		r = copy_to_user(ptimeout, &timeout, sizeof(timeout));
#endif
		if (r) {
			pr_warn("copy to user err\n");
			return -ENOMEM;
		}
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	return ori_futex(regs);
#else
	return ori_futex(uaddr, pfutex_op, val,
			 ptimeout, uaddr2, val3);
#endif
}

static int kacctime_debug_nanosleep_show(struct seq_file *f, void *data) {
	struct iovec *d = (struct iovec *) f->private;

	seq_write(f, d->iov_base, d->iov_len);
	return 0;
}

static int kacctime_debug_nanosleep_open(struct inode *i, struct file *f) {
	return single_open(f, kacctime_debug_nanosleep_show,
			   i->i_private);
}

static const struct file_operations kacctime_debug_nanosleep_fops = {
	.open = kacctime_debug_nanosleep_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release
};

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_nanosleep(const struct pt_regs *regs)
#else
static long kacctime_nanosleep(struct timespec *req, struct timespec *rem)
#endif
{
/*	struct timespec *rqtp = (struct timespec *) regs->di;*/
	struct timespec *rqtp;

	int space = kacctime_seek_timespace();
	int multi = 1;
	int divid = 1;
	struct timespec *pref;
	struct timespec *pdec;
	long r;
	char *filename;
	struct dentry *debug_nano;
	struct iovec *nano_data;
	/* struct timespec *srqtp; */
	struct timespec *t_final;
	struct timespec *remaining;
	struct timespec *t_cur;

	int go_on = true;
	int idx = 0;

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_nanosleep(regs);
#else
		return ori_nanosleep(req, rem);
#endif
	}

	atomic_inc(&kacctime_sleep_count);

	rqtp = kmalloc(sizeof (*rqtp), GFP_KERNEL);
	if (!rqtp) {
		pr_warn("nano rqtp: MEM ERR\n");
		r = -ENOMEM;
		goto out;
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = copy_from_user(rqtp, (struct timespec *) regs->di, sizeof(*rqtp));
#else
	r = copy_from_user(rqtp, req, sizeof(*rqtp));
#endif
	if (r) {
		pr_warn("nano rqtp: copy user ERR\n");
		r = -ENOMEM;
		goto free_rqtp;
	}

	t_final = kmalloc(sizeof (*t_final), GFP_KERNEL);
	if (!t_final) {
		pr_warn("nano t_final: MEM ERR\n");
		r = -ENOMEM;
		goto free_rqtp;
	}

	filename = kmalloc(DEBUGFS_MAX_FILENAME_LEN, GFP_KERNEL);
	if (!filename) {
		pr_warn("nanosleep / filename: MEM ERR\n");
		r = -ENOMEM;
		goto free_t_final;
	}

	snprintf(filename, DEBUGFS_MAX_FILENAME_LEN, "nano_%s_%d_%d",
		 current->comm, current->pid,
		 atomic_read(&kacctime_sleep_count));

	nano_data = kmalloc(sizeof (*nano_data), GFP_KERNEL);
	if (!nano_data) {
		pr_warn("nanosleep / iovec struct: MEM ERR\n");
		r = -ENOMEM;
		goto free_filename;
	}

	nano_data->iov_base = kmalloc(DEBUGFS_MAX_FILE_SIZE, GFP_KERNEL);
	if (!nano_data->iov_base) {
		pr_warn("nanosleep / iov_base: MEM ERR\n");
		r = -ENOMEM;
		goto free_nano_data;
	}

	debug_nano = debugfs_create_file(filename, 0660, kacctime_debugfs,
					 nano_data,
					 &kacctime_debug_nanosleep_fops);
	remaining = kmalloc(sizeof (*remaining), GFP_KERNEL);
	if (!remaining) {
		pr_warn("nano remaining: MEM ERR\n");
		r = -ENOMEM;
		goto free_iovbase;
	}

/*
	debug_nano = debugfs_create_u64(filename, S_IFREG|S_IRUGO,
					kacctime_debugfs,
					(u64 *) & remaining->tv_sec);
*/

	if (!debug_nano) {
		pr_warn("debug nano: ERR create file\n");
	}
	
	t_cur = kmalloc(sizeof (*t_cur), GFP_KERNEL);
	if (!t_cur) {
		pr_warn("nano t_cur: MEM ERR\n");
		r = -ENOMEM;
		goto free_remaining;	       
	}

	ktime_get_real_ts64((struct timespec64*) t_final);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);

	kacctime_timespec_sub(t_final, pref, t_final);
	kacctime_timespec_frac(t_final, multi, divid, t_final);
	kacctime_timespec_add(t_final, pref, t_final);
	kacctime_timespec_add(t_final, pdec, t_final);

	kacctime_timespec_add(t_final, rqtp, t_final);

/*
	nano_data->iov_len = snprintf((char *) nano_data->iov_base,
				      DEBUGFS_MAX_FILE_SIZE,
				      "idx\tcomm\tpid\ttps réel\ttps virt\n\%d\t%s\t%d\t(%ld.%.9ld)\t(-)\n",
				      idx, current->comm, current->pid, 
				      srqtp->tv_sec, srqtp->tv_nsec);
*/
	nano_data->iov_len = snprintf((char *) nano_data->iov_base,
				      DEBUGFS_MAX_FILE_SIZE,
				      "idx\tcomm\tpid\ttps réel\ttps virt\n\%d\t%s\t%d\t(-)\n",
				      idx, current->comm, current->pid);

	while (go_on) {
		ktime_get_real_ts64((struct timespec64 *) t_cur);
		idx++;
		read_param();
		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		pref = &(kacctime_timespace_tab[space].ref);
		pdec = &(kacctime_timespace_tab[space].dec);
		
		kacctime_timespec_sub(t_cur, pref, t_cur);
		kacctime_timespec_frac(t_cur, multi, divid, t_cur);
		kacctime_timespec_add(t_cur, pref, t_cur);
		kacctime_timespec_add(t_cur, pdec, t_cur);

		kacctime_timespec_sub(t_final, t_cur, remaining);

		if (remaining->tv_sec < 0 || remaining->tv_nsec < 0) {
			go_on = false;
			continue;
		}
		if (remaining->tv_sec <= kacctime_sleep_frac_interval) {
			rqtp->tv_sec = remaining->tv_sec;
			rqtp->tv_nsec = remaining->tv_nsec;
			go_on = false;
		} else {
			rqtp->tv_sec = kacctime_sleep_frac_interval;
			rqtp->tv_nsec = 0;
		}

		kacctime_log_flags(LOG_NANO,
				   "NANOSLEEP t_cur:: (%lld.%.9ld) -> t_final(%lld.%.9ld)\n",
				   t_cur->tv_sec, t_cur->tv_nsec,
				   t_final->tv_sec, t_final->tv_nsec);

		kacctime_log_flags(LOG_NANO,
				   "NANOSLEEP (%lld.%.9ld) -> (%lld.%.9ld)\n",
				   remaining->tv_sec, remaining->tv_nsec,
				   rqtp->tv_sec, rqtp->tv_nsec);

/*		nano_data->iov_len =
			snprintf((char *) nano_data->iov_base,
				 10*PAGE_SIZE,
				 "%s%d\t%s\t%d\t(%ld.%.9ld)\t(%ld.%.9ld)\n",
				 (char *)nano_data->iov_base, idx,
				 current->comm, current->pid,
				 remaining->tv_sec, remaining->tv_nsec,
				 rqtp->tv_sec, rqtp->tv_nsec);
*/

		nano_data->iov_len =
			snprintf((char *) nano_data->iov_base,
				 DEBUGFS_MAX_FILE_SIZE,
				 "%s%d\t%s\t%d\t(%ld.%.9ld)\t(%ld.%.9ld)\n",
				 (char *)nano_data->iov_base, idx,
				 current->comm, current->pid,
				 remaining->tv_sec, remaining->tv_nsec,
				 rqtp->tv_sec, rqtp->tv_nsec);

		kacctime_timespec_frac(rqtp, divid, multi, rqtp);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_to_user((struct timespec *) regs->di, rqtp, 
				 sizeof(*rqtp));
#else
		r = copy_to_user(req, rqtp, sizeof(*rqtp));
#endif
		if (r) {
			pr_warn("nano err copy to");
			go_on = false;
		}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = ori_nanosleep(regs);
#else
		r = ori_nanosleep(req, rem);
#endif

		if (r < 0) {
			go_on = false;
		}
	}

	kfree(t_cur);

free_remaining:
	kfree(remaining);

free_iovbase:
	kfree(nano_data->iov_base);
	debugfs_remove(debug_nano);

free_nano_data:
	kfree(nano_data);

free_filename:
	kfree(filename);

free_t_final:
	kfree(t_final);

free_rqtp:
	kfree(rqtp);

out:
	atomic_dec(&kacctime_sleep_count);
	return r;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_statx(const struct pt_regs *regs)
#else
static long kacctime_statx(int dirfd, const char *pathname, int flags,
			   unsigned int mask, struct statx *statxbuf)
#endif
{
	int idx = 1;
	long r;

	int space = kacctime_seek_timespace();

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	char *pathname = (char *) regs->si;
	struct statx *statxbuf = (struct statx *) regs->r8;
#endif
	kacctime_log_flags(LOG_STATX, "STATX: %s\n",
			   pathname);

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_statx(regs);
#else
		return ori_statx(dirfd, pathname, flags, mask, statxbuf);
#endif
	}


	while ((idx < kts_attr_max) || idx < KACCTIME_MAX_FILE_STATX) {
		if (strcmp(pathname,
			   kacctime_statxbuf_vec[idx].pathname) == 0) {
			break;
		}
		idx++;
	}
	kacctime_log_flags(LOG_STATX, "%d->%s %lld %lld %lld %lld\n", idx,
			   kacctime_statxbuf_vec[idx].pathname,
			   kacctime_statxbuf_vec[idx].kts_atime.tv_sec,
			   kacctime_statxbuf_vec[idx].kts_btime.tv_sec,
			   kacctime_statxbuf_vec[idx].kts_ctime.tv_sec,
			   kacctime_statxbuf_vec[idx].kts_mtime.tv_sec);
	
	
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = ori_statx(regs);
#else
	r = ori_statx(dirfd, pathname, flags, mask, statxbuf);
#endif
	if (idx == KACCTIME_MAX_FILE_STATX)
		return r;
	
	statxbuf->stx_atime.tv_sec = kacctime_statxbuf_vec[idx].kts_atime.tv_sec;
	statxbuf->stx_btime.tv_sec = kacctime_statxbuf_vec[idx].kts_btime.tv_sec;
	statxbuf->stx_ctime.tv_sec = kacctime_statxbuf_vec[idx].kts_ctime.tv_sec;
	statxbuf->stx_mtime.tv_sec = kacctime_statxbuf_vec[idx].kts_mtime.tv_sec;

	return r;
}


#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_clock_gettime(const struct pt_regs *regs)
#else
static long kacctime_clock_gettime(clockid_t pclk_id, struct timespec *ptp)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	clockid_t clk_id = regs->di;
	struct timespec *tp = (struct timespec *) regs->si;
#else
	clockid_t clk_id = pclk_id;
	struct timespec *tp = ptp;
#endif
	long ret = 0;
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;
	int space = kacctime_seek_timespace();

	if (clk_id != CLOCK_REALTIME) {
		kacctime_log_flags(LOG_CLOCK_GETTIME,
				   "CLOCK_GETTIME: %d\n", clk_id);
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_clock_gettime(regs);
#else
		return ori_clock_gettime(clk_id, tp);
#endif
	}

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_clock_gettime(regs);
#else
		return ori_clock_gettime(clk_id, tp);
#endif
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		ret = ori_clock_gettime(regs);
#else
		ret = ori_clock_gettime(clk_id, tp);
#endif

	kacctime_log_flags(LOG_FUNC, "[%d] OLD CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(tp, pref, tp);
	kacctime_timespec_frac(tp, multi, divid, tp);
	kacctime_timespec_add(tp, pref, tp);
	kacctime_timespec_add(tp, pdec, tp);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	return (ret);
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static time_t kacctime_time(const struct pt_regs *regs)
#else
static time_t kacctime_time(time_t *pt)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	time_t *t = (time_t *)regs->di;
#else
	time_t *t = pt;
#endif

	time_t t_ori;
	int space = kacctime_seek_timespace();
	struct timespec *pref;
	struct timespec *pdec;
	struct timespec64 tp;
	struct timespec *ptp;

	int multi = 1;
	int divid = 1;

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_time(regs);
#else
		return ori_time(pt);
#endif
	}

	ktime_get_real_ts64(&tp);
	t_ori = tp.tv_sec;

	ptp = (struct timespec *)&tp;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(ptp, pref, ptp);
	kacctime_timespec_frac(ptp, multi, divid, ptp);
	kacctime_timespec_add(ptp, pref, ptp);
	kacctime_timespec_add(ptp, pdec, ptp);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME:[old=%ld:new=%ld]\n",
			   space, t_ori, ptp->tv_sec);

	if (t != NULL) {
		*t = ptp->tv_sec;
	}
	return (ptp->tv_sec);
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_alarm(const struct pt_regs *regs)
#else
static long kacctime_alarm(unsigned int pseconds)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	unsigned int sec = regs->di;
	unsigned int *psec = (unsigned int *)&(regs->di);
#else
	unsigned int sec = pseconds;
	unsigned int *psec = &pseconds;
#endif
	struct timespec nsec;
	int multi;
	int divid;

	int space = kacctime_seek_timespace();

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_alarm(regs));
#else
		return (ori_alarm(sec));
#endif
	}

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d\n", space, sec); 

	nsec.tv_sec = sec;
	nsec.tv_nsec = 0;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_frac(&nsec, divid, multi, &nsec);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d / ns=%ld\n",
			   space, sec, nsec.tv_sec);

	if (nsec.tv_sec < 1) {
		*psec = 1;
	} else {
		*psec = nsec.tv_sec;
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	return (ori_alarm(regs));
#else
	return (ori_alarm(sec));
#endif

}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_select(const struct pt_regs *regs)
#else
static long kacctime_select(int pnfds, fd_set *readfds, fd_set *writefds,
			    fd_set *exceptfds, struct timeval *ptimeout)
#endif
{
	int space = kacctime_seek_timespace();
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	int nfds = regs->di;
	struct timeval *timeout = (struct timeval *) regs->r8;
#else
	int nfds = pnfds;
	struct timeval *timeout = ptimeout;
#endif
	int multi;
	int divid;

	if (timeout == NULL) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	if (nfds > 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	kacctime_log_flags(LOG_FUNC, "[%d] OLD SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timeval_frac(timeout, divid, multi, timeout);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   timeout));
#endif
}

/* fin des fct de temps */

static void kacctime_override_syscall(void)
{
	unsigned long cr0;

	kacctime_syscall_table = (unsigned long **) 
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_flags(LOG_INIT, "sym addr : [%pK]\n", kacctime_syscall_table);

	if (kacctime_syscall_table == NULL) {
		pr_warn("AIE !!!\n");
	}

	cr0 = read_cr0();
	__write_cr0(cr0 & ~X86_CR0_WP);

	ori_statx = (void *)kacctime_syscall_table[__NR_statx];
	kacctime_syscall_table[__NR_statx] = (unsigned long *)kacctime_statx;
	kacctime_log_flags(LOG_INIT, "takeover: statx\n");

	ori_futex = (void *)kacctime_syscall_table[__NR_futex];
	kacctime_syscall_table[__NR_futex] = (unsigned long *)kacctime_futex;
	kacctime_log_flags(LOG_INIT, "takeover: futex\n");

	ori_nanosleep = (void *)kacctime_syscall_table[__NR_nanosleep];
	kacctime_syscall_table[__NR_nanosleep] =
		(unsigned long *)kacctime_nanosleep;
	kacctime_log_flags(LOG_INIT, "takeover: nanosleep\n");

	ori_gettimeofday = (void *)kacctime_syscall_table[__NR_gettimeofday];
	kacctime_syscall_table[__NR_gettimeofday] = 
		(unsigned long *)kacctime_gettimeofday;
	kacctime_log_flags(LOG_INIT, "takeover: gettimeofday\n");

	ori_clock_gettime = (void *)kacctime_syscall_table[__NR_clock_gettime];
	kacctime_syscall_table[__NR_clock_gettime] = 
		(unsigned long *)kacctime_clock_gettime;
	kacctime_log_flags(LOG_INIT, "takeover: clock_gettime\n");

	ori_time = (void *)kacctime_syscall_table[__NR_time];
	kacctime_syscall_table[__NR_time] = (unsigned long *)kacctime_time;
	kacctime_log_flags(LOG_INIT, "takeover: time\n");

	ori_alarm = (void *)kacctime_syscall_table[__NR_alarm];
	kacctime_syscall_table[__NR_alarm] = (unsigned long *)kacctime_alarm;
	kacctime_log_flags(LOG_INIT, "takeover: alarm\n");

	ori_select = (void *)kacctime_syscall_table[__NR_select];
	kacctime_syscall_table[__NR_select] = (unsigned long *)kacctime_select;
	kacctime_log_flags(LOG_INIT, "takeover: select\n");
	
	__write_cr0(cr0);
	return;
}

static void kacctime_pullback_syscall(void)
{
	unsigned long cr0;

	cr0 = read_cr0();
	__write_cr0(cr0 & ~X86_CR0_WP);

	kacctime_syscall_table[__NR_statx] = (unsigned long *)ori_statx;
	kacctime_log_flags(LOG_INIT, "getback : statx\n");

	kacctime_syscall_table[__NR_futex] = (unsigned long *)ori_futex;
	kacctime_log_flags(LOG_INIT, "getback : futex\n");

	kacctime_syscall_table[__NR_nanosleep] = (unsigned long *)ori_nanosleep;
	kacctime_log_flags(LOG_INIT, "getback : nanosleep\n");

	kacctime_syscall_table[__NR_gettimeofday] =
		(unsigned long *)ori_gettimeofday;
	kacctime_log_flags(LOG_INIT, "getback : gettimeofday\n");

	kacctime_syscall_table[__NR_clock_gettime] =
		(unsigned long *)ori_clock_gettime;
	kacctime_log_flags(LOG_INIT, "getback : clock_gettime\n");

	kacctime_syscall_table[__NR_time] = (unsigned long *)ori_time;
	kacctime_log_flags(LOG_INIT, "getback : time\n");

	kacctime_syscall_table[__NR_alarm] = (unsigned long *)ori_alarm;
	kacctime_log_flags(LOG_INIT, "getback : alarm\n");

	kacctime_syscall_table[__NR_select] = (unsigned long *)ori_select;
	kacctime_log_flags(LOG_INIT, "getback : select\n");

	__write_cr0(cr0);

	while (atomic_read(&kacctime_sleep_count) > 0) {
		kacctime_log_flags(LOG_INIT, "sleep running: %d\n",
				   atomic_read(&kacctime_sleep_count));
		ssleep(1);
	}

	return;
}
/* fin override syscall */

/* debugfs */
static void kacctime_mk_debugfs(void)
{
	kacctime_log_flags(LOG_INIT, "creation debugfs\n");
	kacctime_debugfs = debugfs_create_dir(KACCTIME_DEBUGFS_ROOT, NULL);
	debugfs_create_atomic_t("sleep_running", S_IFREG | S_IRUGO,
				kacctime_debugfs,
				&kacctime_sleep_count);
}

static void kacctime_rm_debugfs(void)
{
	kacctime_log_flags(LOG_INIT, "suppression debugfs\n");
	debugfs_remove_recursive(kacctime_debugfs);
}
/* fin debugfs */

ssize_t kts_show(struct kobject *kobj, struct kobj_attribute *attr,
		 char *buf)
{
	int r = 0;
	int idx = 1;

	r = sprintf(buf, "%s\n", attr->attr.name);
	while ((idx < kts_attr_max) || idx < KACCTIME_MAX_FILE_STATX) {
		if (strcmp(attr->attr.name,
			   kacctime_statxbuf_vec[idx].pathname) == 0) {
			r = sprintf(buf, "%d->%s %lld %lld %lld %lld\n", idx,
				    kacctime_statxbuf_vec[idx].pathname,
				    kacctime_statxbuf_vec[idx].kts_atime.tv_sec,
				    kacctime_statxbuf_vec[idx].kts_btime.tv_sec,
				    kacctime_statxbuf_vec[idx].kts_ctime.tv_sec,
				    kacctime_statxbuf_vec[idx].kts_mtime.tv_sec);
			kacctime_log_flags( LOG_STATX, "%s\n", buf);
			return r;
		}
		idx++;
	}
	return r;
}

ssize_t kts_store(struct kobject *kobj, struct kobj_attribute *attr,
		  const char *buf, size_t count)
{
	return 0;
}

/* ssize_t kts_add_file_show(struct kobject *kobj, struct kobj_attribute *attr, */
/* 			  char *buf) */
/* { */
/* 	return 0; */
/* } */

ssize_t kts_add_file_store(struct kobject *kobj, struct kobj_attribute *attr,
			   const char *buf, size_t count)
{
	int r = 0;

	if (kts_attr_max == 1024) {
		return -EIO;
	}

	r = sscanf(buf, "%s %lld:%lld:%lld:%lld",
		   kacctime_statxbuf_vec[kts_attr_max].pathname,
		   &(kacctime_statxbuf_vec[kts_attr_max].kts_atime.tv_sec),
		   &(kacctime_statxbuf_vec[kts_attr_max].kts_btime.tv_sec),
		   &(kacctime_statxbuf_vec[kts_attr_max].kts_ctime.tv_sec),
		   &(kacctime_statxbuf_vec[kts_attr_max].kts_mtime.tv_sec));

	kacctime_log_flags(LOG_STATX, "%d->%s %lld %lld %lld %lld\n",
			   kts_attr_max,
			   kacctime_statxbuf_vec[kts_attr_max].pathname,
			   kacctime_statxbuf_vec[kts_attr_max].kts_atime.tv_sec,
			   kacctime_statxbuf_vec[kts_attr_max].kts_btime.tv_sec,
			   kacctime_statxbuf_vec[kts_attr_max].kts_ctime.tv_sec,
			   kacctime_statxbuf_vec[kts_attr_max].kts_mtime.tv_sec);

	kts_attribute[kts_attr_max].attr.name =
		kacctime_statxbuf_vec[kts_attr_max].pathname;
	kts_attribute[kts_attr_max].attr.mode = VERIFY_OCTAL_PERMISSIONS(0664);
	kts_attribute[kts_attr_max].show = kts_show;
	kts_attribute[kts_attr_max].store = kts_store;
	kts_attrs[kts_attr_max] = &(kts_attribute[kts_attr_max].attr);
	sysfs_create_file(kts_kobj, kts_attrs[kts_attr_max]);
	kts_attr_max++;
	kts_attrs[kts_attr_max] = NULL;
	
	return count;
}

static int kacctime_sysfs_init(void) {
	int r;

	kacctime_module_kobj = (((struct module *)(THIS_MODULE))->mkobj).kobj;	
	kts_kobj = kobject_create_and_add(KACCTIME_SYSFS_BASEDIR,
					  &kacctime_module_kobj);
	if (!kts_kobj)
		return -ENOMEM;

	kts_attribute[0].attr.name = "add_file";
	kts_attribute[0].attr.mode = VERIFY_OCTAL_PERMISSIONS(0664);
//	kts_attribute[0].show = kts_add_file_show;
	kts_attribute[0].store = kts_add_file_store;
	kts_attrs[0] = &(kts_attribute[0].attr);
	kts_attrs[1] = NULL;
	kts_attr_max = 1;
	kts_attr_group.attrs = kts_attrs;

	r = sysfs_create_file(kts_kobj, kts_attrs[0]);
	if (r) {
		kobject_put(kts_kobj);
	}

	kacctime_log_flags(LOG_INIT, "sysfs init: %s\n", KACCTIME_SYSFS_BASEDIR);

	return r;
}

/* Main */
static int __init kacctime_init(void)
{
	kacctime_log_flags(LOG_INIT, "init kacctime_log_mode: %d\n",
			   kacctime_log_mode);

	kacctime_mk_debugfs();
	kacctime_sysfs_init();
	
	kacctime_override_syscall();

	kacctime_timespace_tab[0].ref.tv_sec = ts0[0];
	kacctime_timespace_tab[0].ref.tv_nsec = 0;
	kacctime_timespace_tab[0].dec.tv_sec = ts0[1];
	kacctime_timespace_tab[0].dec.tv_nsec = 0;
	kacctime_timespace_tab[0].multi = ts0[2];
	kacctime_timespace_tab[0].divid = ts0[3];
	
	return (0);
}

static void __exit kacctime_exit(void)
{
	kacctime_pullback_syscall();
	kacctime_rm_debugfs();

	kacctime_log_flags(LOG_INIT, "exit kacctime_log_mode: %d\n",
			   kacctime_log_mode);
}

module_init(kacctime_init);
module_exit(kacctime_exit);
