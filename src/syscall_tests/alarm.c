#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void alarm_handler(int signum){
    printf("ok\n");
}

int main(int argc, char *argv[])
{

    if (argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [seconds]\n");
        return 0;
    }
    int sec = atoi(argv[1]);
    signal(SIGALRM, alarm_handler);
    alarm(sec);
    pause();
    return 0;
}