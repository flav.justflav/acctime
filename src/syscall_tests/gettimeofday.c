#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <errno.h>

void test(int expected, int err, struct timeval *tv)
{
    int ret = gettimeofday(tv, NULL);
    if (err == errno && ret == expected)
        printf("ok\n");
    else
        printf("ERROR");
}

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [case] [second] [nanoseconds]\n");
        return 0;
    }

    struct timeval tv;
    test(0, 0, &tv);

    return 0;
}