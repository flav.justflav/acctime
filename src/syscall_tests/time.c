#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>

void test(int err)
{
    int ret = time(NULL);
    if (err == errno)
        printf("ok\n");
    else
        printf("errno not set:\nTime argument is obsolescent and %s %s",
                "should always be NULL in new code.\nWhen tloc is",
                "NULL, the call cannot fail.\n");
}

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [case]\n");
        return 0;
    }

    if (strcmp(argv[1], "OK") == 0)
        test(0);
    if (strcmp(argv[1], "EFAULT") == 0)
        test(EFAULT);

    return 0;
}