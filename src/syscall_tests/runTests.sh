make

printf "\nalarm\n"
./alarmTest 1

printf "\ngettimeofday\n"
./gettimeofdayTest

printf "\nclock_gettime\n"
./clockgettimeTest OK
./clockgettimeTest EINVAL

printf "\ntime\n"
./timeTest OK
#./timeTest EFAULT

printf "\nnanosleep\n"
./nanosleepTest OK 1 100
./nanosleepTest EINVAL -1 100
./nanosleepTest EINVAL 1 -100
./nanosleepTest EINVAL 1 1000000000
./nanosleepTest EFAULT "" ""
./nanosleepTest EINTR 2 200

printf "\nselect\n"
./selectTest OK
./selectTest EBADF
./selectTest EINVAL
./selectTest EINTR

