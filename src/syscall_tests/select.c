#include <stdio.h>
#include <time.h>
#include <sys/select.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/resource.h>
#include <signal.h>
#include <sys/wait.h>

void test(int nbr, fd_set *set, time_t sec, time_t usec, int err)
{
    struct timeval tv;
    tv.tv_sec = sec;
    tv.tv_usec = usec;

    select(nbr, set, set, set, &tv);

    if (err == errno)
        printf("ok\n");
    else
        printf("ERROR\n");
}

void sig_handler(int signum) {}

int main(int argc, char *argv[])
{

    if (argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [case]\n");
        return 0;
    }

    signal(SIGINT, sig_handler);
    fd_set set;

    FD_ZERO(&set);
    FD_SET(0, &set);

    if (strcmp(argv[1], "OK") == 0)
        test(1, &set, 2, 200, 0);
    if (strcmp(argv[1], "EBADF") == 0)
    {
        FILE *file = fopen("./select.c", "r");
        int fd = file->_fileno;
        fclose(file);
        FD_SET(fd, &set);
        test(10, &set, 2, 200, EBADF);
    }
    if (strcmp(argv[1], "EINVAL") == 0)
        test(-1, &set, 2, 200, EINVAL);
    if (strcmp(argv[1], "EINVAL") == 0)
        test(RLIMIT_NOFILE + 1, &set, 2, 200, EINVAL);
    if (strcmp(argv[1], "EINVAL") == 0)
        test(-1, &set, -2, 200, EINVAL);
    // if (strcmp(argv[1], "ENOMEM") == 0)
    if (strcmp(argv[1], "EINTR") == 0)
    {
        int pid = fork();
        if (pid == 0)
            test(1, &set, 4, 200, 0);
        else
        {
            struct timespec tmp;
            tmp.tv_sec = 2;
            tmp.tv_nsec = 200;
            nanosleep(&tmp, NULL);
            kill(pid, SIGINT);
            waitpid(pid, NULL, 0);
        }
    }

    return 0;
}