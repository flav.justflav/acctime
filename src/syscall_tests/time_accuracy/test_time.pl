#!/usr/bin/perl
use warnings;
use strict;
use Time::HiRes;
use POSIX qw(strftime);
use IO::Handle;

sub computeTime
{
    my $time = $ARGV[0];
    my $usedTime = 0;

    my @a = (1..$#ARGV - 1);
    my $i = 1;
    for (; $i<= $#a; $i+=2)
    {
        $usedTime += $ARGV[$i+1];
        $time -= $ARGV[$i+1] * $ARGV[$i];
    }

    $time = $time / $ARGV[$i] + $usedTime;

    printf "target time: %.9f\n", $time;
}
computeTime;

open(FH, '>', "/sys/module/kacctime_x86_64_LNX418/parameters/ts0");
FH->autoflush(1);

print FH "0,0,$ARGV[1],1\n";
print "new acceleration: x$ARGV[1]\n";

my $begin = Time::HiRes::clock_gettime();
my $pid = fork();
if ($pid == 0)
{
    exec("FLAV_TIME=0 sleep $ARGV[0]") or print STDERR "error: $!\n";
}

my @a = (1..$#ARGV);
for (my $i = 2; $i<= $#a; $i+=2)
{
        sleep $ARGV[$i];
        print FH "0,0,$ARGV[$i+1],1\n";
        print "new acceleration: x$ARGV[$i+1]\n";
}

wait();

my $end =  Time::HiRes::clock_gettime();
my $diff = $end-$begin;

printf "actual time: %.9f\n", $diff;

close(FH);
