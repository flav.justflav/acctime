computeTime ()
{
    time=$1
    usedTime=0
    for i in $(seq 2 2 $(($#-1)))
    do
        speed=$2
        newtime=$3
        usedTime=$((usedTime+newtime))
        time=$((time-newtime*speed))
        shift
        shift
    done

    lastSpeed=$2
    time=$((1000000000 * time/lastSpeed + 1000000000 * usedTime))
    time=$(printf %.10f\\n "$((time))e-9")

    echo "target time: $time"
}


### compute expected time

computeTime "$@"

### get begin time
begin=$(date +%s%N)

### begin sleep on accelerated computer and set its acceleration
file="/sys/module/kacctime_x86_64_LNX418/parameters/ts0"
FLAV_TIME=0 sleep $1 & 
echo "0,0,$2,1" > $file

pid=$!
shift
shift

### wait and change the accelerations
for i in $(seq 2 2 $#)
do
    sleep $1
    echo "echo 0,0,$2,1 > $file"
    echo "0,0,$2,1" > $file
    date +%s.%N
    shift
    shift
done

### wait for accelerated to be finished
wait $pid

### compute and print results
end=$(date +%s%N)
echo "end: " $end
total=$(printf %.10f\\n "$((end-begin))e-9")
echo "actual time: $total"
