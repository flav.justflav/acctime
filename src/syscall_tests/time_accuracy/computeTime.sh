### compute an expected time based on the accelertions
time=$1
usedTime=0

for i in $(seq 2 2 $(($#-1)))
do
    speed=$2
    newtime=$3
    usedTime=$((usedTime+newtime))
    time=$((time-newtime*speed))
    shift
    shift
done

lastSpeed=$2
time=$((1000000000 * time/lastSpeed + 1000000000 * usedTime))
time=$(printf %.10f\\n "$((time))e-9")

echo "target time: $time"