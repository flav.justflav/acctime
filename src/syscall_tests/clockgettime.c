#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <errno.h>

void test(clockid_t id, struct timespec *ts, int expected, int error)
{
    int ret = clock_gettime(id, ts);

    if (ret == expected && error == errno)
        printf("ok\n");
    else
        printf("ERROR\n");
}

int main(int argc, char *argv[])
{
    if (argc > 1 && strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [case]\n");
        return 0;
    }

    struct timespec *ts;
    clockid_t id = CLOCK_REALTIME;

    if (strcmp(argv[1], "OK") == 0)
        test(id, ts, 0, 0);
    
    if (strcmp(argv[1], "EINVAL") == 0)
        test(999, ts, -1, EINVAL);
    
  //  if (strcmp(argv[1], "EINVAL") == 0)


}