#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

void test(struct timespec *req, struct timespec *rem, int expected, int err)
{
    int ret = nanosleep(req, rem);
    if (ret == expected && err == errno)
        printf("ok\n");
    else
        printf("ERROR\n");
}

void sig_handler(int signum) {}

int main(int argc, char *argv[])
{
    if (strcmp(argv[1], "-h") == 0)
    {
        printf("Usage:\n./[executable] [case] [second] [nanoseconds]\n");
        return 0;
    }

    signal(SIGINT, sig_handler);
    struct timespec ts;
    ts.tv_sec = strtoul(argv[2], NULL, 10);
    ts.tv_nsec = strtoul(argv[3], NULL, 10);

    if (strcmp(argv[1], "OK") == 0)
        test(&ts, NULL, 0, 0);
    if (strcmp(argv[1], "EFAULT") == 0)
        test(NULL, NULL, -1, EFAULT);
    if (strcmp(argv[1], "EINVAL") == 0)
        test(&ts, NULL, -1, EINVAL);
    if (strcmp(argv[1], "EINTR") == 0)
    {
        int pid = fork();
        if (pid == 0)
            test(&ts, NULL, -1, EINTR);
        else
        {
            struct timespec tmp;
            tmp.tv_sec = ts.tv_sec / 2;
            tmp.tv_nsec = ts.tv_nsec / 2;
            nanosleep(&tmp, NULL);
            kill(pid, SIGINT);
            waitpid(pid, NULL, 0);
        }
    }
    return 0;
}
