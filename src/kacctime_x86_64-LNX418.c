/**
 * kacctime module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <flav@its-bo.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018     Luc Plisson <lplisson@itsgroup.fr>
 *
 * 2022-09-12 : v.81 -> lnx 4.18 / for DollarU -> pour test 
 * 2022-09-12 : v.82 -> lnx 4.18 / for DollarU / debugfs 
 * 2022-12-13 : v.90 -> lnx 4.18 / tst futex (realtime) -- @flav
 * 2022-12-29 : v.95 -> lnx 3.10 / 4.18 -- @flav
 * 2023-02-05 : v.96 -> add statx syscall -- @flav
 * 2023-05-23 : v.97 -> synchro open/statx -- @flav
 * 2023-06-20 : v.98 -> add global mode / split futex -- @flav
 * 2023-06-22 : v.99 -> kmalloc -> kvmalloc pour le vec des fichers -- @flav
 * 2023-06-23 : v.100 -> fix openat / creat ajout des fichiers -- @jp / @flav
 * 2023-07-04 : v.100 -> fix gettimeofday bug -- @flav
 * 2023-07-06 : v.100 -> fix gettimeofday bug -- @flav
 * 2023-07-06 : v.101 -> add unlinkat -- @flav
 * 2023-10-09 : v.101 -> suppression de ref dans les paramètres -- @flav
 * 2023-10-19 : v.200 -> init cgroup
 * 2023-11-24 : v.300 -> list de cgroup dans debugfs ADD/DEL
 * 2023-11-28 : v.301 -> multi time space
 * 2023-12-28 : v.302 -> correction add file in statxvec / add multiple ts in statax
*/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/uio.h>
#include <linux/futex.h>
#include <linux/cgroup.h>

MODULE_VERSION("2023-12-28 17:17:45 .302");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <flav.justflav@gmail.com>");
MODULE_DESCRIPTION("AccTime LKM");

#define KACCTIME_SYSCALL_SYM	"sys_call_table"

#define CHAR_END_STR	'\0'
#define CHAR_EQUAL	'='

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

/**
 * debugfs pour ajout des ajouts des cgroups
*/
#define KACCTIME_FLAV_CGROUP	"flav_time"
#define KACCTIME_CGROUP_VEC_FILE "acc_cgroup"
static int kacctime_cgroup_vec_len = 128;
module_param(kacctime_cgroup_vec_len, int, 0660);
MODULE_PARM_DESC(kacctime_cgroup_vec_len,
		 "nb de cgroup alterer par defaut (128)");

struct kacctime_cgroup_vec {
	char name[128];
	int ts;
};

static int kacctime_cgroup_vec_max = 0;

static struct kacctime_cgroup_vec *kacctime_cgroup_vec;

/* syscall tables pointers / proto syscall*/
static unsigned long **kacctime_syscall_table;

/* the write_cr0 function cannot be used because of the sensitive cr0 bits pinned by the security issue */
static inline void __write_cr0(unsigned long cr0) 
{
	asm volatile("mov %0,%%cr0" : "+r"(cr0) : : "memory");
}

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int idx;
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
};

static int kacctime_timespace_vec_len = 4;
module_param(kacctime_timespace_vec_len, int, 0660);
MODULE_PARM_DESC(kacctime_timespace_vec_len, "nb d'espace de temps");

#define TS_PREFIX "ts"

static struct kacctime_timespace *kacctime_timespace_tab;

static atomic_t kacctime_sleep_count = ATOMIC_INIT(0);
static atomic_t kacctime_futex_count = ATOMIC_INIT(0);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
#warning "SYSCALL WRAPPER"
static asmlinkage long (*ori_openat)(const struct pt_regs *);
static asmlinkage long (*ori_creat)(const struct pt_regs *);
static asmlinkage long (*ori_nanosleep)(const struct pt_regs *);
static asmlinkage long (*ori_clock_gettime)(const struct pt_regs *);
static asmlinkage long (*ori_gettimeofday)(const struct pt_regs *);
static asmlinkage long (*ori_time)(const struct pt_regs *);
static asmlinkage long (*ori_alarm)(const struct pt_regs *);
static asmlinkage long (*ori_select)(const struct pt_regs *);
static asmlinkage long (*ori_futex)(const struct pt_regs *);
static asmlinkage long (*ori_statx)(const struct pt_regs *);
static asmlinkage long (*ori_unlinkat)(const struct pt_regs *);
#else
#warning "NO SYSCALL WRAPPER (310)"
static asmlinkage long (*ori_openat)(int, const char *, int, mode_t);
static asmlinkage long (*ori_creat)(const char *, mode_t);
static asmlinkage long (*ori_nanosleep)(const struct timespec *,
					struct timespec *);
static asmlinkage long (*ori_clock_gettime)(clockid_t, struct timespec *);
static asmlinkage long (*ori_gettimeofday)(struct timeval *, struct timezone *);
static asmlinkage long (*ori_time)(time_t *);
static asmlinkage long (*ori_alarm)(unsigned int);
static asmlinkage long (*ori_select)(int, fd_set *, fd_set *, fd_set *,
				     struct timeval *);
static asmlinkage long (*ori_futex)(int *, int , int , struct timespec *,
				    int *, int);
static asmlinkage long (*ori_statx)(int dirfd, const char *pathname, int flags,
				    unsigned int mask, struct statx *statxbuf);
static asmlinkage long (*ori_unlinkat)(int dirfd, const char *pathname, int flags);
#endif

/* debugfs */
#define KACCTIME_DEBUGFS_ROOT		"kacctime"
#define DEBUGFS_MAX_FILENAME_LEN	64
#define DEBUGFS_MAX_FILE_SIZE		131072		/* 128 ko */

static struct dentry *kacctime_debugfs_d = NULL;
/*
 * debugfs pour la partie statx
 */
#define KACCTIME_STATXBUF_VEC_FILE	"statxbuf_vec"
static int kacctime_statxbuf_vec_len = 1024;
module_param(kacctime_statxbuf_vec_len, int, 0660);
MODULE_PARM_DESC(kacctime_statxbuf_vec_len, "nb de fichier par defaut (1024)");

static int kacctime_statxbuf_vec_max = 0;

struct kvt_timestamp {
};

struct kacctime_statxbuf {
	char pathname[1024];
	struct statx_timestamp *kts_atime;  /* Last access */
	struct statx_timestamp *kts_btime;  /* Creation */
	struct statx_timestamp *kts_ctime;  /* Last status change */
	struct statx_timestamp *kts_mtime;  /* Last modification */
};

struct kacctime_statxbuf *kacctime_statxbuf_vec;

#define KACCTIME_MAX_LEN_OF_STR_STAXBUF_RECORD (sizeof(struct kacctime_statxbuf) + 4 * (9 + 11) + 7)

/* open */
#define KACCTIME_OPEN_DIRBASE	"/tmp/acctime"

static char *kacctime_open_dirbase = KACCTIME_OPEN_DIRBASE;
static int kacctime_open_dirbase_len = 0;

module_param(kacctime_open_dirbase, charp, 0660);
MODULE_PARM_DESC(kacctime_open_dirbase,
		 "dirbase synchro statx et open defaut: " KACCTIME_OPEN_DIRBASE);

/* Log */
static long     kacctime_log_mode = 0;
module_param(kacctime_log_mode, long, 0660);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/
#define TMP_BUF_MAX_SIZE        1024

#define LOG_NO			(0 << 0)
#define LOG_INIT		(1 << 0)
#define LOG_FUNC		(1 << 1)
#define LOG_CLOCK_GETTIME	(1 << 2)
#define LOG_FUTEX		(1 << 3)
#define LOG_NANO		(1 << 4)
#define LOG_STATX		(1 << 5)
#define LOG_GETTIMEOFDAY	(1 << 6)
#define LOG_OPENAT_CREAT	(1 << 7)
#define LOG_DEBUGFS		(1 << 8)
#define LOG_SEEK		(1 << 9)

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

#define kacctime_log_flags(...)	kacctime_log_func_flags(__func__, __LINE__, \
							current->comm,	\
							current->pid,	\
							__VA_ARGS__)

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
static __inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}

static __inline__ void kacctime_log_func_flags(const char *func,
					       int line, char *comm,
					       int pid, int flags, char *fmt,
					       ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if ((kacctime_log_mode & flags) == 0)
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}
/* fin log */


/* fct de calcul du temps */

static __inline__ void kacctime_timespec_add(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;					
	
	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) +
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_sub(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) -
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_mul(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}				

static __inline__ void kacctime_timespec_div(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{							
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) / f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_frac(struct timespec *fl_op1,
					      int n, int d,
					      struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * n / d;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_virt_to_real(int space, struct timespec *vt)
{
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;

	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(vt, pref, vt);
	kacctime_timespec_sub(vt, pdec, vt);
	kacctime_timespec_frac(vt, divid, multi, vt);
	kacctime_timespec_add(vt, pref, vt);
}

static __inline__ void kacctime_real_to_virt(int space, struct timespec *vt)
{
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;

	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(vt, pref, vt);
	kacctime_timespec_frac(vt, multi, divid, vt);
	kacctime_timespec_add(vt, pref, vt);
	kacctime_timespec_add(vt, pdec, vt);
}

static __inline__ void kacctime_timeval_add(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) +
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_sub(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) -
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
		fl_res->tv_usec = inter % USEC_PER_SEC;
		fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_mul(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_div(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) / f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_frac(struct timeval *fl_op1,
					     int n, int d,
					     struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * n / d;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

/* fin fct calcul tmps */

/* time space param */
static long ts0[4] = {
	0, /* ref time */
	0, /* decalage */
	1, /* multi */
	1  /* div */
};
//static char str_ts0[PAGE_SIZE];

static int read_param(const char *val, const struct kernel_param *kp);

int view_param(char *val, const struct kernel_param *kp)
{
//	int r = param_get_charp(val, kp);
	int r = 0;
	struct timespec64 tp;
	struct timespec64 vtp;

	ktime_get_real_ts64(&tp);
	vtp.tv_sec = tp.tv_sec;
	vtp.tv_nsec = tp.tv_nsec;
	kacctime_real_to_virt(0, (struct timespec *) &vtp);

	r = scnprintf(val, PAGE_SIZE, "0->%s [%lld.%ld]=>[%lld.%ld]\n",
		      *((char **)kp->arg),
		      tp.tv_sec, tp.tv_nsec,
		      vtp.tv_sec, vtp.tv_nsec);
	
	kacctime_log_flags(LOG_INIT, "view call back (%d / [%s])...\n", r, 
			   val);
	kacctime_log_flags(LOG_INIT, "ref: %ld, dec: %ld, fact: %ld/%ld\n",
			   ts0[0], ts0[1], ts0[2], ts0[3]);

	return r;
}

const struct kernel_param_ops param_ops_str_ts0 = {
	.set = &read_param,
	.get = &view_param,
};

// module_param_cb(ts0, &param_ops_str_ts0, str_ts0, 0660);

/* fin time space param */

/**
 * fractionner les sleep
 */ 

#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

static int kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;
module_param(kacctime_sleep_frac_interval, int, 0660);
MODULE_PARM_DESC(kacctime_sleep_frac_interval, "interval de subdivision");

/** kacctime_seek_timespace - 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 */

static int read_param(const char *val, const struct kernel_param *kp) {
	int i = 0;
	struct timespec sref;
	struct timespec sdec;
	struct timespec64 rnow;
	struct timespec vnow;

	int smulti = kacctime_timespace_tab[i].multi;
	int sdivid = kacctime_timespace_tab[i].divid;

	int nmulti;
	int ndivid;
	long dec;

	int r = param_set_charp(val, kp);
	int r_scan;

        kacctime_log_flags(LOG_INIT, "[%s]\n", val);
        r_scan = sscanf(val, "%ld,%ld,%ld\n",
			&ts0[1], &ts0[2], &ts0[3]);

	if (r_scan != 3) {
		pr_warn("err ts0\n");
		return -EINVAL;
	}

	nmulti = ts0[2];
	ndivid = ts0[3];
	dec = ts0[1];


        kacctime_log_flags(LOG_INIT, "readparam r: %ld, d: %ld, fact: %ld/%ld\n",
			   ts0[0], ts0[1], ts0[2], ts0[3]);
		
	if (dec == 0 && nmulti == 0 && ndivid == 0) {
		ktime_get_real_ts64(&rnow);
		
		kacctime_log_flags(LOG_INIT, "readparam init (%ld)\n", ts0[0]);
		kacctime_timespace_tab[i].ref.tv_sec = rnow.tv_sec;
		kacctime_timespace_tab[i].ref.tv_nsec = 0;
		kacctime_timespace_tab[i].dec.tv_sec = 0;
		kacctime_timespace_tab[i].dec.tv_nsec = 0;
		kacctime_timespace_tab[i].multi = 1;
		kacctime_timespace_tab[i].divid = 1;

		return r;
	}

        if (nmulti < 1) {
                nmulti = 1;
        }
        if (ndivid < 1) {
                ndivid = 1;
        }

        if (smulti < 1) {
                smulti = 1;
        }
        if (sdivid < 1) {
                sdivid = 1;
        }

	if (dec != 0) {
		kacctime_timespace_tab[i].dec.tv_sec = dec;
		kacctime_timespace_tab[i].multi = nmulti;
		kacctime_timespace_tab[i].divid = ndivid;
		
		return r;		
	}

	sref.tv_sec = kacctime_timespace_tab[i].ref.tv_sec;
	sref.tv_nsec = kacctime_timespace_tab[i].ref.tv_nsec;
	sdec.tv_sec = kacctime_timespace_tab[i].dec.tv_sec;
	sdec.tv_nsec = kacctime_timespace_tab[i].dec.tv_nsec;

	ktime_get_real_ts64(&rnow);
	kacctime_log_flags(LOG_INIT,
			   "ts0 changed, recalcul [%ld,%ld](%d/%d)\n",
			   rnow.tv_sec, rnow.tv_nsec, smulti, sdivid);
	
	kacctime_log_flags(LOG_INIT, "s=>[%ld,%ld] (%d/%d)",
			   sref.tv_sec, sdec.tv_sec, smulti, sdivid);
	kacctime_log_flags(LOG_INIT, "n=>[%ld,%ld] (%d/%d)",
			   ts0[0], ts0[1], nmulti, ndivid);
	
	vnow.tv_sec = rnow.tv_sec;
	vnow.tv_nsec = rnow.tv_nsec;
	
	kacctime_timespec_sub(&vnow, &sref, &vnow);
	kacctime_timespec_frac(&vnow, smulti, sdivid, &vnow);
	kacctime_timespec_add(&vnow, &sref, &vnow);
	kacctime_timespec_add(&vnow, &sdec, &vnow);
	
	kacctime_log_flags(LOG_INIT,"ts0 changed, recalcul... [%ld,%ld]\n",
			   vnow.tv_sec, vnow.tv_nsec);		
	
	kacctime_timespace_tab[i].ref.tv_sec = rnow.tv_sec;
	ts0[0] = rnow.tv_sec;
	kacctime_timespace_tab[i].ref.tv_nsec = rnow.tv_nsec;
	kacctime_timespec_sub(&vnow, (struct timespec *)&rnow,
			      &(kacctime_timespace_tab[i].dec));
	ts0[1] = kacctime_timespace_tab[i].dec.tv_sec;
	kacctime_timespace_tab[i].multi = nmulti;
	kacctime_timespace_tab[i].divid = ndivid;

	return r;
}

static int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;
	char *cpath_buf;
	int  cpath_ret;
	int cur = 0;

	cpath_buf = kmalloc(PATH_MAX, GFP_KERNEL);
	if (cpath_buf == NULL) {
		kacctime_log_flags(LOG_INIT, "Mem !\n");
		return (-1);
	}
	cpath_ret = task_cgroup_path(current, cpath_buf, PATH_MAX);

	while (cur < kacctime_cgroup_vec_max) {
		if (strstr(cpath_buf, kacctime_cgroup_vec[cur].name) != 0) {
			kacctime_log_flags(LOG_SEEK, "CGROUP : (%d)[%s]\n",
					   cur, cpath_buf);
			kfree(cpath_buf);
			return kacctime_cgroup_vec[cur].ts;
		}
		cur++;
	}
	kfree(cpath_buf);

	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_flags(LOG_INIT, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_flags(LOG_INIT, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
		  
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_flags(LOG_INIT, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}
//				kfree(buf);
//				return 0; /* only deal with ts0 */

				if ((space < 0) ||
				    (space >= kacctime_timespace_vec_len)) {
					kacctime_log_flags(LOG_INIT,
							   "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_flags(LOG_SEEK, "space -> [%d]\n",
						   space);
				kfree(buf);
				if (kacctime_timespace_tab[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	return (-1);
}

/**
   fin  recherche timespace 
*/

/* creation de fichiers */

static int add_file_to_statxvec(int space, char *pathname)
{
	struct timespec64 tp;
	struct timespec ptp;
	int cur = 0;
	
	ktime_get_real_ts64(&tp);

	ptp.tv_sec = tp.tv_sec;
	ptp.tv_nsec = tp.tv_nsec;
	
	kacctime_real_to_virt(space, &ptp);
	
	kacctime_log_flags(LOG_OPENAT_CREAT,
			   "new file at [%s][%lld/%ld]->[%lld/%ld]\n",
			   pathname, tp.tv_sec, tp.tv_nsec,
			   ptp.tv_sec, ptp.tv_nsec);

	while (cur < kacctime_statxbuf_vec_max) {
		if ((strcmp(kacctime_statxbuf_vec[cur].pathname, "") == 0) ||
		    (strcmp(kacctime_statxbuf_vec[cur].pathname, pathname) == 0)) {
			break;
		}
		cur++;
	}
	if (cur == kacctime_statxbuf_vec_len) {
		pr_info("ERR:: add in vec: max (%d)\n",
			kacctime_statxbuf_vec_len);
		return -EINVAL;
	}
	if (cur == kacctime_statxbuf_vec_max) {
		kacctime_statxbuf_vec_max++;
		strcpy(kacctime_statxbuf_vec[cur].pathname, pathname);
	}
	if (strcmp(kacctime_statxbuf_vec[cur].pathname, "") == 0) {
		strcpy(kacctime_statxbuf_vec[cur].pathname, pathname);		
	}

	kacctime_log_flags(LOG_OPENAT_CREAT,
			   "cur max: %d %d\n", cur, kacctime_statxbuf_vec_max);
	kacctime_statxbuf_vec[cur].kts_atime[space].tv_sec = ptp.tv_sec;
	kacctime_statxbuf_vec[cur].kts_btime[space].tv_sec = ptp.tv_sec;
	kacctime_statxbuf_vec[cur].kts_ctime[space].tv_sec = ptp.tv_sec;
	kacctime_statxbuf_vec[cur].kts_mtime[space].tv_sec = ptp.tv_sec;
	kacctime_log_flags(LOG_OPENAT_CREAT,
			   "kacctime_statxbuf_vec: %s %lld:%lld:%lld:%lld\n",
			   kacctime_statxbuf_vec[cur].pathname,
			   kacctime_statxbuf_vec[cur].kts_atime[space].tv_sec,
			   kacctime_statxbuf_vec[cur].kts_btime[space].tv_sec,
			   kacctime_statxbuf_vec[cur].kts_ctime[space].tv_sec,
			   kacctime_statxbuf_vec[cur].kts_mtime[space].tv_sec);
	return 0;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static asmlinkage long kacctime_openat(const struct pt_regs *regs)
#else
static asmlinkage long kacctime_openat(int dirfd, const char *pathname,
				       int flags, mode_t mode)
#endif
{
	int space = kacctime_seek_timespace();
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	char *pathname = (char *) regs->si;
	int flags = regs->dx;
#endif

	if ((space < 0) ||
	    (strncmp(kacctime_open_dirbase, pathname, kacctime_open_dirbase_len)
	     != 0) || ((flags & (O_CREAT | O_WRONLY | O_RDWR | O_APPEND)) == 0 )) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
		return ori_openat(regs);
#else
		return ori_openat(dirfd, pathname, flags, mode);
#endif       
	}

	if (add_file_to_statxvec(space, pathname) != 0) {
		kacctime_log_flags(LOG_OPENAT_CREAT, "ERRR: openat [%s]\n",
				   pathname);		
		return -EINVAL;
	}
	
	kacctime_log_flags(LOG_OPENAT_CREAT, "[%d] openat [%s]\n", space,
			   pathname);
	
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	return ori_openat(regs);
#else
	return ori_openat(dirfd, pathname, flags, mode);
#endif       
}

static int rm_file_to_statxvec(char *pathname) {
	int cur = 0;

	while ((cur < kacctime_statxbuf_vec_max) &&
	       (strcmp(kacctime_statxbuf_vec[cur].pathname, pathname) != 0))
		cur++;
	if (cur == kacctime_statxbuf_vec_max)
		return 0;
	strcpy(kacctime_statxbuf_vec[cur].pathname, "");
	return 0;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static asmlinkage long kacctime_unlinkat(const struct pt_regs *regs)
#else
static asmlinkage long kacctime_unlinkat(int dirfd, const char *pathname,
					 int flags)
#endif
{
	int space = kacctime_seek_timespace();
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	char *pathname = (char *) regs->si;
#endif

	if ((space < 0) ||
	    (strncmp(kacctime_open_dirbase, pathname, kacctime_open_dirbase_len)
	     != 0)) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
		return ori_unlinkat(regs);
#else
		return ori_unlinkat(dirfd, pathname, flags);
#endif
	}

	rm_file_to_statxvec(pathname);
	
	kacctime_log_flags(LOG_OPENAT_CREAT, "[%d] unlink [%s]\n", space,
			   pathname);
	
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	return ori_unlinkat(regs);
#else
	return ori_unlinkat(dirfd, pathname, flags);
#endif       
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static asmlinkage long kacctime_creat(const struct pt_regs *regs)
#else
static asmlinkage long kacctime_creat(const char *pathname, mode_t mode)
#endif
{
	int space = kacctime_seek_timespace();
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	char *pathname = (char *)regs->di;
#endif

	if ((space < 0)  ||
	    (strncmp(kacctime_open_dirbase, pathname, kacctime_open_dirbase_len)
	     != 0)) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
		return ori_creat(regs);
#else
		return ori_creat(pathname, mode);
#endif
	}
	
	kacctime_log_flags(LOG_OPENAT_CREAT, "[%d] creat [%s]\n", space,
			   pathname);

	if (add_file_to_statxvec(space, pathname) != 0) {
		kacctime_log_flags(LOG_OPENAT_CREAT, "ERRR: openat [%s]\n",
				   pathname);		
		return -EINVAL;
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER	
	return ori_creat(regs);
#else
	return ori_creat(pathname, mode);
#endif
}

/* fct de temps */
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static asmlinkage long kacctime_gettimeofday(const struct pt_regs *regs)
#else
static asmlinkage long kacctime_gettimeofday(struct timeval *ptv,
					     struct timezone *ptz)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	struct timeval *tv = (struct timeval *) regs->di;
#else
	struct timeval *tv = ptv;
#endif
	struct timeval ktv;
	struct timeval ref;
	struct timeval dec;
	int multi = 1;
	int divid = 1;

	int space = kacctime_seek_timespace();

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_gettimeofday(regs));
#else
		return (ori_gettimeofday(ptv, ptz));
#endif
	}

	
	do_gettimeofday(&ktv);
	if (copy_to_user(tv, &ktv, sizeof(ktv)))
		return -EFAULT;	

	ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	ref.tv_usec = kacctime_timespace_tab[space].ref.tv_nsec / 1000;
	dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	dec.tv_usec = kacctime_timespace_tab[space].dec.tv_nsec / 1000;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_log_flags(LOG_GETTIMEOFDAY, "[%d] GETTIMEOFDAY ori (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);

	kacctime_timeval_sub(tv, &ref, tv);
	kacctime_timeval_frac(tv, multi, divid, tv);
	kacctime_timeval_add(tv, &ref, tv);
	kacctime_timeval_add(tv, &dec, tv);

	kacctime_log_flags(LOG_GETTIMEOFDAY, "[%d] GETTIMEOFDAY kt  (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);
	
	return 0;
}

/*
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_futex_back(const struct pt_regs *regs)
#else
static long kacctime_futex_back(int *uaddr, int pfutex_op, int val,
			   struct timespec *ptimeout, int *uaddr2,
			   int val3)
#endif
{
	char *filename;
	struct dentry *debug_futex;

	int space = kacctime_seek_timespace();
	int r;

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	int futex_op = regs->si;
#else
	int futex_op = pfutex_op;
#endif

	atomic_inc(&kacctime_futex_count);
	filename = kmalloc(DEBUGFS_MAX_FILENAME_LEN, GFP_KERNEL);

	if (!filename) {
		pr_warn("debugfs / filename: MEM ERR\n");
		r = -ENOMEM;
		goto out_no_debugfs;
	}

	snprintf(filename, DEBUGFS_MAX_FILENAME_LEN, "futex_%s_%d_%d",
		 current->comm, current->pid,
		 atomic_read(&kacctime_futex_count));

	debug_futex = debugfs_create_atomic_t(filename, 0660,
					      kacctime_debugfs_d,
					      &kacctime_futex_count);

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = ori_futex(regs);
		goto out;
#else
		r = ori_futex(uaddr, pfutex_op, val, ptimeout, uaddr2, val3);
		goto out;
#endif
	}

	if (((futex_op & FUTEX_WAIT_BITSET) != 0) &&
	    ((futex_op & FUTEX_CLOCK_REALTIME) != 0)) {
		struct timespec timeout;

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		if ((void *)regs->r10 == NULL) {
			r = ori_futex(regs);
			goto out;
		}
#else
		if (ptimeout == NULL) {
			r =  ori_futex(uaddr, pfutex_op, val,
				       ptimeout, uaddr2, val3);
			goto out;
		}
#endif

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_from_user(&timeout, (struct timespec *) regs->r10,
				   sizeof(timeout));
#else
		r = copy_from_user(&timeout, ptimeout, sizeof(timeout));
#endif
		if (r) {
			pr_warn("copy from user err\n");
			r = -ENOMEM;
			goto out;
		}

		kacctime_log_flags(LOG_FUTEX, "futex VIRT %d: (%ld / %ld)\n",
				   futex_op, timeout.tv_sec, timeout.tv_nsec);

		kacctime_virt_to_real(space, &timeout);

		kacctime_log_flags(LOG_FUTEX, "futex REAL %d: (%ld / %ld)\n",
				   futex_op, timeout.tv_sec, timeout.tv_nsec);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_to_user((struct timespec *) regs->r10, &timeout,
				 sizeof(timeout));
#else
		r = copy_to_user(ptimeout, &timeout, sizeof(timeout));
#endif
		if (r) {
			pr_warn("copy to user err\n");
			r = -ENOMEM;
			goto out;
		}
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = ori_futex(regs);
#else
	r = ori_futex(uaddr, pfutex_op, val,
		      ptimeout, uaddr2, val3);
#endif

out:
	debugfs_remove(debug_futex);
	kfree(filename);

out_no_debugfs:
	atomic_dec(&kacctime_futex_count);
	return r;
}
*/

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static int split_loop(const struct pt_regs *regs)
#else
static int split_loop(int *uaddr, int pfutex_op, int val,
		       struct timespec *ptimeout, int *uaddr2, int val3)
#endif
{
	int r;
	int futex_rc = -ETIMEDOUT;
	struct timespec target_time;
	int space = kacctime_seek_timespace();
	struct timespec current_time;
	struct timespec fractime = {.tv_sec = kacctime_sleep_frac_interval,
				    .tv_nsec = 0};
	int flag = FUTEX_WAIT;
	struct pt_regs pregs;

	memcpy(&pregs, regs, sizeof(pregs));

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = copy_from_user(&target_time, (struct timespec *) regs->r10,
			   sizeof(target_time));
	put_user(flag, (int *) &regs->si);
	pregs.si = FUTEX_WAIT;
#else
	r = copy_from_user(&target_time, ptarget_time, sizeof(target_time));
	put_user(flag, (int *) &pfutex_op);
#endif
	if (r) {
		pr_warn("copy from user err\n");
		r = -ENOMEM;
		return r;
	}

	kacctime_log_flags(LOG_FUTEX, "futex loop init VIRT: (%ld / %ld)\n",
			   target_time.tv_sec, target_time.tv_nsec);
	while (true) {
		struct timespec remainder;
		struct timespec vfractime;

		vfractime.tv_sec = fractime.tv_sec;
		vfractime.tv_nsec = fractime.tv_nsec;
		kacctime_timespec_frac(&vfractime,
				       kacctime_timespace_tab[space].multi,
				       kacctime_timespace_tab[space].divid,
				       &vfractime);

		kacctime_log_flags(LOG_FUTEX,
				   "futex frac/vfrac: (%ld / %ld) -> (%ld / %ld) (op: %d / %d)\n",
				   fractime.tv_sec, fractime.tv_nsec,
				   vfractime.tv_sec, vfractime.tv_nsec,
				   pregs.si, FUTEX_WAIT);

		ktime_get_real_ts64((struct timespec64 *) &current_time);
		kacctime_real_to_virt(space, &current_time);
		remainder = timespec_sub(target_time, current_time);
		if (timespec_valid(&remainder) == false) {
			return futex_rc;
		}
		kacctime_log_flags(LOG_FUTEX,
				   "futex loop VIRT: (%ld / %ld) -> %d\n",
				   remainder.tv_sec, remainder.tv_nsec,
				   timespec_compare(&remainder, &vfractime));
		if (timespec_compare(&remainder, &vfractime) <= 0 ) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
			kacctime_timespec_frac(&remainder,
					       kacctime_timespace_tab[space].divid,
					       kacctime_timespace_tab[space].multi,
					       &remainder);
			
			r = copy_to_user((struct timespec *) pregs.r10,
					 &remainder, sizeof(remainder));
#else
			r = copy_to_user((struct timespec *) ptimeout,
					 &remainder, sizeof(remainder));
#endif
			if (r) {
				pr_warn("futex loop err copy to");
				r = -ENOMEM;
				return r;
			}
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
			futex_rc = ori_futex(&pregs);
#else
			futex_rc = ori_futex(uaddr, futex_op, val,
				      ptimeout, uaddr2, val3);
#endif
			kacctime_log_flags(LOG_FUTEX, "r=%d\n [err: %pe]\n",
					   futex_rc, ERR_PTR(futex_rc));
			return futex_rc;
		}
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_to_user((struct timespec *) pregs.r10,
				 &fractime, sizeof(fractime));
#else
		r = copy_to_user((struct timespec *) ptimeout,
				 &fractime, sizeof(fractime));
#endif
		if (r) {
			pr_warn("futex loop err copy to");
			r = -ENOMEM;
			return r;
		}
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		futex_rc = ori_futex(&pregs);
#else
		futex_rc = ori_futex(uaddr, futex_op, val,
			      ptimeout, uaddr2, val3);		
#endif
		if (futex_rc != -ETIMEDOUT) {
			kacctime_log_flags(LOG_FUTEX, "futex over [%ld]!\n",
					   futex_rc);
			return futex_rc;
		}
	}
	return 0;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_futex(const struct pt_regs *regs)
#else
static long kacctime_futex(int *uaddr, int pfutex_op, int val,
			   struct timespec *ptimeout, int *uaddr2, int val3)
#endif
{
	char *filename;
	struct dentry *debug_futex;
	int space = kacctime_seek_timespace();
	int r;

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	int futex_op = regs->si;
#else
	int futex_op = pfutex_op;
#endif

	atomic_inc(&kacctime_futex_count);
	filename = kmalloc(DEBUGFS_MAX_FILENAME_LEN, GFP_KERNEL);

	if (!filename) {
		pr_warn("debugfs / filename: MEM ERR\n");
		r = -ENOMEM;
		goto out_no_debugfs;
	}

	snprintf(filename, DEBUGFS_MAX_FILENAME_LEN, "futex_%s_%d_%d",
		 current->comm, current->pid,
		 atomic_read(&kacctime_futex_count));

	debug_futex = debugfs_create_atomic_t(filename, 0660,
					      kacctime_debugfs_d,
					      &kacctime_futex_count);

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = ori_futex(regs);
		goto out;
#else
		r = ori_futex(uaddr, pfutex_op, val, ptimeout, uaddr2, val3);
		goto out;
#endif
	}

	if (((futex_op & FUTEX_WAIT_BITSET) != 0) &&
	    ((futex_op & FUTEX_CLOCK_REALTIME) != 0)) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		if ((void *)regs->r10 == NULL) {
			r = ori_futex(regs);
			goto out;
		}
#else
		if (ptimeout == NULL) {
			r =  ori_futex(uaddr, pfutex_op, val,
				       ptimeout, uaddr2, val3);
			goto out;
		}
#endif
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = split_loop(regs);
#else
		r = split_loop(uaddr, pfutex_op, val, ptimeout, uaddr2, val3);
#endif
	} else {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = ori_futex(regs);
#else
		r = ori_futex(uaddr, pfutex_op, val, ptimeout, uaddr2, val3);
#endif
	}
 out:
	debugfs_remove(debug_futex);
	kfree(filename);
	
 out_no_debugfs:
	atomic_dec(&kacctime_futex_count);
	return r;
}

static int kacctime_debug_nanosleep_show(struct seq_file *f, void *data) {
	struct iovec *d = (struct iovec *) f->private;

	seq_write(f, d->iov_base, d->iov_len);
	return 0;
}

static int kacctime_debug_nanosleep_open(struct inode *i, struct file *f) {
	return single_open(f, kacctime_debug_nanosleep_show,
			   i->i_private);
}

static const struct file_operations kacctime_debug_nanosleep_fops = {
	.open = kacctime_debug_nanosleep_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release
};

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_nanosleep(const struct pt_regs *regs)
#else
static long kacctime_nanosleep(struct timespec *req, struct timespec *rem)
#endif
{
/*	struct timespec *rqtp = (struct timespec *) regs->di;*/
	struct timespec *rqtp;

	int space = kacctime_seek_timespace();
	int multi = 1;
	int divid = 1;
	struct timespec *pref;
	struct timespec *pdec;
	long r;
	char *filename;
	struct dentry *debug_nano;
	struct iovec *nano_data;
	/* struct timespec *srqtp; */
	struct timespec *t_final;
	struct timespec *remaining;
	struct timespec *t_cur;

	int go_on = true;
	int idx = 0;

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_nanosleep(regs);
#else
		return ori_nanosleep(req, rem);
#endif
	}

	atomic_inc(&kacctime_sleep_count);

	rqtp = kmalloc(sizeof (*rqtp), GFP_KERNEL);
	if (!rqtp) {
		pr_warn("nano rqtp: MEM ERR\n");
		r = -ENOMEM;
		goto out;
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = copy_from_user(rqtp, (struct timespec *) regs->di, sizeof(*rqtp));
#else
	r = copy_from_user(rqtp, req, sizeof(*rqtp));
#endif
	if (r) {
		pr_warn("nano rqtp: copy user ERR\n");
		r = -ENOMEM;
		goto free_rqtp;
	}

	t_final = kmalloc(sizeof (*t_final), GFP_KERNEL);
	if (!t_final) {
		pr_warn("nano t_final: MEM ERR\n");
		r = -ENOMEM;
		goto free_rqtp;
	}

	filename = kmalloc(DEBUGFS_MAX_FILENAME_LEN, GFP_KERNEL);
	if (!filename) {
		pr_warn("nanosleep / filename: MEM ERR\n");
		r = -ENOMEM;
		goto free_t_final;
	}

	snprintf(filename, DEBUGFS_MAX_FILENAME_LEN, "nano_%s_%d_%d",
		 current->comm, current->pid,
		 atomic_read(&kacctime_sleep_count));

	nano_data = kmalloc(sizeof (*nano_data), GFP_KERNEL);
	if (!nano_data) {
		pr_warn("nanosleep / iovec struct: MEM ERR\n");
		r = -ENOMEM;
		goto free_filename;
	}

	nano_data->iov_base = kmalloc(DEBUGFS_MAX_FILE_SIZE, GFP_KERNEL);
	if (!nano_data->iov_base) {
		pr_warn("nanosleep / iov_base: MEM ERR\n");
		r = -ENOMEM;
		goto free_nano_data;
	}

	debug_nano = debugfs_create_file(filename, 0660, kacctime_debugfs_d,
					 nano_data,
					 &kacctime_debug_nanosleep_fops);
	remaining = kmalloc(sizeof (*remaining), GFP_KERNEL);
	if (!remaining) {
		pr_warn("nano remaining: MEM ERR\n");
		r = -ENOMEM;
		goto free_iovbase;
	}

/*
	debug_nano = debugfs_create_u64(filename, S_IFREG|S_IRUGO,
					kacctime_debugfs,
					(u64 *) & remaining->tv_sec);
*/

	if (!debug_nano) {
		pr_warn("debug nano: ERR create file\n");
	}
	
	t_cur = kmalloc(sizeof (*t_cur), GFP_KERNEL);
	if (!t_cur) {
		pr_warn("nano t_cur: MEM ERR\n");
		r = -ENOMEM;
		goto free_remaining;	       
	}

	ktime_get_real_ts64((struct timespec64*) t_final);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);

	kacctime_timespec_sub(t_final, pref, t_final);
	kacctime_timespec_frac(t_final, multi, divid, t_final);
	kacctime_timespec_add(t_final, pref, t_final);
	kacctime_timespec_add(t_final, pdec, t_final);

	kacctime_timespec_add(t_final, rqtp, t_final);

/*
	nano_data->iov_len = snprintf((char *) nano_data->iov_base,
				      DEBUGFS_MAX_FILE_SIZE,
				      "idx\tcomm\tpid\ttps réel\ttps virt\n\%d\t%s\t%d\t(%ld.%.9ld)\t(-)\n",
				      idx, current->comm, current->pid, 
				      srqtp->tv_sec, srqtp->tv_nsec);
*/
	nano_data->iov_len = snprintf((char *) nano_data->iov_base,
				      DEBUGFS_MAX_FILE_SIZE,
				      "idx\tcomm\tpid\ttps réel\ttps virt\n\%d\t%s\t%d\t(-)\n",
				      idx, current->comm, current->pid);

	while (go_on) {
		ktime_get_real_ts64((struct timespec64 *) t_cur);
		idx++;
		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		pref = &(kacctime_timespace_tab[space].ref);
		pdec = &(kacctime_timespace_tab[space].dec);
		
		kacctime_timespec_sub(t_cur, pref, t_cur);
		kacctime_timespec_frac(t_cur, multi, divid, t_cur);
		kacctime_timespec_add(t_cur, pref, t_cur);
		kacctime_timespec_add(t_cur, pdec, t_cur);

		kacctime_timespec_sub(t_final, t_cur, remaining);

		if (remaining->tv_sec < 0 || remaining->tv_nsec < 0) {
			go_on = false;
			continue;
		}
		if (remaining->tv_sec <= kacctime_sleep_frac_interval) {
			rqtp->tv_sec = remaining->tv_sec;
			rqtp->tv_nsec = remaining->tv_nsec;
			go_on = false;
		} else {
			rqtp->tv_sec = kacctime_sleep_frac_interval;
			rqtp->tv_nsec = 0;
		}

		kacctime_log_flags(LOG_NANO,
				   "NANOSLEEP t_cur:: (%lld.%.9ld) -> t_final(%lld.%.9ld)\n",
				   t_cur->tv_sec, t_cur->tv_nsec,
				   t_final->tv_sec, t_final->tv_nsec);

		kacctime_log_flags(LOG_NANO,
				   "NANOSLEEP (%lld.%.9ld) -> (%lld.%.9ld)\n",
				   remaining->tv_sec, remaining->tv_nsec,
				   rqtp->tv_sec, rqtp->tv_nsec);

/*		nano_data->iov_len =
			snprintf((char *) nano_data->iov_base,
				 10*PAGE_SIZE,
				 "%s%d\t%s\t%d\t(%ld.%.9ld)\t(%ld.%.9ld)\n",
				 (char *)nano_data->iov_base, idx,
				 current->comm, current->pid,
				 remaining->tv_sec, remaining->tv_nsec,
				 rqtp->tv_sec, rqtp->tv_nsec);
*/

		nano_data->iov_len =
			snprintf((char *) nano_data->iov_base,
				 DEBUGFS_MAX_FILE_SIZE,
				 "%s%d\t%s\t%d\t(%ld.%.9ld)\t(%ld.%.9ld)\n",
				 (char *)nano_data->iov_base, idx,
				 current->comm, current->pid,
				 remaining->tv_sec, remaining->tv_nsec,
				 rqtp->tv_sec, rqtp->tv_nsec);

		kacctime_timespec_frac(rqtp, divid, multi, rqtp);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = copy_to_user((struct timespec *) regs->di, rqtp, 
				 sizeof(*rqtp));
#else
		r = copy_to_user(req, rqtp, sizeof(*rqtp));
#endif
		if (r) {
			pr_warn("nano err copy to");
			go_on = false;
		}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		r = ori_nanosleep(regs);
#else
		r = ori_nanosleep(req, rem);
#endif

		if (r < 0) {
			go_on = false;
		}
	}

	kfree(t_cur);

free_remaining:
	kfree(remaining);

free_iovbase:
	kfree(nano_data->iov_base);
	debugfs_remove(debug_nano);

free_nano_data:
	kfree(nano_data);

free_filename:
	kfree(filename);

free_t_final:
	kfree(t_final);

free_rqtp:
	kfree(rqtp);

out:
	atomic_dec(&kacctime_sleep_count);
	return r;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_statx(const struct pt_regs *regs)
#else
static long kacctime_statx(int dirfd, const char *pathname, int flags,
			   unsigned int mask, struct statx *statxbuf)
#endif
{
	int idx = 0;
	long r;

	int space = kacctime_seek_timespace();

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	char *pathname = (char *) regs->si;
	struct statx *statxbuf = (struct statx *) regs->r8;
#endif
	kacctime_log_flags(LOG_STATX, "STATX: %s\n", pathname);

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_statx(regs);
#else
		return ori_statx(dirfd, pathname, flags, mask, statxbuf);
#endif
	}

	while (idx < kacctime_statxbuf_vec_max) {
		if (strcmp(pathname, kacctime_statxbuf_vec[idx].pathname) == 0) {
			break;
		}
		idx++;
	}
	kacctime_log_flags(LOG_STATX, "%d->%s %lld %lld %lld %lld\n", idx,
			   kacctime_statxbuf_vec[idx].pathname,
			   kacctime_statxbuf_vec[idx].kts_atime[space].tv_sec,
			   kacctime_statxbuf_vec[idx].kts_btime[space].tv_sec,
			   kacctime_statxbuf_vec[idx].kts_ctime[space].tv_sec,
			   kacctime_statxbuf_vec[idx].kts_mtime[space].tv_sec);
	
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	r = ori_statx(regs);
#else
	r = ori_statx(dirfd, pathname, flags, mask, statxbuf);
#endif

	if (idx == kacctime_statxbuf_vec_max) 
		return r;

	if (kacctime_statxbuf_vec[idx].kts_atime[space].tv_sec == -1) {
		return r;		
	}
	
	statxbuf->stx_atime.tv_sec = kacctime_statxbuf_vec[idx].kts_atime[space].tv_sec;
	statxbuf->stx_btime.tv_sec = kacctime_statxbuf_vec[idx].kts_btime[space].tv_sec;
	statxbuf->stx_ctime.tv_sec = kacctime_statxbuf_vec[idx].kts_ctime[space].tv_sec;
	statxbuf->stx_mtime.tv_sec = kacctime_statxbuf_vec[idx].kts_mtime[space].tv_sec;

	return r;
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_clock_gettime(const struct pt_regs *regs)
#else
static long kacctime_clock_gettime(clockid_t pclk_id, struct timespec *ptp)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	clockid_t clk_id = regs->di;
	struct timespec *tp = (struct timespec *) regs->si;
#else
	clockid_t clk_id = pclk_id;
	struct timespec *tp = ptp;
#endif
	long ret = 0;
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;
	int space = kacctime_seek_timespace();

	if (clk_id != CLOCK_REALTIME) {
		kacctime_log_flags(LOG_CLOCK_GETTIME,
				   "CLOCK_GETTIME: %d\n", clk_id);
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_clock_gettime(regs);
#else
		return ori_clock_gettime(clk_id, tp);
#endif
	}

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_clock_gettime(regs);
#else
		return ori_clock_gettime(clk_id, tp);
#endif
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		ret = ori_clock_gettime(regs);
#else
		ret = ori_clock_gettime(clk_id, tp);
#endif

	kacctime_log_flags(LOG_FUNC, "[%d] OLD CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(tp, pref, tp);
	kacctime_timespec_frac(tp, multi, divid, tp);
	kacctime_timespec_add(tp, pref, tp);
	kacctime_timespec_add(tp, pdec, tp);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	return (ret);
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static time_t kacctime_time(const struct pt_regs *regs)
#else
static time_t kacctime_time(time_t *pt)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	time_t *t = (time_t *)regs->di;
#else
	time_t *t = pt;
#endif

	time_t t_ori;
	int space = kacctime_seek_timespace();
	struct timespec *pref;
	struct timespec *pdec;
	struct timespec64 tp;
	struct timespec *ptp;

	int multi = 1;
	int divid = 1;

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return ori_time(regs);
#else
		return ori_time(pt);
#endif
	}

	ktime_get_real_ts64(&tp);
	t_ori = tp.tv_sec;

	ptp = (struct timespec *)&tp;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(ptp, pref, ptp);
	kacctime_timespec_frac(ptp, multi, divid, ptp);
	kacctime_timespec_add(ptp, pref, ptp);
	kacctime_timespec_add(ptp, pdec, ptp);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME:[old=%ld:new=%ld]\n",
			   space, t_ori, ptp->tv_sec);

	if (t != NULL) {
		if (put_user(ptp->tv_sec, t)) {
			return -EFAULT;
		}
	}
	return (ptp->tv_sec);
}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_alarm(const struct pt_regs *regs)
#else
static long kacctime_alarm(unsigned int pseconds)
#endif
{
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	unsigned int sec = regs->di;
	unsigned int *psec = (unsigned int *)&(regs->di);
#else
	unsigned int sec = pseconds;
	unsigned int *psec = &pseconds;
#endif
	struct timespec nsec;
	int multi;
	int divid;

	int space = kacctime_seek_timespace();

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_alarm(regs));
#else
		return (ori_alarm(sec));
#endif
	}

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d\n", space, sec); 

	nsec.tv_sec = sec;
	nsec.tv_nsec = 0;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_frac(&nsec, divid, multi, &nsec);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d / ns=%ld\n",
			   space, sec, nsec.tv_sec);

	if (nsec.tv_sec < 1) {
		*psec = 1;
	} else {
		*psec = nsec.tv_sec;
	}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	return (ori_alarm(regs));
#else
	return (ori_alarm(sec));
#endif

}

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
static long kacctime_select(const struct pt_regs *regs)
#else
static long kacctime_select(int pnfds, fd_set *readfds, fd_set *writefds,
			    fd_set *exceptfds, struct timeval *ptimeout)
#endif
{
	int space = kacctime_seek_timespace();
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
	int nfds = regs->di;
	struct timeval *timeout = (struct timeval *) regs->r8;
#else
	int nfds = pnfds;
	struct timeval *timeout = ptimeout;
#endif
	int multi;
	int divid;

	if (timeout == NULL) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	if (nfds > 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	if (space < 0) {
#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   ptimeout));
#endif
	}

	kacctime_log_flags(LOG_FUNC, "[%d] OLD SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timeval_frac(timeout, divid, multi, timeout);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

#ifdef CONFIG_ARCH_HAS_SYSCALL_WRAPPER
		return (ori_select(regs));
#else
		return (ori_select(pnfds, readfds, writefds, exceptfds,
				   timeout));
#endif
}

/* fin des fct de temps */

static void kacctime_override_syscall(void)
{
	unsigned long cr0;

	kacctime_syscall_table = (unsigned long **) 
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_flags(LOG_INIT, "sym addr : [%pK]\n", kacctime_syscall_table);

	if (kacctime_syscall_table == NULL) {
		pr_warn("AIE !!!\n");
	}

	cr0 = read_cr0();
	__write_cr0(cr0 & ~X86_CR0_WP);

	ori_unlinkat = (void *)kacctime_syscall_table[__NR_unlinkat];
	kacctime_syscall_table[__NR_unlinkat] = (unsigned long *)kacctime_unlinkat;
	kacctime_log_flags(LOG_INIT, "takeover: unlinkat (%pK)\n",
			   kacctime_syscall_table[__NR_unlinkat]);

	ori_openat = (void *)kacctime_syscall_table[__NR_openat];
	kacctime_syscall_table[__NR_openat] = (unsigned long *)kacctime_openat;
	kacctime_log_flags(LOG_INIT, "takeover: openat (%pK)\n",
			   kacctime_syscall_table[__NR_openat]);

	ori_creat = (void *)kacctime_syscall_table[__NR_creat];
	kacctime_syscall_table[__NR_creat] = (unsigned long *)kacctime_creat;
	kacctime_log_flags(LOG_INIT, "takeover: creat (%pK)\n",
			   kacctime_syscall_table[__NR_creat]);
	
	ori_statx = (void *)kacctime_syscall_table[__NR_statx];
	kacctime_syscall_table[__NR_statx] = (unsigned long *)kacctime_statx;
	kacctime_log_flags(LOG_INIT, "takeover: statx (%pK)\n",
			   kacctime_syscall_table[__NR_statx]);
	
	ori_futex = (void *)kacctime_syscall_table[__NR_futex];
	kacctime_syscall_table[__NR_futex] = (unsigned long *)kacctime_futex;
	kacctime_log_flags(LOG_INIT, "takeover: futex (%pK)\n",
			   kacctime_syscall_table[__NR_futex]);

	ori_nanosleep = (void *)kacctime_syscall_table[__NR_nanosleep];
	kacctime_syscall_table[__NR_nanosleep] =
		(unsigned long *)kacctime_nanosleep;
	kacctime_log_flags(LOG_INIT, "takeover: nanosleep (%pK)\n",
			   kacctime_syscall_table[__NR_nanosleep]);

	ori_gettimeofday = (void *)kacctime_syscall_table[__NR_gettimeofday];
	kacctime_syscall_table[__NR_gettimeofday] = 
		(unsigned long *)kacctime_gettimeofday;
	kacctime_log_flags(LOG_INIT, "takeover: gettimeofday (%pK)\n",
			   kacctime_syscall_table[__NR_gettimeofday]);
	
	ori_clock_gettime = (void *)kacctime_syscall_table[__NR_clock_gettime];
	kacctime_syscall_table[__NR_clock_gettime] = 
		(unsigned long *)kacctime_clock_gettime;
	kacctime_log_flags(LOG_INIT, "takeover: clock_gettime (%pK)\n",
			   kacctime_syscall_table[__NR_clock_gettime]);

	ori_time = (void *)kacctime_syscall_table[__NR_time];
	kacctime_syscall_table[__NR_time] = (unsigned long *)kacctime_time;
	kacctime_log_flags(LOG_INIT, "takeover: time (%pK)\n",
			   kacctime_syscall_table[__NR_time]);

	ori_alarm = (void *)kacctime_syscall_table[__NR_alarm];
	kacctime_syscall_table[__NR_alarm] = (unsigned long *)kacctime_alarm;
	kacctime_log_flags(LOG_INIT, "takeover: alarm (%pK)\n",
			   kacctime_syscall_table[__NR_alarm]);

	ori_select = (void *)kacctime_syscall_table[__NR_select];
	kacctime_syscall_table[__NR_select] = (unsigned long *)kacctime_select;
	kacctime_log_flags(LOG_INIT, "takeover: select (%pK)\n",
			   kacctime_syscall_table[__NR_select]);
	
	__write_cr0(cr0);
	return;
}

static void kacctime_pullback_syscall(void)
{
	unsigned long cr0;

	cr0 = read_cr0();
	__write_cr0(cr0 & ~X86_CR0_WP);

	kacctime_syscall_table[__NR_unlinkat] = (unsigned long *)ori_unlinkat;
	kacctime_log_flags(LOG_INIT, "getback : unlinkat\n");

	kacctime_syscall_table[__NR_openat] = (unsigned long *)ori_openat;
	kacctime_log_flags(LOG_INIT, "getback : openat\n");

	kacctime_syscall_table[__NR_creat] = (unsigned long *)ori_creat;
	kacctime_log_flags(LOG_INIT, "getback : creat\n");

	kacctime_syscall_table[__NR_statx] = (unsigned long *)ori_statx;
	kacctime_log_flags(LOG_INIT, "getback : statx\n");

	kacctime_syscall_table[__NR_futex] = (unsigned long *)ori_futex;
	kacctime_log_flags(LOG_INIT, "getback : futex\n");

	kacctime_syscall_table[__NR_nanosleep] = (unsigned long *)ori_nanosleep;
	kacctime_log_flags(LOG_INIT, "getback : nanosleep\n");

	kacctime_syscall_table[__NR_gettimeofday] =
		(unsigned long *)ori_gettimeofday;
	kacctime_log_flags(LOG_INIT, "getback : gettimeofday\n");

	kacctime_syscall_table[__NR_clock_gettime] =
		(unsigned long *)ori_clock_gettime;
	kacctime_log_flags(LOG_INIT, "getback : clock_gettime\n");

	kacctime_syscall_table[__NR_time] = (unsigned long *)ori_time;
	kacctime_log_flags(LOG_INIT, "getback : time\n");

	kacctime_syscall_table[__NR_alarm] = (unsigned long *)ori_alarm;
	kacctime_log_flags(LOG_INIT, "getback : alarm\n");

	kacctime_syscall_table[__NR_select] = (unsigned long *)ori_select;
	kacctime_log_flags(LOG_INIT, "getback : select\n");

	__write_cr0(cr0);

	while (atomic_read(&kacctime_sleep_count) > 0) {
		kacctime_log_flags(LOG_INIT, "sleep running: %d\n",
				   atomic_read(&kacctime_sleep_count));
		ssleep(1);
	}

	while (atomic_read(&kacctime_futex_count) > 0) {
		kacctime_log_flags(LOG_INIT, "futex running: %d\n",
				   atomic_read(&kacctime_futex_count));
		ssleep(1);
	}

	return;
}
/* fin override syscall */

ssize_t statx_debugfs_read(struct file *f, char *buf, size_t len, loff_t *offset)
{
	int r = 0;
	int idx = *offset;

	buf[0] = 0;
	kacctime_log_flags(LOG_DEBUGFS, "[%s][%ld][%lld]\n", __func__, len,
			   *offset);

	while (idx < kacctime_statxbuf_vec_max &&
	       (r < (len - KACCTIME_MAX_LEN_OF_STR_STAXBUF_RECORD))) {
		int cur_space = 0;

		if (strcmp(kacctime_statxbuf_vec[idx].pathname, "") == 0) {
			idx++;
			continue;
		}

		while (cur_space < kacctime_timespace_vec_len) {
			if (kacctime_statxbuf_vec[idx].kts_atime[cur_space].tv_sec == -1) {
				cur_space++;
				continue;
			}
			r += sprintf(buf + r, "%d/%d->%s %lld %lld %lld %lld\n",
				     idx, cur_space,
				     kacctime_statxbuf_vec[idx].pathname,
				     kacctime_statxbuf_vec[idx].kts_atime[cur_space].tv_sec,
				     kacctime_statxbuf_vec[idx].kts_btime[cur_space].tv_sec,
				     kacctime_statxbuf_vec[idx].kts_ctime[cur_space].tv_sec,
				     kacctime_statxbuf_vec[idx].kts_mtime[cur_space].tv_sec
				     );
			kacctime_log_flags(LOG_DEBUGFS,"%d/%d->%s %lld %lld %lld %lld\n",
					   idx, cur_space,
					   kacctime_statxbuf_vec[idx].pathname,
					   kacctime_statxbuf_vec[idx].kts_atime[cur_space].tv_sec,
					   kacctime_statxbuf_vec[idx].kts_btime[cur_space].tv_sec,
					   kacctime_statxbuf_vec[idx].kts_ctime[cur_space].tv_sec,
					   kacctime_statxbuf_vec[idx].kts_mtime[cur_space].tv_sec
					   );
			cur_space++;
		}
		idx++;
	}

	*offset = idx;
	return r;
}

ssize_t statx_debugfs_write(struct file *f, const char *buffer, size_t len,
			    loff_t *offset)
{
	int r;
	char pathname[1024];
	struct statx_timestamp kts_atime;  /* Last access */
	struct statx_timestamp kts_btime;  /* Creation */
	struct statx_timestamp kts_ctime;  /* Last status change */
	struct statx_timestamp kts_mtime;  /* Last modification */

	char cmd[4];
	int space = 0;

	pr_info("[%s][%s][%ld]\n", __func__, buffer, len);

	r = sscanf(buffer, "%s %d %s %lld:%lld:%lld:%lld",
		   cmd, &space,  pathname, &kts_atime.tv_sec,
		   &kts_btime.tv_sec,
		   &kts_ctime.tv_sec, &kts_mtime.tv_sec);

	pr_info("%s: %s %s %d %lld:%lld:%lld:%lld\n", __func__, cmd,
		pathname, space, kts_atime.tv_sec, kts_btime.tv_sec,
		kts_ctime.tv_sec, kts_mtime.tv_sec);

	if (strcmp(cmd, "ADD") == 0) {
		int cur = 0;

		pr_info("ADD\n");
		if (r != 7) {
			pr_info("[%s] ADD err sscanf !\n", __func__);
			return -EINVAL;
		}

		while (cur < kacctime_statxbuf_vec_max) {
#warning "PB avec les trous"
			pr_info("[%s]\n", kacctime_statxbuf_vec[cur].pathname);
			if (strcmp(kacctime_statxbuf_vec[cur].pathname, "")
			    == 0) {
				break;
			}
			if (strcmp(kacctime_statxbuf_vec[cur].pathname,
				   pathname) == 0) {
				break;
			}
			cur++;
		}
		if (cur == kacctime_statxbuf_vec_len) {
			pr_info("ERR:: add in vec: max (%d)\n",
				kacctime_statxbuf_vec_len);
			return -EINVAL;
		}
		if (cur == kacctime_statxbuf_vec_max)
			kacctime_statxbuf_vec_max++;

		pr_info("cur max: %d %d\n", cur, kacctime_statxbuf_vec_max);
		strcpy(kacctime_statxbuf_vec[cur].pathname, pathname);
		kacctime_statxbuf_vec[cur].kts_atime[space].tv_sec =
			kts_atime.tv_sec;
		kacctime_statxbuf_vec[cur].kts_btime[space].tv_sec =
			kts_btime.tv_sec;
		kacctime_statxbuf_vec[cur].kts_ctime[space].tv_sec =
			kts_ctime.tv_sec;
		kacctime_statxbuf_vec[cur].kts_mtime[space].tv_sec =
			kts_mtime.tv_sec;

		pr_info("kacctime_statxbuf_vec: %s %lld:%lld:%lld:%lld\n",
			kacctime_statxbuf_vec[cur].pathname,
			kacctime_statxbuf_vec[cur].kts_atime[space].tv_sec,
			kacctime_statxbuf_vec[cur].kts_btime[space].tv_sec,
			kacctime_statxbuf_vec[cur].kts_ctime[space].tv_sec,
			kacctime_statxbuf_vec[cur].kts_mtime[space].tv_sec);

		return len;	
	}

	if (strcmp(cmd, "DEL") == 0) {
		int cur = 0;

		pr_info("DEL [%d] [%s]\n", space, pathname);
		if (r != 3) {
			pr_info("[%s] DEL err sscanf !\n", __func__);
			return -EINVAL;
		}

		while (cur < kacctime_statxbuf_vec_max) {
			int cur_space = 0;
			if (strcmp(kacctime_statxbuf_vec[cur].pathname,
				   pathname) == 0) {
				kacctime_statxbuf_vec[cur].kts_atime[space].tv_sec = -1;
				while (cur_space < kacctime_timespace_vec_len) {
					if (kacctime_statxbuf_vec[cur].kts_atime[cur_space].tv_sec != -1) {
						break;
					}
					cur_space++;
				}
				if (cur_space == kacctime_timespace_vec_len) {
					strcpy(kacctime_statxbuf_vec[cur].pathname, "");					
}
				return (len);
			}
			cur++;
		}
		pr_info("DEL [%s] pas dans la liste\n", pathname);
		return -EINVAL;
	}
	return -EINVAL;
}

const struct file_operations fops_statx_debufgs = {
	.owner = THIS_MODULE,
	.write = statx_debugfs_write,
	.read = statx_debugfs_read,
};

static int debugfs_statx_init(void) {
	int cur_statxbuf = 0;

	kacctime_log_flags(LOG_STATX, "[%s / len_vec=%d]\n", __func__,
			   kacctime_statxbuf_vec_len);
	
	kacctime_statxbuf_vec = kvmalloc(kacctime_statxbuf_vec_len *
					 sizeof(*kacctime_statxbuf_vec),
					 GFP_KERNEL);
	if (kacctime_statxbuf_vec == NULL) {
		pr_info("%s: mem !!", __func__);
		return -ENOMEM;
	}
	memset(kacctime_statxbuf_vec, 0, kacctime_statxbuf_vec_len *
	       sizeof(*kacctime_statxbuf_vec));

	while (cur_statxbuf < kacctime_statxbuf_vec_len) {
		int cur_space = 0;

		kacctime_statxbuf_vec[cur_statxbuf].kts_atime =
			kmalloc(kacctime_timespace_vec_len *
				sizeof(struct statx_timestamp), GFP_KERNEL);
		kacctime_statxbuf_vec[cur_statxbuf].kts_btime =
			kmalloc(kacctime_timespace_vec_len *
				sizeof(struct statx_timestamp), GFP_KERNEL);
		kacctime_statxbuf_vec[cur_statxbuf].kts_ctime =
			kmalloc(kacctime_timespace_vec_len *
				sizeof(struct statx_timestamp), GFP_KERNEL);
		kacctime_statxbuf_vec[cur_statxbuf].kts_mtime =
			kmalloc(kacctime_timespace_vec_len *
				sizeof(struct statx_timestamp), GFP_KERNEL);
		while (cur_space < kacctime_timespace_vec_len) {
			kacctime_statxbuf_vec[cur_statxbuf].kts_atime[cur_space].tv_sec = -1;
			cur_space++;
		}
		cur_statxbuf++;
	}
	
	debugfs_create_file(KACCTIME_STATXBUF_VEC_FILE, 0644, kacctime_debugfs_d,
			    NULL, &fops_statx_debufgs);
	return 0;	
}

ssize_t cgroup_debugfs_read(struct file *f, char *buf, size_t len,
			    loff_t *offset)
{
	int r = 0;
	int idx = *offset;

	buf[0] = 0;
	kacctime_log_flags(LOG_DEBUGFS, "[%s][%ld][%lld]\n",
			   __func__, len, *offset);
	
	while (idx < kacctime_cgroup_vec_max) {
		r += sprintf(buf + r, "%d -> %d %s\n", idx,
			     kacctime_cgroup_vec[idx].ts,
			     kacctime_cgroup_vec[idx].name);
		idx++;
	}
	*offset = idx;
	return r;
}

ssize_t cgroup_debugfs_write(struct file *f, const char *buffer, size_t len,
			     loff_t *offset)
{
	int r;
	char cmd[4];
	char tmp_cgroup[128];
	char *tmp_buf;
	int space;

	tmp_buf = kmalloc(len+1, GFP_KERNEL);
	memset(tmp_buf, 0, len+1);
	memcpy(tmp_buf, buffer, len);

	kacctime_log_flags(LOG_DEBUGFS, "[%s][%s][%ld][%ld]\n", __func__,
			   tmp_buf, len, *offset);

	if (len > 128) {
		pr_warn("kacctime cgroup add :: input too large\n");
		return -EINVAL;
	}

	r = sscanf(tmp_buf, "%s %d %s", cmd, &space, tmp_cgroup);
	kfree(tmp_buf);

	if (strcmp(cmd, "ADD") == 0) {
		int cur = 0;

		if (r != 3) {
			pr_warn("kacctime cgroup add err !!\n");
			return -EINVAL;
		}
		kacctime_log_flags(LOG_DEBUGFS, "ADD %s\n", tmp_cgroup);
		while (cur < kacctime_cgroup_vec_max) {
			if (strcmp(kacctime_cgroup_vec[cur].name, "") == 0) {
				break;
			}
			if (strcmp(kacctime_cgroup_vec[cur].name, tmp_cgroup) == 0) {
				pr_info("info: %s modif alter\n",
					kacctime_cgroup_vec[cur].name);
				break;
			}
			cur++;
		}
		if (cur == kacctime_cgroup_vec_len) {
			pr_info("Err: cgrp add max len (%d)\n",
				kacctime_cgroup_vec_len);
			return -EINVAL;
		}
		if (space < 0 || space > kacctime_timespace_vec_len) {
			pr_info("Err: cgrp add space out of boundaries (%d)\n",
				space);
			return -EINVAL;			
		}
		if (cur == kacctime_cgroup_vec_max)
			kacctime_cgroup_vec_max++;
		strcpy(kacctime_cgroup_vec[cur].name, tmp_cgroup);
		kacctime_cgroup_vec[cur].ts = space;
		return len;
	}

	if (strcmp(cmd, "DEL") == 0) {
		int cur = 0;

		if (r != 2) {
			pr_warn("kacctime cgroup del err !!\n");
			return -EINVAL;
		}
		kacctime_log_flags(LOG_DEBUGFS, "DEL %s\n", tmp_cgroup);
		while (cur < kacctime_cgroup_vec_max) {
			if (strcmp(kacctime_cgroup_vec[cur].name,
				   tmp_cgroup) == 0) {
				strcpy(kacctime_cgroup_vec[cur].name, "");
				if (cur == kacctime_cgroup_vec_max-1) {
					kacctime_cgroup_vec_max--;
				}
				return len;
			}
			cur++;
		}
		pr_info("DEL [%s] pas dans la liste\n", tmp_cgroup);
		return -EINVAL;
	}
	pr_warn("kacctime cgroup:: bad cmd\n");	
	return -EINVAL;
}

const struct file_operations fops_cgroup_debufgs = {
	.owner = THIS_MODULE,
	.write = cgroup_debugfs_write,
	.read = cgroup_debugfs_read,	
};

static int debugfs_cgroup_init(void) {
	kacctime_log_flags(LOG_INIT, "[%s / len_vec=%d / sz: %ld]\n", __func__,
			   kacctime_cgroup_vec_len,
			   kacctime_cgroup_vec_len *
			   sizeof(*kacctime_cgroup_vec));

	kacctime_cgroup_vec = kvmalloc(kacctime_cgroup_vec_len *
				       sizeof(*kacctime_cgroup_vec),
				       GFP_KERNEL);
	if (kacctime_cgroup_vec == NULL) {
		pr_info("%s: mem !!", __func__);
		return -ENOMEM;
	}
	memset(kacctime_cgroup_vec, 0, kacctime_cgroup_vec_len *
	       sizeof(*kacctime_cgroup_vec));
	debugfs_create_file(KACCTIME_CGROUP_VEC_FILE, 0644, kacctime_debugfs_d,
			    NULL, &fops_cgroup_debufgs);

	strcpy(kacctime_cgroup_vec[0].name, KACCTIME_FLAV_CGROUP);
	kacctime_cgroup_vec_max = 1;

	return 0;	
}

ssize_t ts_debugfs_read(struct file *f, char *buf, size_t len, loff_t *offset)
{
	struct kacctime_timespace *ts_cur;
	int r = 0;
	struct timespec64 tp;
	struct timespec64 vtp;
	char data[128];

	memset(data, 0, 128);
	ts_cur = (struct kacctime_timespace *) f->f_inode->i_private;

	ktime_get_real_ts64(&tp);
	vtp.tv_sec = tp.tv_sec;
	vtp.tv_nsec = tp.tv_nsec;

	kacctime_real_to_virt(ts_cur->idx, (struct timespec *) &vtp);
	
	kacctime_log_flags(LOG_DEBUGFS, "[%s][%ld][%lld]\n", __func__, len,
			   *offset);
	r = sprintf(data, "%d->%ld,%d,%d [%lld.%ld]=>[%lld.%ld]\n",
		    ts_cur->idx, ts_cur->dec.tv_sec,
		    ts_cur->multi, ts_cur->divid,
		    tp.tv_sec, tp.tv_nsec,
		    vtp.tv_sec, vtp.tv_nsec);

	return simple_read_from_buffer(buf, len, offset, data, 128);
}

ssize_t ts_debugfs_write(struct file *f, const char *buffer, size_t len,
			     loff_t *offset)
{
	char *tmp_buf;
	int nmulti;
	int ndivid;
	int smulti;
	int sdivid;
	long dec;
	int r;
	struct kacctime_timespace *ts_cur;
	struct timespec sref;
	struct timespec sdec;
	struct timespec64 rnow;
	struct timespec vnow;

	ts_cur = (struct kacctime_timespace *) f->f_inode->i_private;	

	tmp_buf = kmalloc(len+1, GFP_KERNEL);
	memset(tmp_buf, 0, len+1);
	memcpy(tmp_buf, buffer, len);

	kacctime_log_flags(LOG_DEBUGFS, "[%s][%s][%ld][%ld]\n", __func__,
			   tmp_buf, len, *offset);

	if (len > 128) {
		pr_warn("kacctime ts :: input too large\n");
		return -EINVAL;
	}

	r = sscanf(tmp_buf, "%ld,%d,%d", &dec, &nmulti, &ndivid);
	kfree(tmp_buf);

	if (r != 3) {
		kacctime_log_flags(LOG_DEBUGFS, "err ts param !");
		return -EINVAL;
	}

	if (dec == 0 && nmulti == 0 && ndivid == 0) {
		ktime_get_real_ts64(&rnow);
		kacctime_log_flags(LOG_INIT, "init space: %d\n", ts_cur->idx);
		ts_cur->ref.tv_sec = rnow.tv_sec;
		ts_cur->ref.tv_nsec = 0;
		ts_cur->dec.tv_sec = 0;
		ts_cur->dec.tv_nsec = 0;
		ts_cur->multi = 1;
		ts_cur->divid = 1;

		return len;
	}

	if (ndivid < 1) {
		return -EINVAL;
	}

	if (nmulti < 1) {
		return -EINVAL;
	}

	if (dec != 0) {
		ts_cur->dec.tv_sec = dec;
		ts_cur->multi = nmulti;
		ts_cur->divid = ndivid;
		
		return len;		
	}

	sref.tv_sec = ts_cur->ref.tv_sec;
	sref.tv_nsec = ts_cur->ref.tv_nsec;
	sdec.tv_sec = ts_cur->dec.tv_sec;
	sdec.tv_nsec = ts_cur->dec.tv_nsec;
	smulti = ts_cur->multi;
	sdivid = ts_cur->divid;

	ktime_get_real_ts64(&rnow);
	kacctime_log_flags(LOG_INIT, "ts%d changed, recalcul [%ld,%ld](%d/%d)\n",
			   ts_cur->idx, rnow.tv_sec, rnow.tv_nsec,
			   smulti, sdivid);
	
	
	vnow.tv_sec = rnow.tv_sec;
	vnow.tv_nsec = rnow.tv_nsec;
	
	kacctime_timespec_sub(&vnow, &sref, &vnow);
	kacctime_timespec_frac(&vnow, smulti, sdivid, &vnow);
	kacctime_timespec_add(&vnow, &sref, &vnow);
	kacctime_timespec_add(&vnow, &sdec, &vnow);
	
	kacctime_log_flags(LOG_INIT,"ts%d changed, recalcul... [%ld,%ld]\n",
			   ts_cur->idx, vnow.tv_sec, vnow.tv_nsec);		
	
	ts_cur->ref.tv_sec = rnow.tv_sec;
	ts_cur->ref.tv_nsec = rnow.tv_nsec;
	kacctime_timespec_sub(&vnow, (struct timespec *)&rnow, &(ts_cur->dec));

	ts_cur->multi = nmulti;
	ts_cur->divid = ndivid;
	
	return len;
}

const struct file_operations fops_ts_debufgs = {
	.owner = THIS_MODULE,
	.write = ts_debugfs_write,
	.read = ts_debugfs_read,	
};

static int debugfs_ts_init(void) {
	int idx = 0;
	struct timespec64 ts;

	kacctime_log_flags(LOG_INIT, "[%s / len_vec=%d / sz: %ld]\n", __func__,
			   kacctime_timespace_vec_len,
			   kacctime_timespace_vec_len *
			   sizeof(*kacctime_timespace_tab));

	kacctime_timespace_tab = kmalloc(kacctime_timespace_vec_len *
					 sizeof(*kacctime_timespace_tab),
					 GFP_KERNEL);
	if (kacctime_timespace_tab == NULL) {
		pr_info("%s: mem !!", __func__);
		return -ENOMEM;
	}

	ktime_get_real_ts64(&ts);
	while (idx < kacctime_timespace_vec_len) {
		char ts_name[128];

		kacctime_timespace_tab[idx].idx = idx;
		kacctime_timespace_tab[idx].multi = 1;
		kacctime_timespace_tab[idx].divid = 1;
		kacctime_timespace_tab[idx].ref.tv_sec = ts.tv_sec;
		kacctime_timespace_tab[idx].ref.tv_nsec = 0;
		kacctime_timespace_tab[idx].dec.tv_sec = 0;
		kacctime_timespace_tab[idx].dec.tv_nsec = 0;
		snprintf(ts_name, 127, "%s%d", TS_PREFIX, idx);		
		debugfs_create_file(ts_name, 0644, kacctime_debugfs_d,
				    &kacctime_timespace_tab[idx],
				    &fops_ts_debufgs);
		idx++;
	}
	return 0;
}

static int kacctime_mk_debugfs(void)
{
	int r = 0;
	
	kacctime_log_flags(LOG_INIT, "creation debugfs\n");
	kacctime_debugfs_d = debugfs_create_dir(KACCTIME_DEBUGFS_ROOT, NULL);
	debugfs_create_atomic_t("sleep_running", S_IFREG | S_IRUGO,
				kacctime_debugfs_d,
				&kacctime_sleep_count);
	debugfs_create_atomic_t("futex_running", S_IFREG | S_IRUGO,
				kacctime_debugfs_d,
				&kacctime_futex_count);

	r -= debugfs_ts_init();
	r -= debugfs_statx_init();
	r -= debugfs_cgroup_init();

	return r;
}

static void kacctime_rm_debugfs(void)
{
	kacctime_log_flags(LOG_INIT, "suppression debugfs\n");
	debugfs_remove_recursive(kacctime_debugfs_d);
}
/* fin debugfs */

/* Main */
static int __init kacctime_init(void)
{
	int r;
//	struct timespec64 ts;

	kacctime_log_flags(LOG_INIT, "init kacctime_log_mode: %d\n",
			   kacctime_log_mode);

	r = kacctime_mk_debugfs();

	if (r < 0) {
		kacctime_log_flags(LOG_INIT, "init kacctime fail\n",
				   kacctime_log_mode);
		return -1;
	}
	kacctime_open_dirbase_len = strlen(kacctime_open_dirbase);
	kacctime_override_syscall();

	return (0);
}

static void __exit kacctime_exit(void)
{
	kacctime_pullback_syscall();
	kacctime_rm_debugfs();
	kacctime_log_flags(LOG_INIT, "exit kacctime_log_mode: %d\n",
			   kacctime_log_mode);
}

module_init(kacctime_init);
module_exit(kacctime_exit);
