/**
 * kacctime module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <fastraud@itsgroup.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018-     Luc Plisson <lplisson@itsgroup.fr>
 *
 * 2022-09-12 : v.81 -> lnx 4.18 / for DollarU -> pour test
 * 2022-09-12 : v.82 -> lnx 4.18 / for DollarU / debugfs
*/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/debugfs.h>
#include <linux/slab.h>
#include <linux/uio.h>

MODULE_VERSION("418 2022-12-06 10:35:31 .82");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <flav@itsgroup.com>");
MODULE_DESCRIPTION("AccTime LKM");

#define KACCTIME_SYSCALL_SYM	"sys_call_table"

#define CHAR_END_STR	'\0'
#define CHAR_EQUAL	'='

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

/* syscall tables pointers / proto syscall*/
typedef asmlinkage long (*sys_call_ptr_t)(const struct pt_regs *);
static sys_call_ptr_t *kacctime_syscall_table;

#define	KACCTIME_MAX_SPACE	1

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
};

static struct kacctime_timespace kacctime_timespace_tab[KACCTIME_MAX_SPACE];

static atomic_t kacctime_sleep_count = ATOMIC_INIT(0);

sys_call_ptr_t ori_nanosleep;
sys_call_ptr_t ori_clock_gettime;
sys_call_ptr_t ori_gettimeofday;
sys_call_ptr_t ori_time;
sys_call_ptr_t ori_alarm;
sys_call_ptr_t ori_select;
sys_call_ptr_t ori_clock_nanosleep;
sys_call_ptr_t ori_wait4;

/* debugfs */
#define KACCTIME_DEBUGFS_ROOT		"kacctime"
#define DEBUGFS_MAX_FILENAME_LEN	64
//#define DEBUGFS_MAX_FILE_SIZE		131072		/* 128 ko */
#define DEBUGFS_MAX_FILE_SIZE		1024

static struct dentry *kacctime_debugfs = NULL;

/* Log */
static long     kacctime_log_mode = 0;
module_param(kacctime_log_mode, long, 0660);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/
#define TMP_BUF_MAX_SIZE        1024

#define LOG_NO			(0 << 0)
#define LOG_INIT		(1 << 0)
#define LOG_FUNC		(1 << 1)
#define LOG_CLOCK_GETTIME	(1 << 2)
#define LOG_ALL		(LOG_NO | LOG_ON | LOG_FUNC)

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

#define kacctime_log_flags(...)	kacctime_log_func_flags(__func__, __LINE__, \
							current->comm,	\
							current->pid,	\
							__VA_ARGS__)

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
static __inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}

static __inline__ void kacctime_log_func_flags(const char *func,
					       int line, char *comm,
					       int pid, int flags, char *fmt,
					       ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if ((kacctime_log_mode & flags) == 0)
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	pr_info("%s", tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	pr_cont("%s", tmp_buf);
	return;
}
/* fin log */


/* fct de calcul du temps */

static __inline__ void kacctime_timespec_add(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;					
	
	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) +
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_sub(struct timespec *fl_op1,
					     struct timespec *fl_op2,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) -
		(fl_op2->tv_sec * NSEC_PER_SEC + fl_op2->tv_nsec);
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_mul(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}				

static __inline__ void kacctime_timespec_div(struct timespec *fl_op1,
					     int f,
					     struct timespec *fl_res)
{							
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) / f;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timespec_frac(struct timespec *fl_op1,
					      int n, int d,
					      struct timespec *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * NSEC_PER_SEC + fl_op1->tv_nsec) * n / d;
	fl_res->tv_nsec = inter % NSEC_PER_SEC;
	fl_res->tv_sec = inter / NSEC_PER_SEC;
}

static __inline__ void kacctime_timeval_add(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) +
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_sub(struct timeval *fl_op1,
					    struct timeval *fl_op2,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) -
		(fl_op2->tv_sec * USEC_PER_SEC + fl_op2->tv_usec);
		fl_res->tv_usec = inter % USEC_PER_SEC;
		fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_mul(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_div(struct timeval *fl_op1,
					    int f,
					    struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) / f;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

static __inline__ void kacctime_timeval_frac(struct timeval *fl_op1,
					     int n, int d,
					     struct timeval *fl_res)
{
	long long int inter;

	inter = (fl_op1->tv_sec * USEC_PER_SEC + fl_op1->tv_usec) * n / d;
	fl_res->tv_usec = inter % USEC_PER_SEC;
	fl_res->tv_sec = inter / USEC_PER_SEC;
}

/* fin fct calcul tmps */

/* time space param */
static long ts0[4] = {
	0, /* ref time */
	0, /* decalage */
	0, /* multi */
	0  /* div */
};

static int arr_argc = 0;

module_param_array(ts0, long, &arr_argc, 0660); 
MODULE_PARM_DESC(ts0, "ref,dec,mul,div");
/* fin time space param */

/**
 * fractionner les sleep
 */ 

#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

static int	kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;
module_param(kacctime_sleep_frac_interval, int, 0660);
MODULE_PARM_DESC(kacctime_sleep_frac_interval, "interval de subdivision");

/** kacctime_seek_timespace - 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 */

static void read_param(void) {
	int i = 0;
	struct timespec sref;
	struct timespec sdec;
	struct timespec nref;
	struct timespec ndec;

	int smulti = kacctime_timespace_tab[i].multi;
	int sdivid = kacctime_timespace_tab[i].divid;

	int nmulti = ts0[2];
	int ndivid = ts0[3];
	long ref = ts0[0];
	long dec = ts0[1];

	if (ref != 0) {
		kacctime_timespace_tab[i].ref.tv_sec = ref;
		kacctime_timespace_tab[i].ref.tv_nsec = 0;
		kacctime_timespace_tab[i].dec.tv_sec = dec;
		kacctime_timespace_tab[i].dec.tv_nsec = 0;
		kacctime_timespace_tab[i].multi = nmulti;
		kacctime_timespace_tab[i].divid = ndivid;

		return;
	}

	if (nmulti < 1) {
		nmulti = 1;
	}
	if (ndivid < 1) {
		ndivid = 1;
	}

	if (smulti < 1) {
		smulti = 1;
	}
	if (sdivid < 1) {
		sdivid = 1;
	}

	sref.tv_sec = kacctime_timespace_tab[i].ref.tv_sec;
	sref.tv_nsec = 0;
	sdec.tv_sec = kacctime_timespace_tab[i].dec.tv_sec;
	sdec.tv_nsec = 0;

	nref.tv_sec = ts0[0];
	nref.tv_nsec = 0;
	ndec.tv_sec = ts0[1];
	ndec.tv_nsec = 0;

	if ((smulti != nmulti) ||
	    (sdivid != ndivid) ||
	    ((nref.tv_sec != 0) && (sref.tv_sec != nref.tv_sec)) ||
	    ((nref.tv_sec != 0) && (ndec.tv_sec != 0) &&
	     (sdec.tv_sec != ndec.tv_sec))
	    ) {
		struct timespec64 rnow;
		struct timespec vnow;

		ktime_get_real_ts64(&rnow);
		kacctime_log_flags(LOG_INIT,
				   "ts0 changed, recalcul [%ld,%ld](%d/%d)\n",
				   rnow.tv_sec, rnow.tv_nsec, smulti, sdivid);
		
		kacctime_log_flags(LOG_INIT, "s=>[%ld,%ld] (%d/%d)",
				   sref.tv_sec, sdec.tv_sec, smulti, sdivid);
		kacctime_log_flags(LOG_INIT, "n=>[%ld,%ld] (%d/%d)",
				   nref.tv_sec, ndec.tv_sec, nmulti, ndivid);

		vnow.tv_sec = rnow.tv_sec;
		vnow.tv_nsec = rnow.tv_nsec;

		kacctime_timespec_sub(&vnow, &sref, &vnow);
		kacctime_timespec_frac(&vnow, smulti, sdivid, &vnow);
		kacctime_timespec_add(&vnow, &sref, &vnow);
		kacctime_timespec_add(&vnow, &sdec, &vnow);

		kacctime_log_flags(LOG_INIT,"ts0 changed, recalcul... [%ld,%ld]\n",
				   vnow.tv_sec, vnow.tv_nsec);		

		kacctime_timespace_tab[i].ref.tv_sec = rnow.tv_sec;
		ts0[0] = rnow.tv_sec;
		kacctime_timespace_tab[i].ref.tv_nsec = rnow.tv_nsec;
		kacctime_timespec_sub(&vnow, (struct timespec *)&rnow,
				      &(kacctime_timespace_tab[i].dec));
		ts0[1] = kacctime_timespace_tab[i].dec.tv_sec;
		kacctime_timespace_tab[i].multi = nmulti;
		kacctime_timespace_tab[i].divid = ndivid;
	}
}

static int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;

	read_param();

	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_flags(LOG_INIT, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_flags(LOG_INIT, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
		  
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_flags(LOG_INIT, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}

				return 0; /* only deal with ts0 */

				if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
					kacctime_log_flags(LOG_INIT,
							   "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_level(5, "space -> [%d]\n", space);
				kfree(buf);
				if (kacctime_timespace_tab[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	if (kacctime_timespace_tab[0].multi == 0)    
		return (-1);
	return (-1);
}

/**
   fin  recherche timespace 
*/

/* fct de temps */

static long kacctime_gettimeofday(const struct pt_regs *regs)
{
	struct timeval *tv = (struct timeval *) regs->di;
	struct timeval ref;
	struct timeval dec;
	int multi = 1;
	int divid = 1;

	int space = kacctime_seek_timespace();

	if (space < 0) {
		return (ori_gettimeofday(regs));
	}

	do_gettimeofday(tv);

	ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	ref.tv_usec = kacctime_timespace_tab[space].ref.tv_nsec / 1000;
	dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	dec.tv_usec = kacctime_timespace_tab[space].dec.tv_nsec / 1000;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_log_flags(LOG_FUNC, "[%d] GETTIMEOFDAY ori (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);

	kacctime_timeval_sub(tv, &ref, tv);
	kacctime_timeval_frac(tv, multi, divid, tv);
	kacctime_timeval_add(tv, &ref, tv);
	kacctime_timeval_add(tv, &dec, tv);

	kacctime_log_flags(LOG_FUNC, "[%d] GETTIMEOFDAY kt  (%ld/%ld)\n",
			   space, tv->tv_sec, tv->tv_usec);
	
	return 0;
}

static int kacctime_debug_nanosleep_show(struct seq_file *f, void *data) {
	struct iovec *d = (struct iovec *) f->private;

	seq_write(f, d->iov_base, d->iov_len);
	return 0;
}

static int kacctime_debug_nanosleep_open(struct inode *i, struct file *f) {
	return single_open(f, kacctime_debug_nanosleep_show,
			   i->i_private);
}

static const struct file_operations kacctime_debug_nanosleep_fops = {
	.open = kacctime_debug_nanosleep_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release
};

static long kacctime_nanosleep(const struct pt_regs *regs)
{
	struct timespec *rqtp = (struct timespec *) regs->di;
	int space = kacctime_seek_timespace();
	int multi = 1;
	int divid = 1;
	struct timespec *pref;
	struct timespec *pdec;
	long r;
	char *filename;
	struct dentry *debug_nano;
	struct iovec *nano_data;
	struct timespec *srqtp;
	struct timespec *t_final;
	struct timespec *remaining;
	struct timespec *t_cur;

	int go_on = true;
	int idx = 0;

	if (space < 0) {
		return ori_nanosleep(regs);
	}

	atomic_inc(&kacctime_sleep_count);

	srqtp = kmalloc(sizeof (*srqtp), GFP_KERNEL);
	if (!srqtp) {
		pr_warn("nano srqtp: MEM ERR\n");
		r = -ENOMEM;
		goto out;
	}

	t_final = kmalloc(sizeof (*t_final), GFP_KERNEL);
	if (!t_final) {
		pr_warn("nano t_final: MEM ERR\n");
		r = -ENOMEM;
		goto free_srqtp;
	}

	srqtp->tv_sec = rqtp->tv_sec;
	srqtp->tv_nsec = rqtp->tv_nsec;

	filename = kmalloc(DEBUGFS_MAX_FILENAME_LEN, GFP_KERNEL);
	if (!filename) {
		pr_warn("nanosleep / filename: MEM ERR\n");
		r = -ENOMEM;
		goto free_t_final;
	}

	snprintf(filename, DEBUGFS_MAX_FILENAME_LEN, "nano_%s_%d_%d",
		 current->comm, current->pid,
		 atomic_read(&kacctime_sleep_count));

	nano_data = kmalloc(sizeof (*nano_data), GFP_KERNEL);
	if (!nano_data) {
		pr_warn("nanosleep / iovec struct: MEM ERR\n");
		r = -ENOMEM;
		goto free_filename;
	}

	nano_data->iov_base = kmalloc(DEBUGFS_MAX_FILE_SIZE, GFP_KERNEL);
	if (!nano_data->iov_base) {
		pr_warn("nanosleep / iov_base: MEM ERR\n");
		r = -ENOMEM;
		goto free_nano_data;
	}
		
	debug_nano = debugfs_create_file(filename, 0660, kacctime_debugfs,
					 nano_data,
					 &kacctime_debug_nanosleep_fops);
	if (!debug_nano) {
		pr_warn("debug nano: ERR create file\n");
	}
	
	remaining = kmalloc(sizeof (*remaining), GFP_KERNEL);
	if (!remaining) {
		pr_warn("nano remaining: MEM ERR\n");
		r = -ENOMEM;
		goto free_iovbase;
	}

	t_cur = kmalloc(sizeof (*t_cur), GFP_KERNEL);
	if (!t_cur) {
		pr_warn("nano t_cur: MEM ERR\n");
		r = -ENOMEM;
		goto free_remaining;	       
	}

	ktime_get_real_ts64((struct timespec64*) t_final);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);

	kacctime_timespec_sub(t_final, pref, t_final);
	kacctime_timespec_frac(t_final, multi, divid, t_final);
	kacctime_timespec_add(t_final, pref, t_final);
	kacctime_timespec_add(t_final, pdec, t_final);

	kacctime_timespec_add(t_final, srqtp, t_final);

	nano_data->iov_len = snprintf((char *) nano_data->iov_base,
				      DEBUGFS_MAX_FILE_SIZE,
				      "idx\tcomm\tpid\ttps réel\ttps virt\n\%d\t%s\t%d\t(%ld.%.9ld)\t(-)\n",
				      idx, current->comm, current->pid, 
				      srqtp->tv_sec, srqtp->tv_nsec);
	while (go_on) {
		ktime_get_real_ts64((struct timespec64 *) t_cur);
		idx++;
		read_param();
		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		pref = &(kacctime_timespace_tab[space].ref);
		pdec = &(kacctime_timespace_tab[space].dec);
		
		kacctime_timespec_sub(t_cur, pref, t_cur);
		kacctime_timespec_frac(t_cur, multi, divid, t_cur);
		kacctime_timespec_add(t_cur, pref, t_cur);
		kacctime_timespec_add(t_cur, pdec, t_cur);

		kacctime_timespec_sub(t_final, t_cur, remaining);

		kacctime_log_flags(LOG_FUNC,
				   "NANOSLEEP DEBUG (%lld.%.9ld) -> (%lld.%.9ld) -> (%lld.%.9ld)\n",
				   t_final->tv_sec, t_final->tv_nsec,
				   t_cur->tv_sec, t_cur->tv_nsec,
				   srqtp->tv_sec, srqtp->tv_nsec);

		if (remaining->tv_sec < 0 || remaining->tv_nsec < 0) {
			go_on = false;
			continue;
		}
		if (remaining->tv_sec <= kacctime_sleep_frac_interval) {
			rqtp->tv_sec = remaining->tv_sec;
			rqtp->tv_nsec = remaining->tv_nsec;
			go_on = false;
		} else {
			rqtp->tv_sec = kacctime_sleep_frac_interval;
			rqtp->tv_nsec = 0;
		}
		kacctime_log_flags(LOG_FUNC,
				   "NANOSLEEP (%lld.%.9ld) -> (%lld.%.9ld)\n",
				   remaining->tv_sec, remaining->tv_nsec,
				   rqtp->tv_sec, rqtp->tv_nsec);

		nano_data->iov_len =
			snprintf((char *) nano_data->iov_base,
				 10*PAGE_SIZE,
				 "%s%d\t%s\t%d\t(%ld.%.9ld)\t(%ld.%.9ld)\n",
				 (char *)nano_data->iov_base, idx,
				 current->comm, current->pid,
				 remaining->tv_sec, remaining->tv_nsec,
				 rqtp->tv_sec, rqtp->tv_nsec);

		kacctime_timespec_frac(rqtp, divid, multi, rqtp);

		r = ori_nanosleep(regs);
		if (r < 0) {
			go_on = false;
		}
	}

	kfree(t_cur);

free_remaining:
	kfree(remaining);

free_iovbase:
	kfree(nano_data->iov_base);
	debugfs_remove(debug_nano);

free_nano_data:
	kfree(nano_data);

free_filename:
	kfree(filename);

free_t_final:
	kfree(t_final);

free_srqtp:
	kfree(srqtp);

out:
	atomic_dec(&kacctime_sleep_count);
	return r;
}

static long kacctime_wait4(const struct pt_regs *regs)
{
	pid_t pid = regs->di;
	int *wstatus = (int *) regs->si;
	int options = regs->dx;
	struct rusage *rusage = (struct rusage *) regs->r10;
	long retval;

	retval = ori_wait4(regs);
	kacctime_log_flags(LOG_FUNC, "WAIT4: %d\n", pid);
	if (rusage != NULL) {
		kacctime_log_flags(LOG_INIT, "WAIT4: pid: %d : (%ld %ld)\n",
				   pid, rusage->ru_utime.tv_sec,
				   rusage->ru_utime.tv_usec);
	}
	return retval;
}

static long kacctime_clock_nanosleep(const struct pt_regs *regs)
{
	clockid_t clock_id = regs->di;
	int flags = regs->si;
	struct timespec *request = (struct timespec *) regs->dx;
	struct timespec *remain = (struct timespec *) regs->r10;

	kacctime_log_flags(LOG_INIT,
			   "CLOCK_NANOSLEEP: %d\n", clock_id);
	return ori_clock_nanosleep(regs);
}

static long kacctime_clock_gettime(const struct pt_regs *regs)
{
	clockid_t clk_id = regs->di;
	struct timespec *tp = (struct timespec *) regs->si;
	long ret = 0;
	struct timespec *pref;
	struct timespec *pdec;
	int multi = 1;
	int divid = 1;
	int space = kacctime_seek_timespace();

	if (clk_id != CLOCK_REALTIME) {
		kacctime_log_flags(LOG_CLOCK_GETTIME,
				   "CLOCK_GETTIME: %d\n", clk_id);
		return ori_clock_gettime(regs);		
	}

	if (space < 0) {
		return ori_clock_gettime(regs);
	}

	ret = ori_clock_gettime(regs);

	kacctime_log_flags(LOG_FUNC, "[%d] OLD CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(tp, pref, tp);
	kacctime_timespec_frac(tp, multi, divid, tp);
	kacctime_timespec_add(tp, pref, tp);
	kacctime_timespec_add(tp, pdec, tp);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW CLOCK_GETTIME (%d : %ld/%ld)\n",
			   space, clk_id, tp->tv_sec, tp->tv_nsec);
	
	return (ret);
}

static time_t kacctime_time(const struct pt_regs *regs)
{
	time_t *t = (time_t *)regs->di;
	time_t t_ori;
	int space = kacctime_seek_timespace();
	struct timespec *pref;
	struct timespec *pdec;
	struct timespec64 tp;
	struct timespec *ptp;

	int multi = 1;
	int divid = 1;

	if (space < 0) {
		return ori_time(regs);
	}

	ktime_get_real_ts64(&tp);
	t_ori = tp.tv_sec;

	ptp = (struct timespec *)&tp;
	pref = &(kacctime_timespace_tab[space].ref);
	pdec = &(kacctime_timespace_tab[space].dec);
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_sub(ptp, pref, ptp);
	kacctime_timespec_frac(ptp, multi, divid, ptp);
	kacctime_timespec_add(ptp, pref, ptp);
	kacctime_timespec_add(ptp, pdec, ptp);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME:[old=%ld:new=%ld]\n",
			   space, t_ori, ptp->tv_sec);

	if (t != NULL) {
		*t = ptp->tv_sec;
	}
	return (ptp->tv_sec);
}

static long kacctime_alarm(const struct pt_regs *regs)
{
	unsigned int sec = regs->di;
	unsigned int *psec = (unsigned int *)&(regs->di);
	struct timespec nsec;
	int multi;
	int divid;

	int space = kacctime_seek_timespace();

	if (space < 0)
		return (ori_alarm(regs));

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d\n", space, sec); 

	nsec.tv_sec = sec;
	nsec.tv_nsec = 0;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_frac(&nsec, divid, multi, &nsec);

	kacctime_log_flags(LOG_FUNC, "[%d] TIME: os=%d / ns=%ld\n",
			   space, sec, nsec.tv_sec);

	if (nsec.tv_sec < 1) {
		*psec = 1;
	} else {
		*psec = nsec.tv_sec;
	}
	return (ori_alarm(regs));
}

static long kacctime_select(const struct pt_regs *regs)
{
	int space = kacctime_seek_timespace();
	int nfds = regs->di;

	struct timeval *timeout = (struct timeval *) regs->r8;
	int multi;
	int divid;

	if (timeout == NULL) {
		return (ori_select(regs));
	}

	if (nfds > 0) {
		return (ori_select(regs));
	}

	if (space < 0) {
		return (ori_select(regs));
	}

	kacctime_log_flags(LOG_FUNC, "[%d] OLD SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timeval_frac(timeout, divid, multi, timeout);

	kacctime_log_flags(LOG_FUNC, "[%d] NEW SELECT(%d, [%ld,%ld])\n",
			   space, nfds,
			   timeout->tv_sec, timeout->tv_usec);

	return (ori_select(regs));
}

/* fin des fct de temps */

static void kacctime_override_syscall(void)
{
	unsigned long cr0;

	kacctime_syscall_table = (sys_call_ptr_t *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_flags(LOG_INIT, "sym addr : [%pK]\n", kacctime_syscall_table);

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	ori_nanosleep = kacctime_syscall_table[__NR_nanosleep];
	kacctime_syscall_table[__NR_nanosleep] = kacctime_nanosleep;
	kacctime_log_flags(LOG_INIT, "takeover: nanosleep\n");

	ori_clock_nanosleep = kacctime_syscall_table[__NR_clock_nanosleep];
	kacctime_syscall_table[__NR_clock_nanosleep] = kacctime_clock_nanosleep;
	kacctime_log_flags(LOG_INIT, "takeover: clock_nanosleep\n");

	ori_gettimeofday = kacctime_syscall_table[__NR_gettimeofday];
	kacctime_syscall_table[__NR_gettimeofday] = kacctime_gettimeofday;
	kacctime_log_flags(LOG_INIT, "takeover: gettimeofday\n");

	ori_clock_gettime = kacctime_syscall_table[__NR_clock_gettime];
	kacctime_syscall_table[__NR_clock_gettime] = kacctime_clock_gettime;
	kacctime_log_flags(LOG_INIT, "takeover: clock_gettime\n");

	ori_time = kacctime_syscall_table[__NR_time];
	kacctime_syscall_table[__NR_time] = kacctime_time;
	kacctime_log_flags(LOG_INIT, "takeover: time\n");

	ori_alarm = kacctime_syscall_table[__NR_alarm];
	kacctime_syscall_table[__NR_alarm] = kacctime_alarm;
	kacctime_log_flags(LOG_INIT, "takeover: alarm\n");

	ori_select = kacctime_syscall_table[__NR_select];
	kacctime_syscall_table[__NR_select] = kacctime_select;
	kacctime_log_flags(LOG_INIT, "takeover: select\n");

	ori_wait4 = kacctime_syscall_table[__NR_wait4];
	kacctime_syscall_table[__NR_wait4] = kacctime_wait4;
	kacctime_log_flags(LOG_INIT, "takeover: wait4\n");

	write_cr0(cr0);
	return;
}

static void kacctime_pullback_syscall(void)
{
	unsigned long cr0;

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	kacctime_syscall_table[__NR_nanosleep] = ori_nanosleep;
	kacctime_log_flags(LOG_INIT, "getback : nanosleep\n");

	kacctime_syscall_table[__NR_clock_nanosleep] = ori_clock_nanosleep;
	kacctime_log_flags(LOG_INIT, "getback : clock_nanosleep\n");

	kacctime_syscall_table[__NR_gettimeofday] = ori_gettimeofday;
	kacctime_log_flags(LOG_INIT, "getback : gettimeofday\n");

	kacctime_syscall_table[__NR_clock_gettime] = ori_clock_gettime;
	kacctime_log_flags(LOG_INIT, "getback : clock_gettime\n");

	kacctime_syscall_table[__NR_time] = ori_time;
	kacctime_log_flags(LOG_INIT, "getback : time\n");

	kacctime_syscall_table[__NR_alarm] = ori_alarm;
	kacctime_log_flags(LOG_INIT, "getback : alarm\n");

	kacctime_syscall_table[__NR_select] = ori_select;
	kacctime_log_flags(LOG_INIT, "getback : select\n");

	kacctime_syscall_table[__NR_wait4] = ori_wait4;
	kacctime_log_flags(LOG_INIT, "getback : wait4\n");

	write_cr0(cr0);

	while (atomic_read(&kacctime_sleep_count) > 0) {
		kacctime_log_flags(LOG_INIT, "sleep running: %d\n",
				   atomic_read(&kacctime_sleep_count));
		ssleep(1);
	}

	return;
}
/* fin override syscall */

/* debugfs */
static void kacctime_mk_debugfs(void)
{
	kacctime_log_flags(LOG_INIT, "creation debugfs\n");
	kacctime_debugfs = debugfs_create_dir(KACCTIME_DEBUGFS_ROOT, NULL);
	debugfs_create_atomic_t("sleep_running", S_IFREG | S_IRUGO,
				kacctime_debugfs,
				&kacctime_sleep_count);
}

static void kacctime_rm_debugfs(void)
{
	kacctime_log_flags(LOG_INIT, "suppression debugfs\n");
	debugfs_remove_recursive(kacctime_debugfs);
}
/* fin debugfs */

/* Main */
static int __init kacctime_init(void)
{
//	struct timespec64 ts;

	kacctime_log_flags(LOG_INIT, "init kacctime_log_mode: %d\n",
			   kacctime_log_mode);

	kacctime_mk_debugfs();
	kacctime_override_syscall();

	kacctime_timespace_tab[0].ref.tv_sec = ts0[0];
	kacctime_timespace_tab[0].ref.tv_nsec = 0;
	kacctime_timespace_tab[0].dec.tv_sec = ts0[1];
	kacctime_timespace_tab[0].dec.tv_nsec = 0;
	kacctime_timespace_tab[0].multi = ts0[2];
	kacctime_timespace_tab[0].divid = ts0[3];
	
	return (0);
}

static void __exit kacctime_exit(void)
{
	kacctime_pullback_syscall();
	kacctime_rm_debugfs();

	kacctime_log_flags(LOG_INIT, "exit kacctime_log_mode: %d\n",
			   kacctime_log_mode);
}

module_init(kacctime_init);
module_exit(kacctime_exit);
