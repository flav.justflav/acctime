
/*===========================================================================*
 * Les fonctionnalites communes d'acctime.
 *==========================================================================*/

#ifndef KACCTIME_CORE_H
#define KACCTIME_CORE_H

#warning " *** [DEBUG] INCLUDE KACCTIME_CORE_H"

/******************************************************************************
 * Les includes
 *****************************************************************************/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <linux/slab.h>
#include <linux/cgroup.h>

#include "config.h"
#include "kacctime.h"
#include "time_function.h"

/******************************************************************************
 * Les structures
 *****************************************************************************/

static long kacctime_log_mode = 0;
static unsigned long *kacctime_syscall_table = NULL;
static unsigned long *kacctime_syscall_table_32 = NULL;

static int kacctime_sleep_frac_cycle = SLEEP_FRAC_CYCLE_DEFAULT;
static int kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;

static struct kobj_attribute kts_attribute[KACCTIME_MAX_SPACE];
static struct attribute *kts_attrs[KACCTIME_MAX_SPACE+1];
static struct attribute_group kts_attr_group;
static struct kacctime_obj kts;
static struct kobject kacctime_module_kobj;

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
	struct timespec	mono_delta;
};

/* Prevert crash exiting module */
static atomic_t  kacctime_counter_sleep = ATOMIC_INIT(0);

struct kacctime_obj {
	struct kobject *kobj;
	struct kacctime_timespace vec[KACCTIME_MAX_SPACE];
};


/******************************************************************************
 * Les macros
 *****************************************************************************/


#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

#define replace_syscall(FUNCTION) \
	original_##FUNCTION = (void *)kacctime_syscall_table[__NR_##FUNCTION]; \
	kacctime_syscall_table[__NR_##FUNCTION] = (unsigned long) kacctime_##FUNCTION; \
	kacctime_log_level(0, "replace : ##FUNCTION\n"); 

#define getback_syscall(FUNCTION) \
	kacctime_syscall_table[__NR_##FUNCTION] = (unsigned long)original_##FUNCTION; \
	kacctime_log_level(0, "getback : ##FUNCTION\n"); 

#define kacctime_timespec_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) + \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) - \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_mul(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) * f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_div(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
	  								\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) / f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec)	\
			* n / d;					\
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) + \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) - \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_mul(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) * f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_div(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) / f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec)	\
			* n / d;					\
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

/******************************************************************************
 * Les prototypes
 *****************************************************************************/

/*
 * Les prototypes: Fonctions outils
 */

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
__inline__ void kacctime_log_func(const char *func, int line, char *comm, int pid, int level, char *fmt, ...);

/**
 * kacctime_seek_timespace - cherche l'espace du process en cours
 *
 * Parcours les listes de programmes (/proc/kAccTime/) à accélerer
 * ou
 * Parcours de l'environnement du processus, si la variable FLAV_TIME
 * existe, elle doit contenir le numéro de l'espace (s'il est valide)
 * 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 * 
 */
int kacctime_seek_timespace(void);



/*
 * Les prototypes: Fonctions de gestion des appels systemes
 */

int kacctime_init_takeover_syscall(void);
void kacctime_replace_syscall(void);
void kacctime_getback_syscall(void);

/*
 * Les prototypes: Fonctions de gestion de sysfs/procfs
 */

static __inline__ void kacctime_init_timespaces(void);
int kacctime_sysfs_init(void);

ssize_t kts_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
ssize_t kts_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count);

static __inline__ void kacctime_update_ref_dec(int idx, int new_multi, int new_divid);



#endif
