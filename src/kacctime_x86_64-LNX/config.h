
/**
 * kacctime kernel module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <fastraud@itsgroup.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018-     Luc Plisson <lplisson@itsgroup.fr>
 *
 * v0.412 -> adding 'sleep' slicing
 * v0.412 -> adding "starting code disabled" 
 * v0.412 -> logs by levels 
 * v0.4124 -> stats in /proc & logs
 * v0.41245 -> select, alarm, poll, epoll_wait
 * v0.412454 -> adding clock_monolitic & futex
 * v0.412454 -> adding semtimedop
 * v0.4124540 -> acc by process name
 * v0.5 -> sleep bias corretion
 * 2017-06-06 : v0.6 -> 32bit functions impl./corrections
 * 2017-09-25 : v0.65 -> seek_spacetime bug correction
 * 2017-12-11 : v0.7 -> slowing code implementation beginning
 * 2018-04-19 : v.8 -> lnx 4 version beginning 
 */

#ifndef CONFIG_H
#define CONFIG_H

#warning " *** [DEBUG] INCLUDE CONFIG_H"

#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/

#define KACCTIME_MAX_SPACE      4
#define KACCTIME_MAX_LEN        256
#define KACCTIME_BUF_SIZE       ((KACCTIME_MAX_SPACE+1) * KACCTIME_MAX_LEN)
#define KACCTIME_CODE_FREQ      300

#define KACCTIME_BITS           6

//#define KACCTIME_KERN_PATH      "/boot/System.map-"
#define KACCTIME_KERN_PATH      "/proc/kallsyms"
#define KACCTIME_SYSCALL_SYM    "sys_call_table"
#define KACCTIME_SYSCALL_SYM32  "ia32_sys_call_table"
#define KACCTIME_SYSFS_BASENAME "kts"
#define KACCTIME_SYSFS_BASEDIR	"kts_vec"
#define CHAR_NEW_LINE           '\n'
#define CHAR_SPACE              ' '
#define CHAR_END_STR            '\0'
#define CHAR_EQUAL              '='


#define SLEEP_FRAC_CYCLE_DEFAULT	10
#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

#define TMP_BUF_MAX_SIZE        1024

/*
** macro de calcul du temps
#define NSEC_PER_SEC 1000000000LL
#define USEC_PER_SEC 1000000LL
*/

#endif
