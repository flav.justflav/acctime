
/*===========================================================================*
 * Definition des surcharges des appels systemes
 *==========================================================================*/

#ifndef TIME_FUNCTION_H
#define TIME_FUNCTION_H

#warning " *** [DEBUG] INCLUDE TIME_FUNCTION_H"

/******************************************************************************
 * Les includes
 *****************************************************************************/

#include "config.h"
#include "kacctime.h"
#include "kacctime_core.h"

/******************************************************************************
 * Les structures
 *****************************************************************************/

// N.A

/******************************************************************************
 * Les macros
 *****************************************************************************/

// N.A

/******************************************************************************
 * Les prototypes
 *****************************************************************************/

/*
 * Les Prototypes: Appels systeme originaux
 */

static time_t (*original_time)(time_t *t);
static int (*original_time_32)(int *t);

static long (*original_gettimeofday)(struct timeval *tv, struct timezone *tz);
static int (*original_gettimeofday_32)(struct timeval_32 *tv, struct timezone *tz); 
 
static long (*original_nanosleep)(const struct timespec *req, struct timespec *rem);
static int (*original_nanosleep_32)(const struct timespec_32 *req, struct timespec_32 *rem);
 
static long (*original_clock_gettime)(clockid_t clk_id, struct timespec *tp);
static int (*original_clock_gettime_32)(clockid_t clk_id, struct timespec_32 *tp);

static long (*original_setitimer)(int which, const struct itimerval *new_value, struct itimerval *old_value);
static int (*original_setitimer_32)(int which, const struct itimerval_32 *new_value, struct itimerval_32 *old_value);

static long (*original_getitimer)(int which, struct itimerval *curr_value);
static int (*original_getitimer_32)(int which, struct itimerval_32 *curr_value);

static long (*original_alarm)(unsigned int sec);
static int (*original_alarm_32)(unsigned int sec);

static long (*original_select)(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout);
static int (*original_select_32)(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval_32 *timeout);

// static long (*original_poll)(struct pollfd *fds, nfds_t nfds, int timeout);
// static int (*original_poll_32)(struct pollfd *fds, nfds_t nfds, int timeout);

static long (*original_epoll_wait)(int epfd, struct epoll_event *events, int maxevents, int timeout);
static int (*original_epoll_wait_32)(int epfd, struct epoll_event *events, int maxevents, int timeout);

static long (*original_futex)(int *uaddr, int futex_op, int val, const struct timespec *timeout, int *uaddr2, int val3);
static long (*original_futex_32)(int *uaddr, int futex_op, int val, const struct timespec_32 *timeout, int *uaddr2, int val3);

static long (*original_semtimedop)(int semid, struct sembuf *sops, size_t nsops, const struct timespec *timeout);


/*
 * Les prototypes: Appels systeme kAcctime
 */

long kacctime_clock_gettime(clockid_t clk_id, struct timespec *tp);

/**
 * nanosleep -- suppend le proc appelant (voir le man)
 * Si le process est accéléré alors :
 * le temps est divisé par le facteur
 * puis la durée est divisé en intervalle pour pouvoir modifier le
 * rapport durant l'attente
 */
long kacctime_nanosleep(const struct timespec *rqtp, struct timespec *rmtp);


#endif
