
#include "time_function.h"

/*
 * Les prototypes: Appels systeme kAcctime
 */

long kacctime_clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int err;
	int space;
	long retval;	
	struct timespec ktp;
	struct timespec ktp_sav;
	int multi;
	int divid;
	struct timespec dec;
	struct timespec ref;

	retval = (*original_clock_gettime)(clk_id, tp);
	if (retval != 0)
		goto out;
	
	space = kacctime_seek_timespace();
	/* kacctime_stats_space_tab[space+1].cmpt_clock_gettime++; */
	/* kacctime_add_stats(space, */
	/* 		   KACCTIME_NB_CLOCK_GETTIME, */
	/* 		   kacctime_stats_space_tab[space+1].cmpt_clock_gettime); */
	if (space < 0)
		goto out;

	if (clk_id == CLOCK_MONOTONIC) {
		retval = (*original_clock_gettime)(CLOCK_REALTIME, tp);      
		if (retval != 0)
			goto out;
	}
  
	err = copy_from_user(&ktp, tp, sizeof(*tp));

	ktp_sav.tv_sec = ktp.tv_sec;
	ktp_sav.tv_nsec = ktp.tv_nsec;
	if (err)
		goto err_mem;
	
	if (clk_id > CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n",
				   clk_id, ktp.tv_sec, ktp.tv_nsec);
		goto out;
	}
  
	multi = kts.vec[space].multi;
	divid = kts.vec[space].divid;
	dec = kts.vec[space].dec;
	ref = kts.vec[space].ref;
  
	kacctime_timespec_sub(ktp, ref, ktp);
	kacctime_timespec_frac(ktp, multi, divid, ktp);
	kacctime_timespec_add(ktp, ref, ktp);
	kacctime_timespec_add(ktp, dec, ktp);
	
	kacctime_log_level(3,
			   "ID=%d, [os=%ld ; ons=%ld] [ns=%ld ; nns=%ld]\n",
			   clk_id, ktp_sav.tv_sec, ktp_sav.tv_nsec,
			   ktp.tv_sec, ktp.tv_nsec);
  
	if (clk_id == CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
		kacctime_timespec_sub(ktp,
				      kts.vec[space].mono_delta,
				      ktp);
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
	}

	err = copy_to_user(tp, &ktp, sizeof(*tp));
	if (err)
		goto err_mem;
	
 out: 
	return (retval);
	
 err_mem:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);  
}

/**
 * nanosleep -- suppend le proc appelant (voir le man)
 * Si le process est accéléré alors :
 * le temps est divisé par le facteur
 * puis la durée est divisé en intervalle pour pouvoir modifier le
 * rapport durant l'attente
 */
long kacctime_nanosleep(const struct timespec *rqtp,
			       struct timespec *rmtp)
{
	mm_segment_t oldfs;
	struct timespec tu;
	struct timespec kreq;	
	int space = -1;
	long retval;
	struct timespec t_zero;
	struct timespec t_un;
	int multi;
	int divid;
	int cycle = 1;
	long int duration = 0;

	atomic_inc(&kacctime_counter_sleep);
	space = kacctime_seek_timespace();

	kacctime_log_level(0, "(%d)-%d\n", space,
			   atomic_read(&kacctime_counter_sleep));

	kacctime_log_level(0, "NANOSLEEP (%ld,%ld)\n", rqtp->tv_sec,
			   rqtp->tv_nsec);

	if (space < 0) {
		retval = (*original_nanosleep)(rqtp, rmtp);
		goto out;
	}

	if (copy_from_user(&tu, rqtp, sizeof(tu))) {
		retval = -EFAULT;
		goto out;
	}
	
	if (!timespec_valid(&tu)) {
		retval = -EINVAL;
		goto out;
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_zero);
	if (retval != 0) {
		kacctime_log_level(0, "pb clock_gettime !\n");
		goto out_fs;
	}		

	while (true) {
		multi = kts.vec[space].multi;
		divid = kts.vec[space].divid;

		if (tu.tv_sec <= kacctime_sleep_frac_interval) {
			kacctime_timespec_frac(tu, divid, multi, kreq);
			kacctime_log_level(0,
					   "[%s - (%d) %d] f:%d/%d / ori [%ld,%ld] -> \
new [%ld,%ld]\n",
					   current->comm, current->cred->uid.val,
					   current->pid, multi, divid,
					   tu.tv_sec, tu.tv_nsec,
					   kreq.tv_sec, kreq.tv_nsec);
			retval = (*original_nanosleep)(&kreq, rmtp);
			if (retval < 0)
				goto out_fs;
			break;
		}
		if ((cycle % kacctime_sleep_frac_cycle) == 0) {
			struct timespec d_reel;
			time_t biais;
			
			retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_un);
			if (retval != 0) {
				kacctime_log_level(0, "pb clock_gettime !\n");
				goto out_fs;
			}
			kacctime_timespec_sub(t_un, t_zero, d_reel);
			biais = d_reel.tv_sec - duration;
			duration += biais;
			if ((biais < 0) ||
			    (biais > kacctime_sleep_frac_interval)) {
				kacctime_log_level(1,
						   "biais:(%d)-dur:(%d) / t0 [%ld,%ld] -> t1 [%ld,%ld]\n",
						   biais, duration,
						   t_zero.tv_sec, t_zero.tv_nsec,
						   t_un.tv_sec, t_un.tv_nsec);
			}
			if (tu.tv_sec >= biais)
				tu.tv_sec -= biais;
			else
				tu.tv_sec = 0;
			if (tu.tv_sec <= kacctime_sleep_frac_interval)
				continue;
		}
		kreq.tv_sec = kacctime_sleep_frac_interval;
		kreq.tv_nsec = 0;
		kacctime_timespec_div(kreq, multi, kreq);
		kacctime_timespec_mul(kreq, divid, kreq);
		
		kacctime_log_level(2,"[%s - (%d) %d] f:%d/%d / ori [%ld,%ld]\
 -> new [%ld,%ld]\n",
				   current->comm, current->cred->uid.val,
				   current->pid, multi, divid, tu.tv_sec,
				   0L,
				   kreq.tv_sec, kreq.tv_nsec);
		retval = (*original_nanosleep)(&kreq, rmtp);
		if (retval < 0)
			goto out_fs;
		tu.tv_sec -= kacctime_sleep_frac_interval;
		duration += kacctime_sleep_frac_interval;
		cycle++;
	}
	
	//	ret = (*original_nanosleep)(&tu, rmtp);
	
 out_fs:
	set_fs(oldfs);
	
 out:
	atomic_dec(&kacctime_counter_sleep);
	return (retval);
}

