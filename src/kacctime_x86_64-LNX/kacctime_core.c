
#include "kacctime_core.h"

/*
 * Fonctions outils
 */

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
__inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	printk(tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	printk(KERN_CONT "%s", tmp_buf);
	return;
}

/**
 * kacctime_seek_timespace - cherche l'espace du process en cours
 *
 * Parcours les listes de programmes (/proc/kAccTime/) à accélerer
 * ou
 * Parcours de l'environnement du processus, si la variable FLAV_TIME
 * existe, elle doit contenir le numéro de l'espace (s'il est valide)
 * 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 * 
 */
int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int n;

	/*
	  retval = kacctime_search_prog_acc_by_name();
	  if (retval != -1)
	  return (retval);
	*/
	
	n = task_cgroup_path(current, tmp_buf, TMP_BUF_MAX_SIZE-1);
	//	printk(KERN_CONT "cgroup:[%s]\n", tmp_buf);

	
	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_level(0, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_level(0, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
			
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_level(0, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}
				if ((space < 0) ||
				    (space >= KACCTIME_MAX_SPACE)) {
					kacctime_log_level(0, "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_level(5, "space -> [%d]\n", space);
				kfree(buf);
				if (kts.vec[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	if (kts.vec[0].multi == 0)    
		return (-1);
	return (0);
}

/*
 * Fonctions de gestion des appels systemes
 */

int kacctime_init_takeover_syscall(void)
{
	int retval;
	
	kacctime_log_level(0, "init take over\n");

	kacctime_syscall_table = (unsigned long *) kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_level(0, "sym addr : [%pK]\n", kacctime_syscall_table);

	kacctime_syscall_table_32 = (unsigned long *) kallsyms_lookup_name(KACCTIME_SYSCALL_SYM32);
	kacctime_log_level(0, "sym addr32 : [%pK]\n", kacctime_syscall_table_32);

	kacctime_replace_syscall();
	//TODO retval
	if (retval == -1) {
		kacctime_log_level(0, "syscall takeover failed !\n");
		return (retval);
	}
	return (0);
}


void kacctime_replace_syscall(void)
{
	unsigned long	cr0;

	kacctime_log_level(0, "kacctime_replace_syscall\n");

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	replace_syscall(nanosleep);
	replace_syscall(clock_gettime);

 	write_cr0(cr0);
	return;
}

void kacctime_getback_syscall(void)
{
	unsigned long cr0;
	
	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	getback_syscall(nanosleep);
	getback_syscall(clock_gettime);

	write_cr0(cr0);
	return;
}


/*
 * Fonctions de gestion de sysfs/procfs
 */

static __inline__ void kacctime_init_timespaces(void)
{
	int idx = 0;
	char *tmp_buf;
	int nb_car;

  	while (idx < KACCTIME_MAX_SPACE) {
		kts.vec[idx].multi = 0;
		kts.vec[idx].divid = 1;
		kts.vec[idx].ref.tv_sec = 0;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = 0;
		kts.vec[idx].dec.tv_nsec = 0;
		kts.vec[idx].mono_delta.tv_sec = 0;
		kts.vec[idx].mono_delta.tv_nsec = 0;
		tmp_buf = kmalloc(5, GFP_KERNEL);
		nb_car = snprintf(tmp_buf, 5, "%s%d", KACCTIME_SYSFS_BASENAME, idx);
		kts_attribute[idx].attr.name = tmp_buf;
		kts_attribute[idx].attr.mode = VERIFY_OCTAL_PERMISSIONS(0664);
		kts_attribute[idx].show = kts_show;
		kts_attribute[idx].store = kts_store;
		kts_attrs[idx] = &(kts_attribute[idx].attr);
		idx++;
  	}
	kts_attrs[idx] = NULL;
	kts_attr_group.attrs = kts_attrs;

	return;
}


int kacctime_sysfs_init(void)
{
	int retval;
	
	kacctime_init_timespaces();

	kacctime_module_kobj = (((struct module *)(THIS_MODULE))->mkobj).kobj;	
	kts.kobj = kobject_create_and_add(KACCTIME_SYSFS_BASEDIR, &kacctime_module_kobj);
	if (!kts.kobj)
		return -ENOMEM;
	retval = sysfs_create_group(kts.kobj, &kts_attr_group);
	if (retval)
		kobject_put(kts.kobj);

	return retval;
}

ssize_t kts_show(struct kobject *kobj, struct kobj_attribute *attr,
                        char *buf)
{
	int idx = 0;
	int ret = 0;
	
        while (idx < KACCTIME_MAX_SPACE) {
		char tmp_buf[sizeof(KACCTIME_SYSFS_BASENAME)+2];

		snprintf(tmp_buf, sizeof(KACCTIME_SYSFS_BASENAME)+2,
			 "%s%d", KACCTIME_SYSFS_BASENAME, idx);
		if (strcmp(attr->attr.name, tmp_buf) == 0) {
			break;
		}
	       idx++;
	}
	ret = sprintf(buf,"space=%d\nmulti=%d\ndivid=%d\nref=%ld\ndec=%ld\n\
mono_delta=[%ld,%ld]\n",
		      idx,
		      kts.vec[idx].multi,
		      kts.vec[idx].divid,
		      kts.vec[idx].ref.tv_sec,
		      kts.vec[idx].dec.tv_sec,
		      kts.vec[idx].mono_delta.tv_sec,
		      kts.vec[idx].mono_delta.tv_nsec);
        return (ret);
}

ssize_t kts_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
        int idx = 0;
        int ret;
	int multi, divid;
	time_t ref, dec;

        while (idx < KACCTIME_MAX_SPACE) {
		char tmp_kts[sizeof(KACCTIME_SYSFS_BASENAME)+2];

		snprintf(tmp_kts, sizeof(KACCTIME_SYSFS_BASENAME)+2, "%s%d",
			 KACCTIME_SYSFS_BASENAME, idx);
		if (strcmp(attr->attr.name, tmp_kts) == 0)
			break;
		idx++;
        }
	if (idx == KACCTIME_MAX_SPACE)
		return -EIO;
	ret = sscanf(buf, "%d:%d:%ld:%ld", &multi, &divid, &ref, &dec);
	kacctime_log_level(0, "%d->%d:%d:%d:%d\n", ret, multi, divid, ref, dec);
	switch (ret) {
	case 1 :
		kacctime_update_ref_dec(idx, multi, 1);
		break;
	case 2:
		kacctime_update_ref_dec(idx, multi, divid);
		break;
	case 3:
		kts.vec[idx].multi = multi;
		kts.vec[idx].divid = divid;
		kts.vec[idx].ref.tv_sec = ref;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = 0;
		kts.vec[idx].dec.tv_nsec = 0;
		break;
	case 4:
		kts.vec[idx].multi = multi;
		kts.vec[idx].divid = divid;
		kts.vec[idx].ref.tv_sec = ref;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = dec;
		kts.vec[idx].dec.tv_nsec = 0;
		break;
	default:
		kacctime_log_level(0, "error format !");
		return -EIO;
	}
	get_monotonic_boottime(&(kts.vec[idx].mono_delta));
	kacctime_log_level(0, "%d->%d:%d:%d:%d:%d:%d\n", idx,
			   kts.vec[idx].multi,
			   kts.vec[idx].divid,
			   kts.vec[idx].ref.tv_sec,
			   kts.vec[idx].dec.tv_sec,
			   kts.vec[idx].mono_delta.tv_sec,
			   kts.vec[idx].mono_delta.tv_nsec);
	return count;
}



static __inline__ void kacctime_update_ref_dec(int idx, int new_multi,
					       int new_divid)
{
	int old_multi;
	int old_divid;
	struct timespec old_dec;
	struct timespec old_ref;
	struct timespec tmp;

	old_multi = kts.vec[idx].multi;
	old_divid = kts.vec[idx].divid;
	old_ref.tv_sec = kts.vec[idx].ref.tv_sec;
	old_ref.tv_nsec = kts.vec[idx].ref.tv_nsec;
	old_dec.tv_sec = kts.vec[idx].dec.tv_sec;
	old_dec.tv_nsec = kts.vec[idx].dec.tv_nsec;

	/* QUID de MONO DELTA */

	kts.vec[idx].multi = new_multi;
	kts.vec[idx].divid = new_divid;

	getnstimeofday(&(kts.vec[idx].ref));

	tmp.tv_sec = kts.vec[idx].ref.tv_sec;
	tmp.tv_nsec = kts.vec[idx].ref.tv_nsec;
	
	kacctime_timespec_sub(tmp, old_ref, tmp);
	kacctime_timespec_frac(tmp, old_multi, old_divid, tmp);
	kacctime_timespec_add(tmp, old_ref, tmp);
	kacctime_timespec_add(tmp, old_dec, tmp);

	printk("tmp[%ld / %ld]\n", tmp.tv_sec,
	       tmp.tv_nsec);
	

	kacctime_timespec_sub(tmp, kts.vec[idx].ref, kts.vec[idx].dec);
	printk("dec[%ld / %ld]\n", kts.vec[idx].dec.tv_sec,
	       kts.vec[idx].dec.tv_nsec);
	return;
}

