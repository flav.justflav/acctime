
/*===========================================================================*
 * Fichier "Main" du module kAcctime.
 * Garder ici uniquement le neecessaire 
 * pour la gestion du module kernel.
 * Les fonctionnalites acctime sont a ajouter 
 * dans kacctime_core.
 *==========================================================================*/

#ifndef KACCTIME_H
#define KACCTIME_H

#warning " *** [DEBUG] INCLUDE KACCTIME_H"

/******************************************************************************
 * Les includes
 *****************************************************************************/

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/utsname.h>
#include <linux/hrtimer.h>
#include <linux/cgroup.h>

#include "config.h"
#include "kacctime_core.h"

/******************************************************************************
 * Les structures
 *****************************************************************************/

// N.A

/******************************************************************************
 * Les macros
 *****************************************************************************/

// N.A

/******************************************************************************
 * Les prototypes
 *****************************************************************************/

/*
 * Les prototypes: Fonctions d'init. d'un module kernel
 */

static int __init kacctime_init(void);
static void __exit kacctime_exit(void);

#endif
