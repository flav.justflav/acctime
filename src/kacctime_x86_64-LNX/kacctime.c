
#include "kacctime.h"

/*
 * Fonctions d'init. d'un module kernel
 */

static int __init kacctime_init(void)
{
	int ret = 0;
	
	kacctime_log_level(0, "init : %ld\n", kacctime_log_mode);

	ret = kacctime_sysfs_init();
	ret = kacctime_init_takeover_syscall();
	
	return (ret);
}

static void __exit kacctime_exit(void)
{
	kacctime_log_level(0, "exit\n");
	kacctime_getback_syscall();
	return;
}

/*
 * " MAIN " du module
 */

module_init(kacctime_init);
module_exit(kacctime_exit);

MODULE_VERSION("490-2018-05-03_13:49:30-0.8");

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <fastraud@itsgroup.fr>");
MODULE_DESCRIPTION("AccTime LKM");

module_param(kacctime_log_mode, long, 0);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

