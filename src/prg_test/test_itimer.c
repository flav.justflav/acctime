#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

void sigalrm_handler(int sig)
{
	char outstr[200];
	struct tm *tm_gtod;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	tm_gtod = malloc(sizeof(*tm_gtod));
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf("TIM [%s]\n", outstr);
	exit(1);
}

int  main(int ac, char **av)
{
	char outstr[200];
	struct tm *tm_gtod;
	struct timeval tv;
	struct itimerval it_val;
	struct itimerval it_val2;

	signal(SIGALRM, sigalrm_handler);   

	//	alarm(atoi(av[1]));

	it_val.it_interval.tv_sec = atoi(av[1]);
	it_val.it_interval.tv_usec = (atoi(av[2]) * 1000) % 10000;
	it_val.it_value.tv_sec = it_val.it_interval.tv_sec;
	it_val.it_value.tv_usec = it_val.it_interval.tv_usec;

	setitimer(ITIMER_REAL, &it_val, NULL);

	tm_gtod = malloc(sizeof(*tm_gtod));
	gettimeofday(&tv, NULL);
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf(" IN [%s]\n", outstr);

	sleep(atoi(av[3]) / 2);
	getitimer(ITIMER_REAL, &it_val2);
	printf("[%d, %d][%d %d]\n",
	       it_val2.it_value.tv_sec,
	       it_val2.it_value.tv_usec,
	       it_val2.it_interval.tv_sec,
	       it_val2.it_interval.tv_usec);
	sleep(atoi(av[3]) / 2);
	
	gettimeofday(&tv, NULL);
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf("OUT [%s]\n", outstr);
	exit(0);
}

