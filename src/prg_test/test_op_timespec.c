#include		<stdio.h>
#include		<stdlib.h>
#include		<time.h>
#include		<sys/time.h>

int			main(void)
{
  struct timespec	tp1;
  struct timespec	tp2;
  int			factor;
  long			ns_under;
  int			cpt;

  cpt = 0;

  while (1)
    {
      tp1.tv_sec = random();
      tp1.tv_nsec = random()% 1000000000L;
      factor = random() % 59 + 1;

      tp1.tv_sec = 43;
      tp1.tv_nsec = 40;
      factor = 4;
      
      tp2.tv_sec = tp1.tv_sec / factor;
      ns_under = ((tp1.tv_sec % factor) * 1000000000L) / factor;
      tp2.tv_nsec = (tp1.tv_nsec / factor) + ns_under;
      if (tp2.tv_nsec >= 1000000000L)
	{
	  printf("ERROR\n");
	}
      printf("(%2d) [%10ld,%10ld]->[%10ld,%10ld]\n",
	     factor,
	     tp1.tv_sec, tp1.tv_nsec,
	     tp2.tv_sec, tp2.tv_nsec);
	     sleep(1);

	     //      printf("cpt : %d\r", cpt++);
    }
  return;
}
