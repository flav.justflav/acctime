#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>

int main(int ac, char **av) {
	char filename_crea[1024];
	char filename_open[1024];
	char filename_openat[1024];
	int r_crea = 0;
	int r_open = 0;
	int r_openat = 0;

	if (ac < 2) {
		fprintf(stderr, "Usage: %s FILE\n", av[0]);
		exit(EXIT_FAILURE);
	}
	snprintf(filename_crea, 1023, "%s_crea", av[1]);
	snprintf(filename_open, 1023, "%s_open", av[1]);
	snprintf(filename_openat, 1023, "%s_openat", av[1]);
	fprintf(stdout, "filename: [%s]\ncrea: [%s]\nopen: [%s]\nopenat: [%s]\n",
		av[1],
		filename_crea,
		filename_open,
		filename_openat);
	r_crea = creat(filename_crea, 0644);
	r_open = open(filename_open, O_CREAT|O_WRONLY|O_TRUNC, 0644);
	r_open = openat(AT_FDCWD, filename_openat, O_CREAT|O_WRONLY|O_TRUNC,
			0644);
	exit(EXIT_SUCCESS);
}
