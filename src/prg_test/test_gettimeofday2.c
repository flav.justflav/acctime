#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int ac, char **av)
{
	struct timeval tv;
	struct timeval tv_kacc;
	int r1, r2;
	struct tm *tm_gtod;
	struct tm *tm_time;
	char outstr[60];
	char outstr2[60];
	char la[200];
	char ln[200];
	
	r1 = gettimeofday(&tv, NULL);
	int r = putenv("FLAV_TIME=2");
	printf("r=%d [%s]\n", r, getenv("FLAV_TIME"));
	r2 = gettimeofday(&tv_kacc, NULL);
	
	unsetenv("FLAV_TIME");
	tm_gtod = malloc(sizeof(*tm_gtod));
	tm_time = malloc(sizeof(*tm_time));
	sprintf(la, "r=%d ; n:[%ld / %ld]\t", r1, tv.tv_sec, tv.tv_usec);
	sprintf(ln, "r=%d ; a:[%ld / %ld]\t", r2, tv_kacc.tv_sec,
		tv_kacc.tv_usec);

	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	tm_time = localtime(&(tv_kacc.tv_sec));
	strftime(outstr2, sizeof(outstr2), "%F %T", tm_time);

	printf("[%s][%s]\n[%s][%s]\n", la, outstr, ln, outstr2);
	return 0;
}
