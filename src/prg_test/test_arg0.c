#include	<stdio.h>
#include	<stdlib.h>
#include	<time.h>
#include	<sys/prctl.h>
#include	<strings.h>

int		main(int ac, char **av, char **ev)
{
  time_t	t;
  int		s;
  size_t	l = 0;
  char		cmd[1024];
  char		**sev;
  
  t = time(NULL);
  printf("time :: [%d]\n", t);

  s = prctl(PR_SET_NAME,"TEST CHG ARG0\0",NULL,NULL,NULL);
  t = time(NULL);
  printf("time :: [%d]\n", t);

  printf("\n");
  sev = ev;
  while (*sev != 0)
    {
      printf("[%s]\n", *sev);
      l += strlen(*sev);
      sev++;
    }
  
  printf("### (%d) %d ###\n", getpid(), l);
  memset(ev, '\0', l);
  
  printf("\n");
  s = 25;
  while (s > 0)
    {
      sprintf(cmd, "### %d ###\r", s);
      write(1, cmd, strlen(cmd));
      sleep(1);
      s--;
    }
  return;
}
