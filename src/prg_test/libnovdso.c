#include <unistd.h>
#include <sys/syscall.h>

int clock_gettime(void * clk_id, void * tp) {
	return syscall(SYS_clock_gettime, clk_id, tp);
}
