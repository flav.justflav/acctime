#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/syscall.h>
#include <linux/stat.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#define _GNU_SOURCE

static void print_time(const char *field, struct statx_timestamp *ts)
{
	struct tm tm;
	time_t tim;
	char buffer[100];
	int len;

	tim = ts->tv_sec;
	if (!localtime_r(&tim, &tm)) {
		perror("localtime_r");
		exit(1);
	}
	len = strftime(buffer, 100, "%F %T", &tm);
	if (len == 0) {
		perror("strftime");
		exit(1);
	}
	printf("%s", field);
	fwrite(buffer, 1, len, stdout);
	printf(".%09u", ts->tv_nsec);
	len = strftime(buffer, 100, "%z", &tm);
	if (len == 0) {
		perror("strftime2");
		exit(1);
	}
	fwrite(buffer, 1, len, stdout);
	printf("\n");
}

int main(int ac, char **av)
{
	struct statx stx;
	int r;
	char *filename;
	char err_msg[128];

	if (ac < 2) {
		snprintf(err_msg, 127, "Usage: %s filename\n", av[0]);
		goto out_err;
	}
	filename = av[1];
	printf("file: %s\n", filename);
	r = faccessat(AT_FDCWD, av[1], R_OK, 0);
	if (r == 0) {
		printf("read access OK\n");
	} else {
		snprintf(err_msg, 127, "read access denied : %s\n",
			 strerror(errno));
		goto out_err;
	}

	r = syscall(__NR_statx, AT_FDCWD, filename, 0, STATX_ALL, &stx);

	if (r == 0) {
		printf("statx ok\n");
	if (stx.stx_mask & STATX_ATIME)
		print_time("Access: ", &stx.stx_atime);
	if (stx.stx_mask & STATX_MTIME)
		print_time("Modify: ", &stx.stx_mtime);
	if (stx.stx_mask & STATX_CTIME)
		print_time("Change: ", &stx.stx_ctime);
	if (stx.stx_mask & STATX_BTIME)
		print_time(" Birth: ", &stx.stx_btime);
	} else {
		snprintf(err_msg, 127, "statx err: %s\n", strerror(errno));
		goto out_err;
	}
	
	exit(EXIT_SUCCESS);

out_err:
	fprintf(stderr, "%s", err_msg);
	exit(EXIT_FAILURE);
}
