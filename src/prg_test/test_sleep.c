#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(int ac, char **av)
{
	struct timespec req, rem;
		
	req.tv_sec = atoi(av[1]);
	req.tv_nsec = atoi(av[2]);
	rem.tv_sec = 0;
	rem.tv_nsec = 0;
	nanosleep(&req, &rem);
	printf("[%d %d][%d %d]\n",
	       req.tv_sec,
	       req.tv_nsec,
	       rem.tv_sec,
	       rem.tv_nsec);


	return 0;
}
