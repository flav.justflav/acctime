#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h> 
#include <errno.h>

extern int errno;
void handle_signal(int signal);


int main(int ac, char **av)
{
    struct sigaction sa;
    int ret, errnum;
    struct timespec tim, tim2;

    tim.tv_sec = atol(av[2]);
    tim.tv_nsec = atol(av[3]);
    sa.sa_handler = &handle_signal;
    sa.sa_flags = SA_RESTART;   
    sigfillset(&sa.sa_mask);
    if (sigaction(SIGINT, &sa, NULL) == -1) 
    {
        perror("Error: cannot handle SIGINT");
    }

    switch (atol(av[1]))
    {
        case 1:
            ret = nanosleep(&tim , &tim2);
            break;
        case 2:
            ret = nanosleep(&tim , &tim2);
            errnum = errno;
            printf("Return : %d\n", ret);
            perror("Erreur ");
            break;
        case 3:
            ret = nanosleep(&tim , &tim2);
            errnum = errno;
            printf("Return : %d\n", ret);
            perror("Erreur ");
            printf("Temps restant : %d s , %d ms \n", tim2.tv_sec,tim2.tv_nsec);
            break;
        default:
            break;

    }
    
}


void handle_signal(int signal) 
{
    const char *signal_name;
    sigset_t pending;

    switch (signal) 
    {
        case SIGINT:
            break;
        default:
            fprintf(stderr, "Caught wrong signal: %d\n", signal);
            return;
    }
}
