#include		<stdio.h>
#include		<time.h>
#include		<sys/time.h>

int			main(void)
{
  struct timespec	tp1;
  struct timespec	tp2;
  
  tp1.tv_sec = 0;
  tp1.tv_nsec = 0;
  tp2.tv_sec = 0;
  tp2.tv_nsec = 0;
  while (1)
    {
      clock_gettime(CLOCK_MONOTONIC, &tp1);
      printf("clock_gettime:MONOTONIC: [%ld, %ld]\n", tp1.tv_sec, tp1.tv_nsec);
      printf("diff mono : %d, %d\n", (tp1.tv_sec-tp2.tv_sec),
	     (tp1.tv_nsec-tp2.tv_nsec));
      tp2.tv_sec = tp1.tv_sec;
      tp2.tv_nsec = tp1.tv_nsec;
      sleep(5);
    }
  return;
}
