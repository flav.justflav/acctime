#include <stdio.h>
#include <time.h>

int		main(int ac, char **av)
{
  time_t	sec = 0;
  time_t	nsec = 0;
  time_t	n_sec = 0;
  time_t	n_nsec = 0;
  long int	fac = 0;
  long int	dec = 0;

  printf("sec : %s\n", av[1]);
  printf("nsec : %s\n", av[2]);
  printf("facteur : %s\n", av[3]);
  printf("décalage : %s\n", av[4]);

  sec = atol(av[1]);
  nsec = atol(av[2]);
  fac = atol(av[3]);
  dec = atol(av[4]);

  printf("sec : [%ld]\n", sec);
  printf("nsec : [%ld]\n", nsec);
  printf("facteur : [%ld]\n", fac);
  printf("décalage : [%ld]\n", dec);

  n_sec = sec / fac;
  n_nsec = nsec / fac + (1000000000 * (sec % fac)) / fac;

  printf("sec : [%ld]\n", n_sec);
  printf("nsec : [%ld]\n", n_nsec);


  int nsec1 = (nsec * fac) % 1000000000;
  int sec_over_nsec = (nsec * fac) / 1000000000;
  n_sec = 1414415707 + (sec-1414415707) * fac + sec_over_nsec;
  n_nsec = nsec1;

  printf("sec : [%ld]\n", n_sec);
  printf("nsec : [%ld]\n", n_nsec);

  return (0);
}
