#include		<stdio.h>
#include		<time.h>
#include		<sys/time.h>

int			main(void)
{
  struct timespec	tp;
  time_t		t;
  int			c;
  struct timeval	tv;
  time_t		t2;
  struct itimerval	n_inter;
  struct itimerval	o_inter;
  

  clock_gettime(CLOCK_REALTIME, &tp);
  printf("clock_gettime :1: [%ld, %ld]\n", tp.tv_sec, tp.tv_nsec);

  tp.tv_sec = 5;
  tp.tv_nsec = 500000000L;
  nanosleep(&tp, NULL);

  clock_gettime(CLOCK_REALTIME, &tp);
  printf("clock_gettime :2: [%ld, %ld]\n", tp.tv_sec, tp.tv_nsec);

  clock_gettime(CLOCK_MONOTONIC, &tp);
  printf("clock_gettime :MONOTONIC: [%ld, %ld]\n", tp.tv_sec, tp.tv_nsec);

  sleep(1);
  
  t = time(NULL);
  printf("time :: [%d]\n", t);

  sleep(1);

  t = time(&t2);
  printf("time :: [%d:%d]\n", t, t2);
  
  //  t = time((time_t *)-1);
  // printf("time :: [%d:%d]\n", t, t2);
  
  sleep(1);

  c = gettimeofday(&tv, NULL);
  printf("gettimeofday :: %d-[%ld:%ld]\n", c, tv.tv_sec, tv.tv_usec);

  n_inter.it_interval.tv_sec = 12;
  n_inter.it_interval.tv_usec = 0;
  
  c = setitimer(ITIMER_REAL, &n_inter, NULL);
  printf("setitimer :: %d [%ld:%ld]\n", c,
	 n_inter.it_interval.tv_sec,
	 n_inter.it_interval.tv_usec);

  sleep(5);
  

  c = setitimer(ITIMER_REAL, &n_inter, &o_inter);
  printf("setitimer :: %d [%ld:%ld][%ld:%ld]\n", c,
	 n_inter.it_interval.tv_sec,
	 n_inter.it_interval.tv_usec,
	 o_inter.it_interval.tv_sec,
	 o_inter.it_interval.tv_usec);
    
  printf("alarm\n");
  alarm(100);
  printf("SELECT\n");
  tv.tv_sec = 200;
  tv.tv_usec = 50;
  t = time(NULL);
  printf("time_bselect :: [%d]\n", t);
  c = select(0, NULL, NULL, NULL, &tv);
  t = time(NULL);
  printf("time_aselect :: [%d]\n", t);
  t = time(NULL);
  printf("time_bselect :: [%d]\n", t);
  c = select(0, NULL, NULL, NULL, (void *)-1);
  t = time(NULL);
  printf("time_aselect :: [%d]\n", t);
  printf("POLLLLL\n");
  poll(NULL, 0, 10000);
  printf("POLLLLL2\n");
  return;
}
