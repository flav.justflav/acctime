#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

void sigalrm_handler(int sig)
{
	char outstr[200];
	struct tm *tm_gtod;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	tm_gtod = malloc(sizeof(*tm_gtod));
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf("ALM [%s]\n", outstr);
	exit(1);
}

int  main(int ac, char **av)
{
	char outstr[200];
	struct tm *tm_gtod;
	struct timeval tv;

	signal(SIGALRM, sigalrm_handler);   

	alarm(atoi(av[1]));

	tm_gtod = malloc(sizeof(*tm_gtod));
	gettimeofday(&tv, NULL);
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf(" IN [%s]\n", outstr);
	sleep(atoi(av[2]));
	gettimeofday(&tv, NULL);
	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	printf(" OUT [%s]\n", outstr);
	exit(0);
}

