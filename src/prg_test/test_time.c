#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

extern int errno;

int main(int ac, char **av)
{
    time_t current_time;
    time_t truc;
    time_t *truc2;

    current_time = time(&truc);
    printf("[%d], [%d]\n", current_time, truc);

    current_time = time(NULL);
    printf("[%d], [%p]\n", current_time, NULL);

    truc2 = 12L;
    current_time = time(truc2);
    printf("[%d], [%p]\n", current_time, NULL);
}
