#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <unistd.h>
#include <linux/futex.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/shm.h>

int futex(int* uaddr, int futex_op, int val, const struct timespec* timeout,
	  int* uaddr2, int val3)
{
	return syscall(SYS_futex, uaddr, futex_op, val, timeout, uaddr2, val3);
}

void do_parent(pid_t child, pid_t parent, time_t interrupt, int *futex_var)
{	
	int futex_rc = 0;

	printf("PARENT:: parent=%d, child=%d\n", parent, child);
	if (interrupt) {
		printf("interrupt in %ld s\n", interrupt);
		*futex_var = 1;
		sleep(interrupt);
		futex_rc = futex(futex_var, FUTEX_WAKE, 1, NULL, NULL, 0);
		printf("PARENT:: rc=%d, errno='%m' (%d)\n", futex_rc, errno);
	}
	printf("wait for end of child\n");
	wait(NULL);
}

void do_child(pid_t parent, struct timespec *target_time, int *futex_var)
{
	pid_t pid = getpid();
	int rc = 0;

	printf("CHILD:: parent=%d, child=%d\n", parent, pid);
	rc = futex(futex_var, FUTEX_WAIT_BITSET | FUTEX_CLOCK_REALTIME,
		   1, target_time, NULL, FUTEX_BITSET_MATCH_ANY);
	printf("CHILD:: rc=%d, errno='%m' (%d)\n", rc, errno);
}

int main(int ac, char **av)
{
	struct timespec timeout = {.tv_sec = 0, .tv_nsec = 0};
	struct timespec target_time;
	char str_target_time[64];
	time_t interrupt = 0;
	pid_t pid = 0;
	pid_t ppid = 0;
	int shm_id;
	int *futex_var;
 
	shm_id = shmget(IPC_PRIVATE, 4096, IPC_CREAT | 0666);
	if (shm_id < 0) {
		perror("shmget");
		exit(1);
	}
 
	futex_var = shmat(shm_id, NULL, 0);
	*futex_var = 1;

	ppid = getpid();
	if (ac < 3) {
		printf("Usage: %s timeout (s) duree_interrup[0 pour non] (s)\n",
		       av[0]);
		exit(EXIT_FAILURE);
	}
	timeout.tv_sec = strtoll(av[1], NULL, 10);
	interrupt = strtoll(av[2], NULL, 10);
	printf("timeout valid: [%ld]\n", timeout.tv_sec);
	printf("père interrup: [%ld]\n", interrupt);
	clock_gettime(CLOCK_REALTIME, &target_time);
	target_time.tv_sec += timeout.tv_sec;
	target_time.tv_nsec += timeout.tv_nsec;
	strftime(str_target_time, 63, "%F %T",
		 localtime(&(target_time.tv_sec)));
	printf("target time: [%s]\n", str_target_time);

	if ((pid = fork())) {
		do_parent(pid, ppid, interrupt, futex_var);
	} else {
		do_child(ppid, &target_time, futex_var);
	}
	return (EXIT_SUCCESS);
}
