#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
  char buffer[30];
  time_t curtime, curtime2, curtime4;
  int cas1, cas2, cas3, cas4, cas5;
  struct timeval tv;
  struct timeval tv2;
  struct timeval *tv5;
  struct timezone tz; 
   
  //cas 1 tv ok & tz ok
  printf("Cas1: Tv Ok Tz Ok\n");
  cas1 = gettimeofday(&tv, &tz);
  curtime = tv.tv_sec;
  strftime(buffer, 30, "%d-%m-%Y %T.", localtime(&curtime)); 
  printf("date: %s%ld\n",buffer,tv.tv_usec);
  printf("timestamp: %d\n",tv.tv_sec);
  printf("retour de gettimeofday: %d\n", cas1);
  
  ///////////////////////////////////
  printf("\n");
  //////////////////////////////////

  //cas 2 tv ok & tz NULL
  printf("Cas2: Tv Ok Tz NULL\n");
  printf("AVANT: %d\n",tv2.tv_sec);
  cas2 = gettimeofday(&tv2, NULL);
  printf("APRES: %d\n",tv.tv_sec);
  curtime2 = tv2.tv_sec;
  strftime(buffer, 30, "%d-%m-%Y %T.", localtime(&curtime2));
  printf("date: %s%ld\n",buffer,tv.tv_usec);
  printf("timestamp: %d\n",tv2.tv_sec);
  printf("retour de gettimeofday: %d\n", cas2);

  ///////////////////////////////////
  printf("\n");
  //////////////////////////////////
  
  //Cas 3 tv NULL & tz NULL
  printf("Cas3: Tv NULL Tz NULL\n");
  cas3 = gettimeofday(NULL, NULL);
  printf("retour de gettimeofday: %d\n", cas3);

  ///////////////////////////////////
  printf("\n");
  //////////////////////////////////
  
  // cas 4 passage d'un pointeur sur structure
  printf("Cas 4: on donne un pointeur sur une structure sans allocation mémoire\n");
  cas5 = gettimeofday(tv5, NULL);
  curtime4 = tv5->tv_sec;
  strftime(buffer, 30, "%d-%m-%Y %T.", localtime(&curtime));
  printf("%s%ld\n",buffer,tv5->tv_usec);
  printf("retour de gettimeofday: %d\n", cas5);
 
  return 0;
}
