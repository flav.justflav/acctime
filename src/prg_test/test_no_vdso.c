#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>

int			main(void)
{
  struct timespec	tp;
  int r;
  
  r = syscall(SYS_clock_gettime, CLOCK_REALTIME, &tp);
  printf("clock_gettime :1: %d [%ld, %ld]\n", r, tp.tv_sec, tp.tv_nsec);
  perror("ARRG");
  tp.tv_sec = 5;
  tp.tv_nsec = 500000000L;
  nanosleep(&tp, NULL);

  r = syscall(SYS_clock_gettime, CLOCK_REALTIME, &tp);
  printf("clock_gettime :2: %d [%ld, %ld]\n", r, tp.tv_sec, tp.tv_nsec);
  perror("ARRG");

  r = syscall(SYS_clock_gettime, CLOCK_MONOTONIC, &tp);
  printf("clock_gettime :MONOTONIC: %d [%ld, %ld]\n", r, tp.tv_sec, tp.tv_nsec);
  perror("ARRG");

  return 0;
}
