#include <linux/futex.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/syscall.h>

#define futex(a, b, c, d, e, f) syscall(SYS_futex, a, b, c, d, e, f)

void wake_futex_blocking(int *futex_addr) {
	while (1) {
		int futex_rc = futex(futex_addr, FUTEX_WAKE, 1, NULL, NULL, 0);
		if (futex_rc == -1) {
			perror("futex");
			exit(1);
		} else if (futex_rc > 0) {
			return;
		}
	}
}

void wait_on_futex_value(int *futex_addr, int val) {
	while (1) {
		int futex_rc = futex(futex_addr, FUTEX_WAIT, val, NULL, NULL, 0);
		if (futex_rc == -1) {
			if (errno != EAGAIN) {
			perror("futex");
			exit(1);
			}
		} else if (futex_rc == 0) {
			if (*futex_addr == val) {
				return;
			}
		} else {
			abort();
		}
	}
}

int main(int argc, char **argv) {
	int shm_id = shmget(IPC_PRIVATE, 4096, IPC_CREAT | 0666);

	if (shm_id < 0) {
		perror("shmget");
		exit(1);
	}

	int *shared_data = shmat(shm_id, NULL, 0);
	*shared_data = 0;

	int forkstatus = fork();
	if (forkstatus < 0) {
		perror("fork");
		exit(1);
	}

	if (forkstatus == 0) {
		printf("fils attente A\n");
		wait_on_futex_value(shared_data, 0xA);
		printf("fils écrit B\n");
		*shared_data = 0xB;
		wake_futex_blocking(shared_data);
	} else {
		printf("parent écrit A\n");
		*shared_data = 0xA;
		wake_futex_blocking(shared_data);
		printf("parent attente B\n");
		wait_on_futex_value(shared_data, 0xB);

		wait(NULL);
		shmdt(shared_data);
	}
	return 0;
}
