#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int ac, char **av)
{
	struct timeval tv;
	int r;
	time_t t;
	struct tm *tm_gtod;
	struct tm *tm_time;
	
	char outstr[200];
	char outstr2[200];
	
	r = gettimeofday(&tv, NULL);
	t = time(NULL);
	tm_gtod = malloc(sizeof(*tm_gtod));
	tm_time = malloc(sizeof(*tm_time));
	printf("gettimeofday :: r=%d / [%ld / %ld]\ttime=%ld\t\t", r,
	       tv.tv_sec, tv.tv_usec, t);

	tm_gtod = localtime(&(tv.tv_sec));
	strftime(outstr, sizeof(outstr), "%F %T", tm_gtod);
	tm_time = localtime(&t);
	strftime(outstr2, sizeof(outstr2), "%F %T", tm_time);

	printf("[%s]\t[%s]\n", outstr, outstr2);
	return 0;
}
