#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int main(int ac, char **av)
{
    struct timespec tim;

    tim.tv_sec = strtoul(av[1], NULL, 10);
    tim.tv_nsec = strtoul(av[2], NULL, 10);
    printf("sec=%ld , nsec=%ld\n", tim.tv_sec, tim.tv_nsec);

    int r = nanosleep(&tim, NULL);

    printf("r=%d\n", r);

    exit(EXIT_SUCCESS);
}
