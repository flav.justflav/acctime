#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#define PROCFS_MAX_SIZE		1024
#define PROCFS_ACCTIME_DIR		"AccTime"
#define PROCFS_ACCTIME_LST 		"lst"
#define PROCFS_ACCTIME_DEL 		"del"
#define PROCFS_ACCTIME_ADD 		"add"

#define TMP_BUF_MAX_SIZE	128
#define ACCTIME_MAX_SPACE	32

static struct proc_dir_entry *acctime_dir;
static struct proc_dir_entry *acctime_lst;
static struct proc_dir_entry *acctime_del;
static struct proc_dir_entry *acctime_add;

struct	acctime_space
{
  int	factor;
  int	ref;
  int	dec;
};

struct acctime_space tab_acctime_space[ACCTIME_MAX_SPACE];

int	procfile_lst_read(char *buffer, char **buffer_location,
			  off_t offset, int buffer_length, int *eof, void *data)
{
  int	i = 0;
  char	tmp_buf[PROCFS_MAX_SIZE];
  int	rt = 0;

  memset(tmp_buf, 0, PROCFS_MAX_SIZE);
  printk("PROCFS :: procfile_lst_read (/proc/%s/%s) called\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_LST);
  while (i < ACCTIME_MAX_SPACE)
    {
      int	r = 0;
      char	str[TMP_BUF_MAX_SIZE];

      memset(str, 0, TMP_BUF_MAX_SIZE);
      printk("PROCFS :: space %d : %d,%d,%d\n", i, tab_acctime_space[i].factor,
	     tab_acctime_space[i].ref,
	     tab_acctime_space[i].dec);
      r = snprintf(str, 127, "%d:%d:%d:%d\n", i, tab_acctime_space[i].factor,
	       tab_acctime_space[i].ref,
	       tab_acctime_space[i].dec);
      strncat(tmp_buf, str, r);
      rt += r;
      i++;
    }
  memcpy(buffer, tmp_buf, rt+1);
  printk("PROCFS :: [[%s]]\n", tmp_buf);
  printk("PROCFS :: [[%s]]\n", buffer);
  return (rt);
}

/*
int	procfile_lst_write(struct file *file, const char *buffer, unsigned long count,
		       void *data)
{
  procfs_buffer_size = count;
  if (procfs_buffer_size > PROCFS_MAX_SIZE )
    {
      procfs_buffer_size = PROCFS_MAX_SIZE;
    }
  if ( copy_from_user(procfs_buffer, buffer, procfs_buffer_size) ) {
    return -EFAULT;
  }
  
  printk("PROCFS :: procfile_write (%s) called {%s}\n", PROCFS_ACCTIME_DIR, (char *) buffer);
  
  return procfs_buffer_size;
}
*/
/*
int	procfile_del_read(char *buffer, char **buffer_location,
			  off_t offset, int buffer_length, int *eof, void *data)
{
  int ret;
  
  printk("PROCFS :: procfile_del_read (/proc/%s/%s) called\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_DEL);
  if (offset > 0)
    {
      ret = 0;
    }
  else
    {
      memcpy(buffer, procfs_buffer, procfs_buffer_size);
      ret = procfs_buffer_size;
    }
  return ret;
}
*/

int	procfile_del_write(struct file *file, const char *buffer, unsigned long count,
		       void *data)
{
  char	tmp_buf[TMP_BUF_MAX_SIZE];
  int	space;

  memset(tmp_buf, 0, TMP_BUF_MAX_SIZE);
  if (copy_from_user(tmp_buf, buffer, count))
    {
      return -EFAULT;
    }
  tmp_buf[count] = 0;
  if (kstrtoint(tmp_buf, 10, &space))
    return (-1);
  if (space > ACCTIME_MAX_SPACE-1)
    return (-1);
  tab_acctime_space[space].factor = 0;
  tab_acctime_space[space].ref = 0;
  tab_acctime_space[space].dec = 0;

  printk("PROCFS :: procfile_add_write (%s) called [%d:%d:%d:%d]\n", PROCFS_ACCTIME_DEL, 
	 space,
	 tab_acctime_space[space].factor,
	 tab_acctime_space[space].ref,
	 tab_acctime_space[space].dec);

  return (count);
}

/*
int	procfile_add_read(char *buffer, char **buffer_location,
			  off_t offset, int buffer_length, int *eof, void *data)
{
  int ret;
  
  printk("PROCFS :: procfile_add_read (/proc/%s/%s) called\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_ADD);
  if (offset > 0)
    {
      ret = 0;
    }
  else
    {
      memcpy(buffer, procfs_buffer, procfs_buffer_size);
      ret = procfs_buffer_size;
    }
  return ret;
}
*/

int	procfile_add_write(struct file *file, const char *buffer, unsigned long count,
		       void *data)
{
  char	tmp_buf[PROCFS_MAX_SIZE];
  char	*factor; 
  char	*dec;
  char	*ref;
  char	*space_s;
  int	space;
  char	*seek;

  if (copy_from_user(tmp_buf, buffer, count))
    {
      return -EFAULT;
    }
  tmp_buf[count] = 0;
  space_s = tmp_buf;
  seek = strchr(tmp_buf, ':');
  factor = seek+1;
  *seek = 0;  
  if (kstrtoint(space_s, 10, &space))
    return (-1);
  if (space > ACCTIME_MAX_SPACE-1)
    return (-1);
  seek = strchr(factor, ':');
  ref = seek+1;
  *seek = 0;  
  if (kstrtoint(factor, 10, &(tab_acctime_space[space].factor)))
    return (-1);

  seek = strchr(ref, ':');
  dec = seek+1;
  *seek = 0;  
  if (kstrtoint(ref, 10, &(tab_acctime_space[space].ref)))
    return (-1);

  if (kstrtoint(dec, 10, &(tab_acctime_space[space].dec)))
    return (-1);
  
  printk("PROCFS :: procfile_add_write (%s) called [%d:%d:%d:%d]\n", PROCFS_ACCTIME_ADD, 
	 space,
	 tab_acctime_space[space].factor,
	 tab_acctime_space[space].ref,
	 tab_acctime_space[space].dec);
  return count;
}

int	init_module()
{
  int	i = 0;

  memset(tab_acctime_space, 0, sizeof(tab_acctime_space));

  while (i < ACCTIME_MAX_SPACE)
    {
      printk("PROCFS :: space %d : %d,%d,%d\n", i, tab_acctime_space[i].factor,
	     tab_acctime_space[i].ref,
	     tab_acctime_space[i].dec);
      i++;
    }

  acctime_dir = proc_mkdir(PROCFS_ACCTIME_DIR, NULL);
  if(!acctime_dir)
    {
      printk("PROCFS :: Error creating proc entry : %s", PROCFS_ACCTIME_DIR);
      return -ENOMEM;
    }
  printk("PROCFS :: /proc/%s created\n", PROCFS_ACCTIME_DIR);	
  
  acctime_add = create_proc_entry(PROCFS_ACCTIME_ADD, 0644, acctime_dir);
  if (acctime_add == NULL)
    {
      remove_proc_entry(PROCFS_ACCTIME_ADD, acctime_dir);
      printk("PROCFS :: Error: Could not initialize /proc/%s/%s\n",
	     PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_ADD);
      return -ENOMEM;
    }
  /*  acctime_add->read_proc  = procfile_add_read;*/
  acctime_add->write_proc = procfile_add_write;
  acctime_add->mode 	  = S_IFREG | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
  acctime_add->uid 	  = 0;
  acctime_add->gid 	  = 1402;
  printk("PROCFS :: /proc/%s/%s created\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_ADD);

  acctime_del = create_proc_entry(PROCFS_ACCTIME_DEL, 0644, acctime_dir);
  if (acctime_del == NULL)
    {
      remove_proc_entry(PROCFS_ACCTIME_DEL, acctime_dir);
      printk("PROCFS :: Error: Could not initialize /proc/%s/%s\n",
	     PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_DEL);
      return -ENOMEM;
    }
  /*  acctime_del->read_proc  = procfile_del_read;*/
  acctime_del->write_proc = procfile_del_write;
  acctime_del->mode 	  = S_IFREG | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;
  acctime_del->uid 	  = 0;
  acctime_del->gid 	  = 1402;
  printk("PROCFS :: /proc/%s/%s created\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_DEL);

  acctime_lst = create_proc_entry(PROCFS_ACCTIME_LST, 0644, acctime_dir);
  if (acctime_lst == NULL)
    {
      remove_proc_entry(PROCFS_ACCTIME_LST, acctime_dir);
      printk("PROCFS :: Error: Could not initialize /proc/%s/%s\n",
	     PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_LST);
      return -ENOMEM;
    }
  acctime_lst->read_proc  = procfile_lst_read;
  /*  acctime_lst->write_proc = procfile_lst_write;  */
  acctime_lst->mode 	  = S_IFREG | S_IRUSR | S_IRGRP;
  acctime_lst->uid 	  = 0;
  acctime_lst->gid 	  = 1402;
  printk("PROCFS :: /proc/%s/%s created\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_LST);

  return 0;
}

void	cleanup_module()
{
  remove_proc_entry(PROCFS_ACCTIME_ADD, acctime_dir);
  printk("PROCFS :: /proc/%s/%s removed\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_ADD);

  remove_proc_entry(PROCFS_ACCTIME_DEL, acctime_dir);
  printk("PROCFS :: /proc/%s/%s removed\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_DEL);

  remove_proc_entry(PROCFS_ACCTIME_LST, acctime_dir);
  printk("PROCFS :: /proc/%s/%s removed\n", PROCFS_ACCTIME_DIR, PROCFS_ACCTIME_LST);

  remove_proc_entry(PROCFS_ACCTIME_DIR, NULL);
  printk("PROCFS :: /proc/%s removed\n", PROCFS_ACCTIME_DIR);
}
