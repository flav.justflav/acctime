// SPDX-License-Identifier: GPL-2.0

/*
 * Copyright (C) 2014-2019 ITSGroup
 *
 * kacctime module kernel
 *
 * alter the time perception of user processus
 *
 * Copyright (C) 2014-2019 Flavien ASTRAUD <flav@itsgroup.com>
 * Copyright (C) 2014-2019 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 *
 * Released under gpl-v2 licence
 *
 */

/*
 * Version : 
 * v0.412 -> ajout du découpage du 'sleep'
 * v0.412 -> ajout la déactivation du code de démarrage
 * v0.412 -> logs par niveau
 * v0.4124 -> stats dans /proc & logs
 * v0.41245 -> select, alarm, poll, epoll_wait
 * v0.412454 -> ajout clock_monolitic & futex
 * v0.412454 -> ajout semtimedop
 * v0.4124540 -> acc via le nom du process
 * v0.5 -> correction biais sleep
 * 2017-06-06 : v0.6 -> impl./corrections des fonctions 32bit
 * 2017-09-25 : v0.65 -> correction bug seek_spacetime
 * 2017-12-11 : v0.7 -> début code ralentissement
 * 2018-08-22 : v0.8 -> correction diff addre sys_call_tab
 * 2018-09-04 : v0.85 -> flag for stats
 * 2018-11-06 : v0.9 -> debuggage de futex
 * 2022-12-05 : correction monotonic dans clock_gettime
 */

#include <linux/kallsyms.h>
#include <linux/errno.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/utsname.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/hashtable.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/futex.h>

MODULE_VERSION("2022-12-05 15:27:08-0.91");

static long	kacctime_log_mode = 0;
static long	kacctime_stats_flag = 0;

module_param(kacctime_log_mode, long, 0);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

module_param(kacctime_stats_flag, long, 0);
MODULE_PARM_DESC(kacctime_stats_flag, "flag for stats activation");

#define STATS_INACTIF	0
#define STATS_ACTIF	1

#define FCT_STATE_FLAG_ALL		"ALL"
#define FCT_STATE_FLAG_TIME		"TIME"
#define FCT_STATE_FLAG_NONE		"NONE"

static char	*kacctime_fct_state = FCT_STATE_FLAG_NONE;

module_param(kacctime_fct_state, charp, 0000);
MODULE_PARM_DESC(kacctime_fct_state, "Fct à activer");


#define FCT_ACTIF			1
#define FCT_INACTIF			0

#warning "DEBUG -> Activation partielle"
static int	fct_state_gettimeofday  = FCT_ACTIF;
static int	fct_state_time		= FCT_ACTIF;
static int	fct_state_clock_gettime = FCT_ACTIF;
static int	fct_state_alarm		= FCT_ACTIF;
static int	fct_state_setitimer     = FCT_ACTIF;
static int	fct_state_getitimer     = FCT_ACTIF;
static int	fct_state_nanosleep     = FCT_ACTIF;
static int	fct_state_select	= FCT_ACTIF;
static int	fct_state_poll		= FCT_ACTIF;
static int	fct_state_epoll		= FCT_ACTIF;
static int	fct_state_futex		= FCT_ACTIF;
static int	fct_state_semtimedop    = FCT_ACTIF;
/*
** SECURITÉ
*/
#define KACCTIME_NO_SECU	1
#define KACCTIME_CUSTOMER	"flav"

/*
 * DEFINE
 */
#define	KACCTIME_MAX_SPACE	4
#define KACCTIME_MAX_LEN	256
#define KACCTIME_BUF_SIZE	((KACCTIME_MAX_SPACE+1) * KACCTIME_MAX_LEN)
#define KACCTIME_CODE_SEP	':'
#define KACCTIME_CODE_SEP_STR	":"
#define KACCTIME_CODE_FREQ	300

#define KACCTIME_BITS		6

//#define KACCTIME_KERN_PATH	"/boot/System.map-"

#define KACCTIME_KERN_PATH	"/proc/kallsyms"

#define KACCTIME_SYSCALL_SYM	"sys_call_table"
#define KACCTIME_SYSCALL_SYM32	"ia32_sys_call_table"

#define CHAR_NEW_LINE		'\n'
#define CHAR_SPACE		' '
#define CHAR_END_STR		'\0'
#define CHAR_EQUAL		'='

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)

/*
** fractionner les sleep
*/ 
#define SLEEP_FRAC_CYCLE_DEFAULT	10
#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

static int	kacctime_sleep_frac_cycle = SLEEP_FRAC_CYCLE_DEFAULT;
static int	kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;

/*
** Structure et define pour la retro compatibilité 32bits
 */
#define __NR_time_32		13
#define __NR_nanosleep_32	162
#define __NR_clock_gettime_32	265
#define __NR_gettimeofday_32	78
#define __NR_setitimer_32       104
#define __NR_getitimer_32       105
#define __NR_alarm_32		27
#define __NR_select_32		142
#define __NR_poll_32		168
#define __NR_epoll_wait_32	256
#define __NR_futex_32		240

struct timespec_32 {
	int tv_sec;		/* seconds */
	int tv_nsec;		/* nanoseconds */
};

struct timeval_32 {
	int tv_sec;		/* seconds */
	int tv_usec;		/* microseconds */
};

struct itimerval_32 {
	struct timeval_32 it_interval;		/* next value */
	struct timeval_32 it_value;		/* current value */
};

/**
 * type qui manque 
 */
typedef unsigned long int nfds_t;

typedef union epoll_data {
	void *ptr;
	int fd;
	uint32_t u32;
	uint64_t u64;
} epoll_data_t;

struct epoll_event {
	uint32_t events;	/* Epoll events */
	epoll_data_t data;	/* User data variable */
} __EPOLL_PACKED;

/* pointeurs vers les tableaux des syscalls  */
static unsigned long *kacctime_syscall_table = NULL;
static unsigned long *kacctime_syscall_table_32 = NULL;

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
	struct timespec	mono_delta;
};

static struct kacctime_timespace kacctime_timespace_tab[KACCTIME_MAX_SPACE];

/* Prevert crash exiting module */
static atomic_t  kacctime_counter_sleep = ATOMIC_INIT(0);
static atomic_t  kacctime_counter32_sleep = ATOMIC_INIT(0);

static atomic_t  kacctime_counter_select = ATOMIC_INIT(0);
static atomic_t  kacctime_counter32_select = ATOMIC_INIT(0);

static atomic_t  kacctime_counter_poll = ATOMIC_INIT(0);
//static atomic_t  kacctime_counter32_poll = ATOMIC_INIT(0);

static atomic_t  kacctime_counter_epoll = ATOMIC_INIT(0);
//static atomic_t  kacctime_counter32_epoll = ATOMIC_INIT(0);

/**
 * struct kacctime_stats - compteur des appels systèmes.
 */
struct s_kacctime_stats {
	unsigned long int cmpt_time;
	unsigned long int cmpt_gettimeofday;
	unsigned long int cmpt_nanosleep;
	unsigned long int cmpt_clock_gettime;
	unsigned long int cmpt_setitimer;
	unsigned long int cmpt_getitimer;
	unsigned long int cmpt_time_32;
	unsigned long int cmpt_gettimeofday_32;
	unsigned long int cmpt_nanosleep_32;
	unsigned long int cmpt_clock_gettime_32;
	unsigned long int cmpt_setitimer_32;
	unsigned long int cmpt_getitimer_32;
	unsigned long int cmpt_alarm;
	unsigned long int cmpt_alarm_32;
	unsigned long int cmpt_select;
	unsigned long int cmpt_select_32;
	unsigned long int cmpt_poll;
	unsigned long int cmpt_poll_32;
	unsigned long int cmpt_epoll;
	unsigned long int cmpt_epoll_32;
	unsigned long int cmpt_futex;
	unsigned long int cmpt_semtimedop;
};

static struct s_kacctime_stats kacctime_stats_space_tab[KACCTIME_MAX_SPACE+1];

#define KACCTIME_NB_TAKEOVER		12

#define KACCTIME_NB_TIME		0
#define KACCTIME_NB_GETTIMEOFDAY	1
#define KACCTIME_NB_NANOSLEEP		2
#define KACCTIME_NB_CLOCK_GETTIME	3
#define KACCTIME_NB_SETITIMER		4
#define KACCTIME_NB_GETITIMER		5
#define KACCTIME_NB_ALARM		6
#define KACCTIME_NB_SELECT		7
#define KACCTIME_NB_POLL		8
#define KACCTIME_NB_EPOLL		9
#define KACCTIME_NB_FUTEX		10
#define KACCTIME_NB_SEMTIMEDOP		11

static char *tab_fct[] = {
	"time",
	"gettimeofday",
	"nanosleep",
	"clock_gettime",
	"setitimer",
	"getitimer",
	"alarm",
	"select",
	"poll",
	"epoll",
	"futex",
	"semtimedop",
	"time32",
	"gettimeofday32",
	"nanosleep32",
	"clock_gettime32",
	"setitimer32",
	"getitimer32",
	"alarm32",
	"select32",
	"poll32",
	"epoll32"
};

/**
 * struct s_kacctime_stats_comm - hashtable globale
 * @key: clé de la hash
 * @hash_list: structure de la hashtable
 * @cpt: compteur d'appel du syscall
 * @space: espace dans lequel le syscall a été appelé
 * @fct: idx de la fonction (pour retourner le nom cf. tab_fct)
 * @name: nom de la fonction
 */
struct s_kacctime_stats_comm {
	int key;
	struct hlist_node hash_list;
	int cpt;
	int space;
	int fct;
	char name[TASK_COMM_LEN];
};

DEFINE_HASHTABLE(kacctime_hstats, KACCTIME_BITS);

/*
** prog acc par le nom
*/
#define KACCTIME_PROGNAME_LIST_SIZE	16

/**
 * struct s_kacctime_prgnam_list - liste des prg accélerés
 * @tab: tableau contenant les noms
 * @len: longeur du tableau
 */
struct s_kacctime_prgnam_list {
	char tab[KACCTIME_PROGNAME_LIST_SIZE][TASK_COMM_LEN];
	int len;
};

static struct s_kacctime_prgnam_list kacctime_progname_list[KACCTIME_MAX_SPACE];

/**
 * /proc/ permet de manager le module
 * initialisation des espaces : add
 * modification du facteur d'accélération
 * modification des paramétres
 * Ajout des processus par nom de commandes
 */
#define KACCTIME_PROCDIR	"kAccTime"
#define KACCTIME_PROCCTL	"ctl"
#define KACCTIME_PROCPRM	"param"
#define KACCTIME_PROCADD	"add"
#define KACCTIME_PROCADD_PRGNAM	"add_prgnam"
#define KACCTIME_PROCUPD	"upd"
#define KACCTIME_PROCLST	"lst"
#define KACCTIME_PROCDEL	"del"
#define KACCTIME_PROCDEL_PRGNAM	"del_prgnam"
#define KACCTIME_PROCSTA	"sta"

static struct proc_dir_entry *kacctime_procdir;
static struct proc_dir_entry *kacctime_procctl;
static struct proc_dir_entry *kacctime_procprm;
static struct proc_dir_entry *kacctime_procadd;
static struct proc_dir_entry *kacctime_procadd_prgnam;
static struct proc_dir_entry *kacctime_procupd;
static struct proc_dir_entry *kacctime_proclst;
static struct proc_dir_entry *kacctime_procdel;
static struct proc_dir_entry *kacctime_procdel_prgnam;
static struct proc_dir_entry *kacctime_procsta;

static struct file_operations kacctime_procctl_fops;
static struct file_operations kacctime_procprm_fops;
static struct file_operations kacctime_procadd_fops;
static struct file_operations kacctime_procadd_prgnam_fops;
static struct file_operations kacctime_procupd_fops;
static struct file_operations kacctime_proclst_fops;
static struct file_operations kacctime_procdel_fops;
static struct file_operations kacctime_procdel_prgnam_fops;
static struct file_operations kacctime_procsta_fops;

static int kacctime_initstate = 0;
/*
 * log macro
 */
#define MSG_ARCH		"LNX"
#define INTERNAL_NAME		"kAccTime"		/*Virtual Time*/
#define TMP_BUF_MAX_SIZE	1024
#define FMT_TIME_T		"%ld"
#define FMT_MYTIME_T		"%ld"
#define FMT_CLKID		"%lld"
#define FMT_LL			"%ld"

#define FLAG_LOG_HASH_STR	"HASH"
#define FLAG_LOG_HASH		(1<<3)

#define FLAG_LOG_NANO_STR	"NANO"
#define FLAG_LOG_NANO		(1<<4)

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

/**
 * kacctime_log_func - fonction de log par niveau
 * @func: nom de la fonction dans laquelle on log
 * @line: ligne à laquelle on log
 * @comm: nom du process
 * @pid: pid du process
 * @level: niveau de log : voir la variable kacctime_log_mode
 * @fmt: chaîne de format
 * @...: argument à remplacer dans la chaîne
 *
 * Les paramétres de la fonction sont pré-rempli par la macro :
 * kacctime_log_level.
 */
static void kacctime_log_func(const char *func, int line, char *comm, int pid,
			      int level, char *fmt, ...)
{
  va_list ap;
  char tmp_buf[TMP_BUF_MAX_SIZE];
  int nb_car;
 
  va_start(ap, fmt);
  if (kacctime_log_mode < level)
	  //  if (0 != level) /* POUR LES TESTS */
	  return;

  nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
		    "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
		    comm, pid, func, line);
  printk(tmp_buf);
  nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
		     fmt, ap);
  printk(tmp_buf);
  return;
}

/*
** macro de calcul du temps

#define NSEC_PER_SEC 1000000000LL
#define USEC_PER_SEC 1000000LL
*/

#define kacctime_timespec_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) + \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) - \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_mul(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) * f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_div(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
	  								\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) / f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec)	\
			* n / d;					\
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) + \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) - \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_mul(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) * f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_div(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) / f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec)	\
			* n / d;					\
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

/*
** fin macro de calcul du temps
*/

/*
** Prototypes
*/
time_t (*original_time)(time_t *t);
long (*original_gettimeofday)(struct timeval *tv, struct timezone *tz);
long (*original_nanosleep)(const struct timespec *req, struct timespec *rem);
int (*original_nanosleep_32)(const struct timespec_32 *req,
			     struct timespec_32 *rem);
long (*original_clock_gettime)(clockid_t clk_id, struct timespec *tp);
int (*original_clock_gettime_32)(clockid_t clk_id, struct timespec_32 *tp);
int (*original_gettimeofday_32)(struct timeval_32 *tv, struct timezone *tz);
int (*original_time_32)(int *t);
long (*original_setitimer)(int which, const struct itimerval *new_value,
			   struct itimerval *old_value);
int (*original_setitimer_32)(int which, const struct itimerval_32 *new_value,
			     struct itimerval_32 *old_value);
long (*original_getitimer)(int which, struct itimerval *curr_value);
int (*original_getitimer_32)(int which, struct itimerval_32 *curr_value);
long (*original_alarm)(unsigned int sec);
int (*original_alarm_32)(unsigned int sec);
long (*original_select)(int nfds, fd_set *readfds, fd_set *writefds,
			fd_set *exceptfds, struct timeval *timeout);
int (*original_select_32)(int nfds, fd_set *readfds, fd_set *writefds,
			  fd_set *exceptfds, struct timeval_32 *timeout);
long (*original_poll)(struct pollfd *fds, nfds_t nfds, int timeout);
int (*original_poll_32)(struct pollfd *fds, nfds_t nfds, int timeout);
long (*original_epoll_wait)(int epfd, struct epoll_event *events,
			    int maxevents, int timeout);
int (*original_epoll_wait_32)(int epfd, struct epoll_event *events,
			      int maxevents, int timeout);
long (*original_futex)(int *uaddr, int futex_op, int val,
		       const struct timespec *timeout,
		       int *uaddr2, int val3);
long (*original_futex_32)(int *uaddr, int futex_op, int val,
			  const struct timespec_32 *timeout,
			  int *uaddr2, int val3);
long (*original_semtimedop)(int semid, struct sembuf *sops, size_t nsops,
			    const struct timespec *timeout);

/**
 * kacctime_mk_hash_key - fonction de calcul de la clé pour la table de hash
 * @name: nom du processus
 *
 * Return: la clé (int)
 */
static int	kacctime_mk_hash_key(char *name)
{
  int	k = 0;
  char	*s;

  s = name;
  while (*name++)
	  k = k^(*name);
  kacctime_log_level(6, "k=%s--%d\n", s, k);
  return (k);
}

/**
 * kacctime_add_stats - fonction d'ajout d'une entrée dans la table de hash
 * @space: l'espace auquel appartient le processus
 * @idx_fct: l'index de la fonction pour retrouver son nom
 * @gl_cpt: le compteur d'appel
 * 
 * Calcul de la clé ; on recherche dans la hash si l'entrée existe déja :
 * Même nom, même fonction et même espace,
 * si oui, on ajoute +1 au compteur
 * si non, on ajoute l'entrée dans la hash
 * 
 * Return: négatif si on a eu une erreur leur d'allocation mémoire,
 * sinon %0
 */
static int kacctime_add_stats(int space, int idx_fct, unsigned long int gl_cpt)
{
	struct s_kacctime_stats_comm *kstats;
	struct s_kacctime_stats_comm *itr;
	int key;

	key = kacctime_mk_hash_key(current->comm);
	hash_for_each_possible(kacctime_hstats, itr, hash_list, key) {
		if ((strcmp(itr->name, current->comm) == 0) &&
		    (itr->fct == idx_fct) &&
		    (itr->space == space)) {
			kacctime_log_level(6, "%s and %d already in hash\n",
					   current->comm, idx_fct);
			itr->cpt++;
			return (0);
		}
	}
	kstats = kmalloc(sizeof(*kstats), GFP_KERNEL);
	if (kstats == NULL) {
		kacctime_log_level(0, "Mem !\n");
		return (-ENOMEM);
	}
	kstats->space = space;
	kstats->key = key;
	kstats->name[TASK_COMM_LEN-1] = '\0';
	strncpy(kstats->name, current->comm, TASK_COMM_LEN-1);
	kstats->cpt = gl_cpt;
	kstats->fct = idx_fct;
	hash_add(kacctime_hstats, &(kstats->hash_list), kstats->key);
	return (0);
}

/**
 * kacctime_search_prog_acc_by_name - identification du processus
 * Cette fonction permet de chercher si le nom du processus figure
 * parmis ceux qui doivent être accéléré même si FLAV_TIME n'est pas
 * présente.
 * Return: négatif si le processus n'est pas trouvé, 
 *         %d: le premier espace dans lequel on a trouvé le nom
 *             du process
 */
static int	kacctime_search_prog_acc_by_name(void)
{
  int		i_space = 0;

  while (i_space < KACCTIME_MAX_SPACE) {
	  if (kacctime_timespace_tab[i_space].multi != 0) {
		  int i_prog = 0;
		  while (i_prog < KACCTIME_PROGNAME_LIST_SIZE) {
			  if (kacctime_progname_list[i_space].len == 0)
				  break;
			  if (strcmp(kacctime_progname_list[i_space].tab[i_prog],
				     current->comm) == 0) {
				  kacctime_log_level(0, "Trouvé [%d]: %s\n",
						     i_space,
						     current->comm);
				  return (i_space);
			  }
			  i_prog++;
		  }
	  }
	  i_space++;
  }
  return (-1);
}

/**
 * kacctime_seek_timespace - cherche l'espace du process en cours
 *
 * Parcours les listes de programmes (/proc/kAccTime/) à accélerer
 * ou
 * Parcours de l'environnement du processus, si la variable FLAV_TIME
 * existe, elle doit contenir le numéro de l'espace (s'il est valide)
 * 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 * 
 */
static int	kacctime_seek_timespace(void)
{
  char		*value;
  char		*p;
  char		*buf;
  int		retval;
  int		buf_len;


  retval = kacctime_search_prog_acc_by_name();
  if (retval != -1)
	  return (retval);
  
  buf_len = current->mm->env_end - current->mm->env_start;
  buf = kmalloc(buf_len, GFP_KERNEL);
  if (buf == NULL) {
	  kacctime_log_level(0, "Mem !\n");
	  return (-1);
  }
  retval = copy_from_user(buf, (char *) current->mm->env_start,
			  buf_len);
  if (retval != 0) {
	  kacctime_log_level(0, "Copy from user !\n");
	  kfree(buf);
	  return (-1);
  }
  value = p = buf;
  while (p < buf+buf_len) {
	  if (*p == CHAR_END_STR) {
		  int	space;
		  int	space_found;
		  
		  space_found = strncmp(value, KACCTIME_FLAV_TIME,
					KACCTIME_FLAV_TIME_SIZE-1);
		  if (space_found == 0) {
			  while (*value != CHAR_EQUAL)
				  value += (unsigned long int) 1;
			  value += (unsigned long int) 1;
			  if (kstrtoint(value, 10, &space) < 0) {
				  kacctime_log_level(0, "Bad space format !\n");
				  kfree(buf);
				  return (-1);
			  }
			  if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
				  kacctime_log_level(0, "space out of MAX_SPACE (%d) !\n", space);
				  kfree(buf);
				  return (-1);
			  }
			  kacctime_log_level(5, "space -> [%d]\n", space);
			  kfree(buf);
			  if (kacctime_timespace_tab[space].multi <= 0)
				  return (-1);
			  return (space);
		  }
		  value = p + (unsigned long int) 1;
	  }
	  p += (unsigned long int) 1;
  }
  kfree(buf);
  if (kacctime_timespace_tab[0].multi == 0)    
	  return (-1);
  return (0);
}

/**
* time functions -> voir les man 
*/
static long	kacctime_clock_gettime(clockid_t clk_id, struct timespec *tp);

/**
 * nanosleep -- suppend le proc appelant (voir le man)
 * Si le process est accéléré alors :
 * le temps est divisé par le facteur
 * puis la durée est divisé en intervalle pour pouvoir modifier le
 * rapport durant l'attente
 */
static long kacctime_nanosleep(const struct timespec *req,
				      struct timespec *rem)
{
  int err;
  int space;
  long retval;
  struct timespec kreq;
  struct timespec kreq_tmp;
  mm_segment_t oldfs;
  int multi;
  int divid;
  struct timespec t_zero;
  struct timespec t_un;
  long int duration = 0;
  int cycle = 1;
  
  atomic_inc(&kacctime_counter_sleep);
  space = kacctime_seek_timespace();
  kacctime_log_level(3, "(%d)-%d\n", space,
		     atomic_read(&kacctime_counter_sleep));

  if (kacctime_stats_flag == STATS_ACTIF) {
	  kacctime_stats_space_tab[space+1].cmpt_nanosleep++;
	  kacctime_add_stats(space, KACCTIME_NB_NANOSLEEP,
			     kacctime_stats_space_tab[space+1].cmpt_nanosleep);
  }
  
  if (space < 0) {
	  retval = (*original_nanosleep)(req, rem);
	  atomic_dec(&kacctime_counter_sleep);
	  return (retval);
  }

  oldfs = get_fs();
  set_fs(KERNEL_DS);
  err = copy_from_user(&kreq_tmp, req, sizeof(*req));
  if (err) {
	  kacctime_log_level(0, "Mem !\n");
	  retval = -1;
	  goto out;
  }

  retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_zero);
  if (retval != 0) {
	  kacctime_log_level(0, "pb clock_gettime !\n");
	  goto out;
  }
  while (true) {
	  multi = kacctime_timespace_tab[space].multi;
	  divid = kacctime_timespace_tab[space].divid;
	  if (kreq_tmp.tv_sec <= kacctime_sleep_frac_interval) {
		  kacctime_timespec_div(kreq_tmp, multi, kreq);
		  kacctime_timespec_mul(kreq, divid, kreq);
		  kacctime_log_level(3,
				     "[%s - (%d) %d] f:%d/%d / ori [%ld,%ld] -> \
new [%ld,%ld]\n",
				     current->comm, current->cred->uid.val,
				     current->pid, multi, divid,
				     kreq_tmp.tv_sec, kreq_tmp.tv_nsec,
				     kreq.tv_sec, kreq.tv_nsec);
		  retval = (*original_nanosleep)(&kreq, rem);
#warning "CODE à corriger !"
		  if (retval < 0)
			  goto out;
		  break;
	  }
	  if ((cycle % kacctime_sleep_frac_cycle) == 0) {
		  struct timespec	d_reel;
		  time_t		biais;
		  
		  retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_un);
		  if (retval != 0) {
			  kacctime_log_level(0, "pb clock_gettime !\n");
			  goto out;
		  }
		  kacctime_timespec_sub(t_un, t_zero, d_reel);
		  biais = d_reel.tv_sec - duration;
		  duration += biais;
		  if ((biais < 0) || (biais > kacctime_sleep_frac_interval)) {
			  kacctime_log_level(1,
					     "biais:(%d)-dur:(%d) / t0 [%ld,%ld] -> t1 [%ld,%ld]\n",
					     biais, duration,
					     t_zero.tv_sec, t_zero.tv_nsec,
					     t_un.tv_sec, t_un.tv_nsec);
		  }
		  if (kreq_tmp.tv_sec >= biais)
			  kreq_tmp.tv_sec -= biais;
		  else
			  kreq_tmp.tv_sec = 0;
		  if (kreq_tmp.tv_sec <= kacctime_sleep_frac_interval)
			  continue;
	  }
	  
	  kreq.tv_sec = kacctime_sleep_frac_interval;
	  kreq.tv_nsec = 0;
	  kacctime_timespec_div(kreq, multi, kreq);
	  kacctime_timespec_mul(kreq, divid, kreq);
	  
	  kacctime_log_level(2,"[%s - (%d) %d] f:%d/%d / ori [%ld,%ld]\
 -> new [%ld,%ld]\n",
			     current->comm, current->cred->uid.val,
			     current->pid, multi, divid, kreq_tmp.tv_sec, 0L,
			     kreq.tv_sec, kreq.tv_nsec);
	  retval = (*original_nanosleep)(&kreq, rem);
	  if (retval < 0)
		  goto out;
	  kreq_tmp.tv_sec -= kacctime_sleep_frac_interval;
	  duration += kacctime_sleep_frac_interval;
	  cycle++;
  }
  
 out:
  set_fs(oldfs);
  atomic_dec(&kacctime_counter_sleep);
  return (retval);
}


static int kacctime_nanosleep_32(const struct timespec_32 *req32,
					 struct timespec_32	  *rem32)
{
  struct timespec *req64 = NULL;
  struct timespec *rem64 = NULL;
  long retval;
  int err;
  mm_segment_t oldfs;
  
  kacctime_log_level(2, "NANO32\n");

  req64 = kmalloc(sizeof(*req64), GFP_USER);
  if (req64 == NULL)
	  goto out;

  err = get_user(req64->tv_sec, &(req32->tv_sec));
  if (err)
	  goto out_err_req;
  
  err = get_user(req64->tv_nsec, &(req32->tv_nsec));
  if (err)
	  goto out_err_req;

  if (rem32) {
	  rem64 = kmalloc(sizeof(*rem64), GFP_USER);
	  if (rem64 == NULL)
		  goto out_err_req;
	  err = get_user(rem64->tv_sec, &(rem32->tv_sec));
	  if (err)
		  goto out_err_rem;
	  err = get_user(rem64->tv_nsec, &(rem32->tv_nsec));
	  if (err)
		  goto out_err_rem;
  }

  oldfs = get_fs();
  set_fs(KERNEL_DS);
  retval = kacctime_nanosleep(req64, rem64);
  set_fs(oldfs);
  
  if (rem32) {
	  err = put_user(rem64->tv_sec, &(rem32->tv_sec));
	  if (err)
		  goto out_err_rem;
	  err = put_user(rem64->tv_nsec, &(rem32->tv_nsec));
	  if (err)
		  goto out_err_rem;
	  kfree(rem64);
  }
  
  kfree(req64);
  return (retval);
  
 out_err_rem:
  kfree(rem64);

 out_err_req:
  kfree(req64);

 out:
  kacctime_log_level(0, "Mem !\n");      
  return (-ENOMEM);
}

static long kacctime_clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int err;
	int space;
	long retval;	
	struct timespec ktp;
	struct timespec ktp_sav;
	int multi;
	int divid;
	struct timespec dec;
	struct timespec ref;

	retval = (*original_clock_gettime)(clk_id, tp);
	if (retval != 0)
		goto out;
	
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_clock_gettime++;
		kacctime_add_stats(space, KACCTIME_NB_CLOCK_GETTIME,
				   kacctime_stats_space_tab[space+1].cmpt_clock_gettime);
	}
	
	if (space < 0)
		goto out;

	if (clk_id == CLOCK_MONOTONIC) {
		retval = (*original_clock_gettime)(CLOCK_MONOTONIC, tp);
		if (retval != 0)
			goto out;
	}
  
	err = copy_from_user(&ktp, tp, sizeof(*tp));

	ktp_sav.tv_sec = ktp.tv_sec;
	ktp_sav.tv_nsec = ktp.tv_nsec;
	if (err)
		goto err_mem;
	
	if (clk_id > CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n",
				   clk_id, ktp.tv_sec, ktp.tv_nsec);
		goto out;
	}
  
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	dec = kacctime_timespace_tab[space].dec;
	ref = kacctime_timespace_tab[space].ref;
  
	kacctime_timespec_sub(ktp, ref, ktp);
	kacctime_timespec_frac(ktp, multi, divid, ktp);
	kacctime_timespec_add(ktp, ref, ktp);
	kacctime_timespec_add(ktp, dec, ktp);
	
	kacctime_log_level(3,
			   "ID=%d, [os=%ld ; ons=%ld] [ns=%ld ; nns=%ld]\n",
			   clk_id, ktp_sav.tv_sec, ktp_sav.tv_nsec,
			   ktp.tv_sec, ktp.tv_nsec);
  
	if (clk_id == CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
		kacctime_timespec_sub(ktp,
				      kacctime_timespace_tab[space].mono_delta,
				      ktp);
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
	}

	err = copy_to_user(tp, &ktp, sizeof(*tp));
	if (err)
		goto err_mem;
	
 out: 
	return (retval);
	
 err_mem:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);  
}

static int kacctime_clock_gettime_32(clockid_t clk_id, struct timespec_32 *tp)
{
	int err;
	int retval;
	struct timespec *tp64 = NULL;
	mm_segment_t oldfs;

	kacctime_log_level(2, "GETTIME32 %d\n", clk_id);

	tp64 = kmalloc(sizeof(*tp64), GFP_USER);
	if (tp64 == NULL) 
		goto out_err;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = kacctime_clock_gettime(clk_id, tp64);
	set_fs(oldfs);
	
	err = put_user(tp64->tv_sec, &(tp->tv_sec));
	if (err) 
		goto out_free;
	err = put_user(tp64->tv_nsec, &(tp->tv_nsec));
	if (err) 
		goto out_free;

	kfree(tp64);  
	return (retval);

 out_free:
	kfree(tp64);  
	
 out_err:
	kacctime_log_level(0, "Mem !\n");      
	return (-ENOMEM);
}

static time_t kacctime_time(time_t *t)
{
	int space;
	struct timespec tp;
	long retval;
	mm_segment_t oldfs;
	int multi;
	int divid;
	struct timespec dec;
	struct timespec ref;
	time_t sec;
	
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_time++;
		kacctime_add_stats(space, KACCTIME_NB_TIME,
				   kacctime_stats_space_tab[space+1].cmpt_time);
	}
	
	if (space < 0)
		return (original_time(t));
	
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	dec = kacctime_timespace_tab[space].dec;
	ref = kacctime_timespace_tab[space].ref;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = original_clock_gettime(CLOCK_REALTIME, &tp);
	set_fs(oldfs);

	sec = tp.tv_sec;
	
	kacctime_timespec_sub(tp, ref, tp);
	kacctime_timespec_frac(tp, multi, divid, tp);
	kacctime_timespec_add(tp, ref, tp);
	kacctime_timespec_add(tp, dec, tp);
	
	kacctime_log_level(3, "(%ld/%d):[old=%ld:new=%ld]\n",
			   retval, space, sec, tp.tv_sec);
	if (t != NULL)
		if (put_user(tp.tv_sec, t)) {
			kacctime_log_level(0, "Mem !\n");
			return (-EFAULT);
		}
	
	return (tp.tv_sec);
}

static int kacctime_time_32(int *t)
{
	kacctime_log_level(2, "TIME32\n");
	return kacctime_time((time_t *) t);
}

static long kacctime_gettimeofday(struct timeval *tv, struct timezone *tz)
{
	int err;
	int space;
	int retval;
	int multi;
	int divid;
	struct timeval ktv;
	struct timeval ktv_sav;
	struct timeval dec;
	struct timeval ref;
	
	retval = original_gettimeofday(tv, tz);
	if (retval == -1)
		return (-1);

	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_gettimeofday++;
		kacctime_add_stats(space, KACCTIME_NB_GETTIMEOFDAY,
				   kacctime_stats_space_tab[space+1].cmpt_gettimeofday);
	}
	
	if (space < 0)
		return (retval);
	
	err = copy_from_user(&ktv, tv, sizeof(*tv));
	if (err)
		goto out_err;


	ktv_sav.tv_sec = ktv.tv_sec;
	ktv_sav.tv_usec = ktv.tv_usec;
	
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	dec.tv_usec = 0;
	ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	ref.tv_usec = 0;
	
	kacctime_timeval_sub(ktv, ref, ktv);
	kacctime_timeval_frac(ktv, multi, divid, ktv);
	kacctime_timeval_add(ktv, ref, ktv);
	kacctime_timeval_add(ktv, dec, ktv);
	
	kacctime_log_level(3, "[os=%ld ; ous=%ld] :: [ns=%ld ; nus=%ld]\n", 
			   ktv_sav.tv_sec, ktv_sav.tv_usec,
			   ktv.tv_sec, ktv.tv_usec); 
		
	err = copy_to_user(tv, &ktv, sizeof(*tv));
	if (err)
		goto out_err;
	
	return (0);
	
 out_err:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);
}

static int kacctime_gettimeofday_32(struct timeval_32 *tv, struct timezone *tz)
{
	int err;
	int retval;
	struct timeval *tv64;
	mm_segment_t oldfs;

	kacctime_log_level(2, "GETTIMEOFDAY32\n");

	tv64 = kmalloc(sizeof(*tv64), GFP_USER);
	if (tv64 == NULL)
		goto out_err;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = kacctime_gettimeofday(tv64, tz);
	set_fs(oldfs);
	if (retval == -1)
		goto out_free;

	err = put_user(tv64->tv_sec, &(tv->tv_sec));
	if (err) 
		goto out_free;
	err = put_user(tv64->tv_usec, &(tv->tv_usec));
	if (err) 
		goto out_free;

	kfree(tv64);
	return (retval);
	
 out_free:
	kfree(tv64);
	
 out_err:
	kacctime_log_level(0, "Mem !\n");
	return -EFAULT;
}

static long kacctime_setitimer(int which, const struct itimerval *new_value,
			  struct itimerval *old_value)
{
	int retval;
	int multi;
	int divid;
	struct timespec dec;
	struct timespec ref;
	int space;
	struct itimerval acc_nv;
	mm_segment_t oldfs;
	int err;
	struct timeval	stime;
  
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_setitimer++;
		kacctime_add_stats(space, KACCTIME_NB_SETITIMER,
				   kacctime_stats_space_tab[space+1].cmpt_setitimer);
	}
	
	if (space < 0)
		return (original_setitimer(which, new_value, old_value));
	
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	dec = kacctime_timespace_tab[space].dec;
	ref = kacctime_timespace_tab[space].ref;
		
	err = copy_from_user(&(acc_nv.it_interval),
			     &(new_value->it_interval),
			     sizeof(new_value->it_interval));
	if (err)
		goto out_err;

	stime.tv_sec = acc_nv.it_interval.tv_sec;
	stime.tv_usec = acc_nv.it_interval.tv_usec;
	
	kacctime_timeval_frac(acc_nv.it_interval, divid, multi,
			      acc_nv.it_interval);
	
	kacctime_log_level(3, "[os=%ld ; ous=%ld] :: [ns=%ld ; nus=%ld]\n",
			   stime.tv_sec, stime.tv_usec,
			   acc_nv.it_interval.tv_sec,
			   acc_nv.it_interval.tv_usec);

	err = copy_from_user(&(acc_nv.it_value),
			     &(new_value->it_value),
			     sizeof(new_value->it_value));
	if (err)
		goto out_err;
	
	stime.tv_sec = acc_nv.it_value.tv_sec;
	stime.tv_usec = acc_nv.it_value.tv_usec;

	kacctime_timeval_frac(acc_nv.it_value, divid, multi, acc_nv.it_value);

	kacctime_log_level(3, "[os=%ld ; ous=%ld] :: [ns=%ld ; nus=%ld]\n",
			   stime.tv_sec, stime.tv_usec,
			   acc_nv.it_value.tv_sec,
			   acc_nv.it_value.tv_usec);
	
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = original_setitimer(which, &acc_nv, old_value);
	set_fs(oldfs);
	
	return (retval);
	
 out_err:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);
}

static int kacctime_setitimer_32(int which, const struct itimerval_32 *new_value,
			    struct itimerval_32 *old_value)
{
	int err;
	int retval;
	struct itimerval *new_value64;
	struct itimerval *old_value64;
	mm_segment_t oldfs;
  
	kacctime_log_level(2, "SETITIMER_32\n");

	new_value64 = kmalloc(sizeof(*new_value64), GFP_USER);
	old_value64 = kmalloc(sizeof(*old_value64), GFP_USER);
	if ((new_value64 == NULL) || (old_value64 == NULL))
		goto out_err;
	
	err = get_user(new_value64->it_interval.tv_sec,
		       &(new_value->it_interval.tv_sec));
	if (err) 
		goto out_free;
	err = get_user(new_value64->it_interval.tv_usec,
		       &(new_value->it_interval.tv_usec));
	if (err) 
		goto out_free;
	err = get_user(new_value64->it_value.tv_sec,
		       &(new_value->it_value.tv_sec));
	if (err) 
		goto out_free;
	err = get_user(new_value64->it_value.tv_usec,
		       &(new_value->it_value.tv_usec));

	if (old_value != NULL) {
		kacctime_log_level(2, "old_value ready\n");
		if (err) 
			goto out_free;
		err = get_user(old_value64->it_interval.tv_sec,
			       &(old_value->it_interval.tv_sec));
		if (err) 
			goto out_free;
		err = get_user(old_value64->it_interval.tv_usec,
			       &(old_value->it_interval.tv_usec));
		if (err) 
			goto out_free;
		err = get_user(old_value64->it_value.tv_sec,
			       &(old_value->it_value.tv_sec));
		if (err) 
			goto out_free;
		err = get_user(old_value64->it_value.tv_usec,
			       &(old_value->it_value.tv_usec));
		if (err) 
			goto out_free;
	} else {
		kacctime_log_level(2, "old_value not ready\n");
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = kacctime_setitimer(which, new_value64, old_value64);
	set_fs(oldfs);
		
	if (retval == -1)
		goto out_free;

	if (old_value != NULL) {
		if (err) 
			goto out_free;
		err = put_user(old_value64->it_interval.tv_sec,
			       &(old_value->it_interval.tv_sec));
		if (err) 
			goto out_free;
		err = put_user(old_value64->it_interval.tv_usec,
			       &(old_value->it_interval.tv_usec));
		if (err) 
			goto out_free;
		err = put_user(old_value64->it_value.tv_sec,
			       &(old_value->it_value.tv_sec));
		if (err) 
			goto out_free;
		err = put_user(old_value64->it_value.tv_usec,
			       &(old_value->it_value.tv_usec));
		if (err) 
			goto out_free;
	}
	
	kfree(new_value64);
	kfree(old_value64);
	return (0);

 out_free:
	kfree(new_value64);
	kfree(old_value64);
	
 out_err:
	kacctime_log_level(0, "Mem !\n");      
	return -EFAULT;
}

static long kacctime_getitimer(int which, struct itimerval *curr_value)
{
	int space;
	long retval;
	struct itimerval kcurr;
	int multi;
	int divid;
	int err;
	mm_segment_t oldfs;
	
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_getitimer++;
		kacctime_add_stats(space, KACCTIME_NB_GETITIMER,
				   kacctime_stats_space_tab[space+1].cmpt_getitimer);
	}
	
	if (space < 0)
		return (original_getitimer(which, curr_value));

	
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = original_getitimer(which, &kcurr);
	set_fs(oldfs);

	kacctime_log_level(2, "retval=%d\n", retval);
	
	if (retval == -1)    
		return (retval);

	kacctime_log_level(2, "o [%d %d]\n",
			   kcurr.it_value.tv_sec,
			   kcurr.it_value.tv_usec);
	kacctime_log_level(2, "o [%d %d]\n",
			   kcurr.it_interval.tv_sec,
			   kcurr.it_interval.tv_usec);

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;
	
	kacctime_timeval_frac(kcurr.it_interval,
			      multi, divid,
			      kcurr.it_interval);

	kacctime_timeval_frac(kcurr.it_value,
			       multi, divid,
			       kcurr.it_value);
	
	kacctime_log_level(2, "n [%d %d]\n",
			   kcurr.it_value.tv_sec,
			   kcurr.it_value.tv_usec);
	kacctime_log_level(2, "n [%d %d]\n",
			   kcurr.it_interval.tv_sec,
			   kcurr.it_interval.tv_usec);

	err = put_user(kcurr.it_value.tv_sec,
		       &(curr_value->it_value.tv_sec));
	if (err)
		goto out_err_mem;
	err = put_user(kcurr.it_value.tv_usec,
		       &(curr_value->it_value.tv_usec));
	if (err)
		goto out_err_mem;	
	err = put_user(kcurr.it_interval.tv_sec,
		       &(curr_value->it_interval.tv_sec));
	if (err)
		goto out_err_mem;	
	err = put_user(kcurr.it_interval.tv_usec,
		       &(curr_value->it_interval.tv_usec));
	if (err)
		goto out_err_mem;	

	return (retval);

 out_err_mem:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);	
}

static int kacctime_getitimer_32(int which, struct itimerval_32 *curr_value)
{
	int err;
	int retval;
	struct itimerval *curr_value64;
	mm_segment_t oldfs;
	
	kacctime_log_level(2, "GETITIMER_32\n");

	curr_value64 = kmalloc(sizeof(*curr_value64), GFP_USER);
	if (curr_value64 == NULL)
		goto out_err;
	
	err = get_user(curr_value64->it_interval.tv_sec,
		       &(curr_value->it_interval.tv_sec));
	if (err) 
		goto out_free;
	err = get_user(curr_value64->it_interval.tv_usec,
		       &(curr_value->it_interval.tv_usec));
	if (err) 
		goto out_free;
	err = get_user(curr_value64->it_value.tv_sec,
		       &(curr_value->it_value.tv_sec));
	if (err) 
		goto out_free;
	err = get_user(curr_value64->it_value.tv_usec,
		       &(curr_value->it_value.tv_usec));
	if (err) 
		goto out_free;

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = kacctime_getitimer(which, curr_value64);
	set_fs(oldfs);

	if (retval == -1)
		goto out_free;

	err = put_user(curr_value64->it_interval.tv_sec,
		       &(curr_value->it_interval.tv_sec));
	if (err) 
		goto out_free;
	err = put_user(curr_value64->it_interval.tv_usec,
		       &(curr_value->it_interval.tv_usec));
	if (err) 
		goto out_free;
	err = put_user(curr_value64->it_value.tv_sec,
		       &(curr_value->it_value.tv_sec));
	if (err) 
		goto out_free;
	err = put_user(curr_value64->it_value.tv_usec,
		       &(curr_value->it_value.tv_usec));
	if (err) 
		goto out_free;
	
	kfree(curr_value64);
	return (0);

 out_free:
	kfree(curr_value64);
	
 out_err:
	kacctime_log_level(0, "Mem !\n");      
	return -EFAULT;
}

static long kacctime_alarm(unsigned int sec)
{
	int space;
	struct timespec nsec;
	int multi;
	int divid;
	
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_alarm++;
		kacctime_add_stats(space, KACCTIME_NB_ALARM,
				   kacctime_stats_space_tab[space+1].cmpt_alarm);
	}
	
	if (space < 0)
		return (original_alarm(sec));
	if (sec == 0)
		return (original_alarm(0));

	nsec.tv_sec = sec;
	nsec.tv_nsec = 0;
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

	kacctime_timespec_frac(nsec, divid, multi, nsec);

	kacctime_log_level(3, "os=%d / ns=%d\n", sec, nsec.tv_sec);

	if (nsec.tv_sec < 1)
		return (original_alarm(1));
	return (original_alarm(nsec.tv_sec));
}

static int kacctime_alarm_32(unsigned int sec)
{
  kacctime_log_level(2, "ALARM32\n");

  return (kacctime_alarm(sec));
}

static long kacctime_select(int nfds, fd_set *readfds, fd_set *writefds,
		       fd_set *exceptfds, struct timeval *timeout)
{
	int space;
	int retval;
	int err;
	int multi;
	int divid;
	mm_segment_t oldfs;
	struct timeval ktimeout;
	struct timeval ktimeout_sav;
	struct timespec t_zero;
	struct timespec t_un;
	long int duration = 0;
	int cycle = 1;


	/*	if (timeout != NULL) {
		err = get_user(ktimeout.tv_sec, &(timeout->tv_sec));
		if (err) 
			return -1;
		
		err = get_user(ktimeout.tv_usec, &(timeout->tv_usec));
		if (err)
			return -1;
		
		kacctime_log_level(0, "SELECT [%d ; %ld,%ld]\n", nfds,
				   ktimeout.tv_sec,
				   ktimeout.tv_usec);
				   }*/
	
	atomic_inc(&kacctime_counter_select);
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_select++;
		kacctime_add_stats(space, KACCTIME_NB_SELECT,
				   kacctime_stats_space_tab[space+1].cmpt_select);
	}
	
	if (space < 0) {
		retval = original_select(nfds, readfds, writefds,
					 exceptfds, timeout);
		goto out_dec;
	}

	if (timeout == NULL) {
		retval = original_select(nfds, readfds, writefds,
					 exceptfds, timeout);
		goto out_dec;
	}
	
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	
	err = get_user(ktimeout.tv_sec, &(timeout->tv_sec));
	if (err) 
		goto out_mem_fault;
	
	err = get_user(ktimeout.tv_usec, &(timeout->tv_usec));
	if (err)
		goto out_mem_fault;
	
	retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_zero);
	if (retval != 0) {
		kacctime_log_level(0, "pb clock_gettime !\n");
		goto out;
	}

	ktimeout_sav.tv_sec = ktimeout.tv_sec;
	ktimeout_sav.tv_usec = ktimeout.tv_usec;
	while (true) {
		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		ktimeout_sav.tv_sec = ktimeout.tv_sec;
				
		if (ktimeout.tv_sec <= kacctime_sleep_frac_interval) {
			ktimeout.tv_usec = ktimeout_sav.tv_usec;
			kacctime_timeval_frac(ktimeout, divid, multi,
					      ktimeout);	  
			kacctime_log_level(3, "LAST [%s - (%d) %d] \
f:%d/%d / ori [%ld,%ld] -> new [%ld,%ld]\n",
					   current->comm, current->cred->uid.val,
					   current->pid, multi, divid,
					   ktimeout_sav.tv_sec,
					   ktimeout_sav.tv_usec,
					   ktimeout.tv_sec,
					   ktimeout.tv_usec);
			retval = original_select(nfds, readfds, writefds,
						 exceptfds, &ktimeout);
			goto out;
		}

		if ((cycle % kacctime_sleep_frac_cycle) == 0) {
			struct timespec	d_reel;
			time_t biais;
			
			retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_un);
			if (retval != 0) {
				kacctime_log_level(0, "pb clock_gettime !\n");
				goto out;
			}
			d_reel = timespec_sub(t_un, t_zero);
			biais = d_reel.tv_sec - duration;
			duration += biais;
			if ((biais < 0) ||
			    (biais > kacctime_sleep_frac_interval))
				kacctime_log_level(1, "biais : (%d)\n", biais);
			if (ktimeout.tv_sec >= biais)
				ktimeout.tv_sec -= biais;
			else
				ktimeout.tv_sec = 0;
			if (ktimeout.tv_sec <= kacctime_sleep_frac_interval)
				continue;
		}

		ktimeout.tv_sec = kacctime_sleep_frac_interval;
		ktimeout.tv_usec = 0;
		kacctime_timeval_frac(ktimeout, divid, multi, ktimeout);
		kacctime_log_level(3, "[%s - (%d) %d] f:%d/%d / ori\
 [%ld,%ld] -> new [%ld,%ld]\n",
				   current->comm, current->cred->uid.val,
				   current->pid, multi, divid,
				   ktimeout_sav.tv_sec,
				   ktimeout_sav.tv_usec,
				   ktimeout.tv_sec,
				   ktimeout.tv_usec);
		retval = original_select(nfds, readfds, writefds, exceptfds,
					 &ktimeout);
		if (retval)
			goto out;
		ktimeout.tv_sec = ktimeout_sav.tv_sec -
			kacctime_sleep_frac_interval;
		duration += kacctime_sleep_frac_interval;
		cycle++;
	}
	
 out_mem_fault:
	kacctime_log_level(0, "Mem !\n");
	retval = -1;
 out:
	set_fs(oldfs);
 out_dec:
	atomic_dec(&kacctime_counter_select);
	return (retval);
}

static int kacctime_select_32(int nfds, fd_set *readfds, fd_set *writefds,
			 fd_set *exceptfds, struct timeval_32 *timeout)
{
	int retval;
	int err;
	struct timeval *ktimeout;
	mm_segment_t oldfs;
	
	//	kacctime_log_level(1, "SELECT32\n");

	ktimeout = kmalloc(sizeof(*ktimeout), GFP_USER);
	if (ktimeout == NULL)
		goto out_err;

	err = get_user(ktimeout->tv_sec, &(timeout->tv_sec));
	if (err)
		goto out_free;
	err = get_user(ktimeout->tv_usec, &(timeout->tv_usec));
	if (err)
		goto out_free;
	
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	retval = kacctime_select(nfds, readfds, writefds, exceptfds, ktimeout);
	set_fs(oldfs);

	kfree(ktimeout);
	return retval;

 out_free:
	kfree(ktimeout);
	
 out_err:
	return -1;
}

static long kacctime_poll(struct pollfd *fds, nfds_t nfds, int timeout)
{
	int space;
	long retval;
	struct timespec ktimeout;
	int timeout_sav;
	int err;
	mm_segment_t oldfs;
	int multi;
	int divid;
	
	atomic_inc(&kacctime_counter_poll);
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_poll++;
		kacctime_add_stats(space, KACCTIME_NB_POLL,
				   kacctime_stats_space_tab[space+1].cmpt_poll);
	}
	
	if ((space < 0) || (timeout < 0)) {
		retval = original_poll(fds, nfds, timeout);
		goto out;
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);
	
	err = get_user(ktimeout.tv_sec, &timeout);
	if (err) {
		kacctime_log_level(0, "Mem !\n");
		retval = -1;
		goto out_fs;
	}
	ktimeout.tv_nsec = 0;
	
	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;


#warning "Pas de BOUCLE !!!"

	timeout_sav = ktimeout.tv_sec;
	kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
	kacctime_log_level(4,"[%d] -> [%d]\n", timeout_sav, ktimeout.tv_sec);

	retval = original_poll(fds, nfds, ktimeout.tv_sec);
	
 out_fs:
	set_fs(oldfs);
 out:
	atomic_dec(&kacctime_counter_poll);
	return retval;
}

static int kacctime_poll_32(struct pollfd *fds, nfds_t nfds, int timeout)
{
	/*	kacctime_log_level(0, "POLL32\n");*/
	return kacctime_poll(fds, nfds, timeout);
}


static long kacctime_epoll_wait(int epfd, struct epoll_event *events,
			   int maxevents, int timeout)
{
	int space;
	struct timespec ktimeout;
	int timeout_sav;
	long retval;
	int err;
	mm_segment_t oldfs;
	int multi;
	int divid;
  
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_epoll++;
		kacctime_add_stats(space, KACCTIME_NB_EPOLL,
				   kacctime_stats_space_tab[space+1].cmpt_epoll);

	}
	
	if (space < 0)
		return (original_epoll_wait(epfd, events, maxevents, timeout));
	
	if (timeout < 0)
		return (original_epoll_wait(epfd, events, maxevents, timeout));
  
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	
	err = get_user(ktimeout.tv_sec, &timeout);
	if (err) {
		kacctime_log_level(0, "Mem !\n");
		retval = -1;
		goto out_fs;
	}
	ktimeout.tv_nsec = 0;

	multi = kacctime_timespace_tab[space].multi;
	divid = kacctime_timespace_tab[space].divid;

#warning "à vérifier -> pas de boucle"

	timeout_sav = ktimeout.tv_sec;
	kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
	retval = original_epoll_wait(epfd, events, maxevents, ktimeout.tv_sec);

 out_fs:
	set_fs(oldfs);
	atomic_dec(&kacctime_counter_poll);
	return retval;
}

static long kacctime_epoll_wait_32(int epfd, struct epoll_event *events,
				   int maxevents, int timeout)
{
	kacctime_log_level(0, "EPOLL32\n");
	return kacctime_epoll_wait(epfd, events, maxevents, timeout);
}

static long kacctime_futex(int *uaddr, int futex_op, int val,
		      const struct timespec *timeout,
		      int *uaddr2, int val3)
{
	long retval;
	int space;
	int multi;
	int divid;
	int err;	
	struct timespec ktimeout;
	mm_segment_t oldfs;
	int cmd = futex_op & FUTEX_CMD_MASK;
	
	space = kacctime_seek_timespace();
	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_futex++;
		kacctime_add_stats(space, KACCTIME_NB_FUTEX,
				   kacctime_stats_space_tab[space+1].cmpt_futex);
	}
	
	if ((space < 0)) {
		kacctime_log_level(2,"FUTEX (space=%d)\n", space);
		return original_futex(uaddr, futex_op, val,
				      timeout, uaddr2, val3);
	}
	
	if (timeout == NULL) {
		retval = original_futex(uaddr, futex_op, val, timeout,
					uaddr2, val3);
		return (retval);
	}

	if (cmd == FUTEX_WAIT || cmd == FUTEX_LOCK_PI ||
	    cmd == FUTEX_WAIT_BITSET ||
	    cmd == FUTEX_WAIT_REQUEUE_PI) {
		if (copy_from_user(&ktimeout, timeout, sizeof(ktimeout)) != 0) {
			kacctime_log_level(0, "E1\n");
			return -EFAULT;
		}
		
       		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
		
		kacctime_log_level(1, "FUTEX %d (%ld,%ld)->(%ld,%ld)\n",
				   futex_op, timeout->tv_sec, timeout->tv_nsec,
				   ktimeout.tv_sec, ktimeout.tv_nsec);
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		retval = original_futex(uaddr, futex_op, val,
					&ktimeout, uaddr2, val3);
		set_fs(oldfs);
		return (retval);
	}
	
	return original_futex(uaddr, futex_op, val,
			      timeout, uaddr2, val3);	
}

static int kacctime_futex_32(int *uaddr, int futex_op, int val,
			     const struct timespec_32 *timeout,
			     int *uaddr2, int val3)
{
	int retval;
	int space;
	int multi;
	int divid;
	int err;	
	struct timespec_32 ktimeout;
	mm_segment_t oldfs;
	int cmd = futex_op & FUTEX_CMD_MASK;
	
	space = kacctime_seek_timespace();
	if ((space < 0)) {
		return original_futex_32(uaddr, futex_op, val,
					 timeout, uaddr2, val3);
	}
	
	if (timeout == NULL) {
		retval = original_futex_32(uaddr, futex_op, val, timeout,
					   uaddr2, val3);
		return (retval);
	}

	if (cmd == FUTEX_WAIT || cmd == FUTEX_LOCK_PI ||
	    cmd == FUTEX_WAIT_BITSET ||
	    cmd == FUTEX_WAIT_REQUEUE_PI) {
		if (copy_from_user(&ktimeout, timeout, sizeof(ktimeout)) != 0) {
			kacctime_log_level(0, "E1\n");
			return -EFAULT;
		}
		
       		multi = kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
		
		kacctime_log_level(1, "FUTEX32 %d (%d,%d)->(%d,%d)\n",
				   futex_op, timeout->tv_sec, timeout->tv_nsec,
				   ktimeout.tv_sec, ktimeout.tv_nsec);
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		retval = original_futex_32(uaddr, futex_op, val,
					   &ktimeout, uaddr2, val3);
		set_fs(oldfs);
		return (retval);
	}
	
	return original_futex_32(uaddr, futex_op, val,
				 timeout, uaddr2, val3);	
}

static long kacctime_semtimedop(int semid, struct sembuf *sops,
			   size_t nsops,
			   const struct timespec *timeout)
{
	int space;
	mm_segment_t oldfs;
	int multi;
	int divid;
	int err;
	struct timespec ktimeout;
	struct timespec ktimeout_sav;
	long retval;
	struct timespec	t_zero;
	struct timespec	t_un;
	long int duration = 0;
	int cycle = 1;
	
	space = kacctime_seek_timespace();

	if (kacctime_stats_flag == STATS_ACTIF) {
		kacctime_stats_space_tab[space+1].cmpt_semtimedop++;
		kacctime_add_stats(space, KACCTIME_NB_SEMTIMEDOP,
				   kacctime_stats_space_tab[space+1].cmpt_semtimedop);
	}
	
	if ((space < 0) || (timeout == NULL))
		return (original_semtimedop(semid, sops, nsops, timeout));
	
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	
	err = get_user(ktimeout.tv_sec, &(timeout->tv_sec));
	if (err) {
		kacctime_log_level(0, "Mem !\n");
		retval = -EFAULT;
		goto out;
	}
	
	err = get_user(ktimeout.tv_nsec, &(timeout->tv_nsec));
	if (err) {
		kacctime_log_level(0, "Mem !\n");      
		retval = -EFAULT;
		goto out;
	}

	ktimeout_sav.tv_sec = ktimeout.tv_sec;
	ktimeout_sav.tv_nsec = ktimeout.tv_nsec;
	
	retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_zero);
	if (retval != 0) {
		kacctime_log_level(0, "pb clock_gettime !\n");
		goto out;
	}
	while (true) {
		multi =  kacctime_timespace_tab[space].multi;
		divid = kacctime_timespace_tab[space].divid;
		if (ktimeout.tv_sec <= kacctime_sleep_frac_interval) {
			kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
			kacctime_log_level(3, "[%s - (%d) %d]\
 f:%d/%d / ori [%ld,%ld] -> new [%ld,%ld]\n",
					   current->comm, current->cred->uid.val,
					   current->pid, multi, divid,
					   ktimeout_sav.tv_sec,
					   ktimeout_sav.tv_nsec,
					   ktimeout.tv_sec, ktimeout.tv_nsec);
			retval = (*original_semtimedop)(semid, sops, nsops,
							&ktimeout);
			break;
		}
		if ((cycle % kacctime_sleep_frac_cycle) == 0) {
			struct timespec	d_reel;
			time_t		biais;
			
			retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_un);
			if (retval != 0) {
				kacctime_log_level(0, "pb clock_gettime !\n");
				goto out;
			}
			d_reel = timespec_sub(t_un, t_zero);
			biais = d_reel.tv_sec - duration;
			duration += biais;
			if ((biais < 0) || (biais > kacctime_sleep_frac_interval))
				kacctime_log_level(0, "biais : (%d)\n", biais);
			if (ktimeout.tv_sec >= biais)
				ktimeout.tv_sec -= biais;
			else
				ktimeout.tv_sec = 0;
			if (ktimeout.tv_sec <= kacctime_sleep_frac_interval)
				continue;
		}		
		ktimeout.tv_sec = kacctime_sleep_frac_interval;
		ktimeout.tv_nsec = 0;
		kacctime_timespec_frac(ktimeout, divid, multi, ktimeout);
		kacctime_log_level(3,
				   "[%s - (%d) %d]\
 f:%d/%d / ori [%ld,%ld] -> new [%ld,%ld]\n",
				   current->comm, current->cred->uid.val,
				   current->pid, multi, divid,
				   ktimeout_sav.tv_sec, 0L,
				   ktimeout.tv_sec, ktimeout.tv_nsec);
		retval = (*original_semtimedop)(semid, sops, nsops, &ktimeout);
		if (retval != -EAGAIN)
			goto out;
		ktimeout.tv_sec -= kacctime_sleep_frac_interval;
	}
	
 out:
  set_fs(oldfs);
  return (retval);
}

/**
 * kacctime_get_version
 */
static void	kacctime_get_version(char *p)
{
	/*
	  snprintf(p, KACCTIME_MAX_LEN, "%s%s", KACCTIME_KERN_PATH,
	  utsname()->release);
	*/
	snprintf(p, KACCTIME_MAX_LEN, "%s", KACCTIME_KERN_PATH);
	return;
}


/**
 * kacctime_lookfor_sym - recherche de l'adresse du tableau des syscall
 * @kern: System map
 * @sym: symbole de la table des syscall
 *
 * Return: (%ld) adresse de la table
 */
static unsigned long	*kacctime_lookfor_sym(char *kern, char *sym)
{
  char			*p;
  char			*p_end;
  char			sym_string[KACCTIME_MAX_LEN];
  char			*loc_sym;
  unsigned long		*sym_ptr;
  
  p = kern;
  loc_sym = strstr(p, sym);
  if (loc_sym == NULL)
    return (NULL);      
  while (*loc_sym != CHAR_NEW_LINE)
    loc_sym--;
  loc_sym++;
  p_end = loc_sym;
  while (*p_end != CHAR_SPACE)
    p_end++;
  strncpy(sym_string, loc_sym, p_end-loc_sym);
  sym_string[p_end-loc_sym] = CHAR_END_STR;
  kacctime_log_level(0, "STR -> %s\n", sym_string);
  sym_ptr = (unsigned long *) simple_strtol(sym_string, NULL, 16);  
  return (sym_ptr);
}

/**
 * kacctime_get_syscall_table - Recherche du tableau de pointeur des syscalls
 *
 * @kernfile: System map
 *
 * Return: -1 si erreur, 0 si ok
 */
static int	kacctime_get_syscall_table(char *kernfile)
{
  mm_segment_t	fs;
  struct file	*f;
  int		kern_len;
  char		*kern_data;
  int		rcount;
  
  fs = get_fs();
  set_fs(KERNEL_DS);

  f = filp_open(kernfile, O_RDONLY, 0);
  if ( IS_ERR(f) || ( f == NULL ))
    {
      set_fs(fs);
      return (-1);
    }
  kern_len = vfs_llseek(f, 0, SEEK_END);
  kacctime_log_level(0, "kern size : %d\n", kern_len);
  kern_data = kmalloc(kern_len, GFP_KERNEL);
  if (kern_data == NULL)
    {
      set_fs(fs);
      filp_close(f, 0);
      return (-1);      
    }
  rcount = vfs_llseek(f, 0, 0);
  kacctime_log_level(0, "kern rcount : %d\n", rcount);
  if (rcount < 0)
    {
      set_fs(fs);
      filp_close(f, 0);
      return (-1);      
    }
  rcount = vfs_read(f, kern_data, kern_len, &f->f_pos);
  kacctime_log_level(0, "kern read len : %d\n", rcount);
  if (rcount != kern_len)
    {
      set_fs(fs);
      filp_close(f, 0);
      return (-1);      
    }
  set_fs(fs);
  filp_close(f, 0);  
  kacctime_syscall_table = kacctime_lookfor_sym(kern_data,
						KACCTIME_SYSCALL_SYM);
  if (kacctime_syscall_table == NULL)
    return (-1);
  kacctime_log_level(0, "sym addr : [%pK]\n", kacctime_syscall_table);
  kacctime_syscall_table_32 = kacctime_lookfor_sym(kern_data,
						   KACCTIME_SYSCALL_SYM32);
  if (kacctime_syscall_table_32 == NULL)
    return (-1);
  kacctime_log_level(0, "sym addr32 : [%pK]\n", kacctime_syscall_table_32);
  return (0);
 }


/**
 * kacctime_replace_syscall - remplacement des syscall
 *
 * En fonction des paramétres de démarrage du module, on active ou non
 * le remplacement des syscall
 * Sauvegarde de l'ancien
 * Return: négatif si erreur
 *         0 si ok
 */
static int	kacctime_replace_syscall(void)
{
  unsigned long	cr0;

  kacctime_log_level(0, "kacctime_replace_syscall\n");

  cr0 = read_cr0();
  write_cr0(cr0 & ~X86_CR0_WP);

  if (fct_state_nanosleep == FCT_ACTIF)
    {
      original_nanosleep = (void *)kacctime_syscall_table[__NR_nanosleep];
      kacctime_syscall_table[__NR_nanosleep] = (unsigned long) kacctime_nanosleep;

      original_nanosleep_32 =
	(void *)kacctime_syscall_table_32[__NR_nanosleep_32];
      kacctime_syscall_table_32[__NR_nanosleep_32] =
	(unsigned long) kacctime_nanosleep_32;

      kacctime_log_level(0, "takeover : nanosleep\n");
    }

  original_clock_gettime =
	  (void *)kacctime_syscall_table[__NR_clock_gettime];
  if (fct_state_clock_gettime == FCT_ACTIF)
    {
      kacctime_syscall_table[__NR_clock_gettime] =
	(unsigned long) kacctime_clock_gettime;

      original_clock_gettime_32 =
	(void *)kacctime_syscall_table_32[__NR_clock_gettime_32];
      kacctime_syscall_table_32[__NR_clock_gettime_32] =
	(unsigned long) kacctime_clock_gettime_32;

      kacctime_log_level(0, "takeover : clock_gettime\n");
    }
  
  if (fct_state_time == FCT_ACTIF)
    {
      original_time = (void *)kacctime_syscall_table[__NR_time];
      kacctime_syscall_table[__NR_time] = (unsigned long) kacctime_time;

      original_time_32 = (void *)kacctime_syscall_table_32[__NR_time_32];
      kacctime_syscall_table_32[__NR_time_32] = (unsigned long) kacctime_time_32;

      kacctime_log_level(0, "takeover : time\n");
    }

  if (fct_state_gettimeofday == FCT_ACTIF)
    {
      original_gettimeofday = (void *)kacctime_syscall_table[__NR_gettimeofday];
      kacctime_syscall_table[__NR_gettimeofday] =
	(unsigned long) kacctime_gettimeofday;

      original_gettimeofday_32 =
	(void *)kacctime_syscall_table_32[__NR_gettimeofday_32];
      kacctime_syscall_table_32[__NR_gettimeofday_32] =
	(unsigned long) kacctime_gettimeofday_32;

      kacctime_log_level(0, "takeover : gettimeofday\n");
    }

  if (fct_state_setitimer == FCT_ACTIF)
    {
      original_setitimer = (void *)kacctime_syscall_table[__NR_setitimer];
      kacctime_syscall_table[__NR_setitimer] = (unsigned long) kacctime_setitimer;

      original_setitimer_32 =
	(void *)kacctime_syscall_table_32[__NR_setitimer_32];
      kacctime_syscall_table_32[__NR_setitimer_32] =
	(unsigned long) kacctime_setitimer_32;

      kacctime_log_level(0, "takeover : setitimer\n");
    }

  if (fct_state_getitimer == FCT_ACTIF)
    {
      original_getitimer = (void *)kacctime_syscall_table[__NR_getitimer];
      kacctime_syscall_table[__NR_getitimer] = (unsigned long) kacctime_getitimer;

      original_getitimer_32 =
	(void *)kacctime_syscall_table_32[__NR_getitimer_32];
      kacctime_syscall_table_32[__NR_getitimer_32] =
	(unsigned long) kacctime_getitimer_32;

      kacctime_log_level(0, "takeover : getitimer\n");
    }

  if (fct_state_alarm == FCT_ACTIF)
    {
      original_alarm = (void *)kacctime_syscall_table[__NR_alarm];
      kacctime_syscall_table[__NR_alarm] = (unsigned long) kacctime_alarm;

      original_alarm_32 = (void *)kacctime_syscall_table_32[__NR_alarm_32];
      kacctime_syscall_table_32[__NR_alarm_32] = (unsigned long) kacctime_alarm_32;
      
      kacctime_log_level(0, "takeover : alarm\n");
    }
  
  if (fct_state_select == FCT_ACTIF)
    {
      original_select = (void *)kacctime_syscall_table[__NR_select];
      kacctime_syscall_table[__NR_select] = (unsigned long) kacctime_select;

      original_select_32 = (void *)kacctime_syscall_table_32[__NR_select_32];
      kacctime_syscall_table_32[__NR_select_32] = (unsigned long) kacctime_select_32;
      
      kacctime_log_level(0, "takeover : select\n");
    }

  if (fct_state_poll == FCT_ACTIF)
    {
      original_poll = (void *)kacctime_syscall_table[__NR_poll];
      kacctime_syscall_table[__NR_poll] = (unsigned long) kacctime_poll;

      original_poll_32 = (void *)kacctime_syscall_table_32[__NR_poll_32];
      kacctime_syscall_table_32[__NR_poll_32] = (unsigned long) kacctime_poll_32;
      
      kacctime_log_level(0, "takeover : poll\n");
    }

  if (fct_state_epoll == FCT_ACTIF)
    {
      original_epoll_wait = (void *)kacctime_syscall_table[__NR_epoll_wait];
      kacctime_syscall_table[__NR_epoll_wait] =
	      (unsigned long) kacctime_epoll_wait;
      original_epoll_wait_32 =
	      (void *)kacctime_syscall_table_32[__NR_epoll_wait_32];
      kacctime_syscall_table_32[__NR_epoll_wait_32] = (unsigned long) kacctime_epoll_wait_32;
      kacctime_log_level(0, "takeover : epoll\n");
    }

  if (fct_state_futex == FCT_ACTIF)
    {
      original_futex = (void *)kacctime_syscall_table[__NR_futex];
      kacctime_syscall_table[__NR_futex] = (unsigned long) kacctime_futex;
      original_futex_32 = (void *)kacctime_syscall_table_32[__NR_futex_32];
      kacctime_syscall_table_32[__NR_futex_32] = (unsigned long) kacctime_futex_32;
      kacctime_log_level(0, "takeover : futex\n");
    }

  if (fct_state_semtimedop == FCT_ACTIF)
    {
      original_semtimedop = (void *)kacctime_syscall_table[__NR_semtimedop];
      kacctime_syscall_table[__NR_semtimedop] = (unsigned long) kacctime_semtimedop;
      kacctime_log_level(0, "takeover : semtimedop\n");
    }
  
  write_cr0(cr0);
  return (0);
}

/**
 * kacctime_proclst_read - affiche la config des espace
 *
 * fichier de /proc qui contient les espaces, et leur paramétres :
 * n:facteur:référence:décalage
 * n est le numéro de l'espace
 * facteur est le facteur d'accélération
 * référence est la date de début de l'accélération
 * décalage est le décalage par rapport à la date
 * Par espace, on a aussi la liste de nom de processus. Le module accélere
 * tous les processus dont le nom (sur 15 caractères) se trouve dans cette
 * liste
 */
static int	proclst_rest_read = 1;
static ssize_t	kacctime_proclst_read(struct file *file, char *buf,
				      size_t count, loff_t *offs)
{
  char		tmp_buf[KACCTIME_BUF_SIZE];
  int		i;
  int		retval;
  int		rt;

  i = 0;
  rt = 0;
  memset(tmp_buf, 0, KACCTIME_BUF_SIZE);
  while (i < KACCTIME_MAX_SPACE)
    {
      int	line_len;
      char	space_str[KACCTIME_MAX_LEN];

      line_len = snprintf(space_str, KACCTIME_MAX_LEN-1, "%d:%d:%d:%ld:%ld\n", i,
			  kacctime_timespace_tab[i].multi,
			  kacctime_timespace_tab[i].divid,
			  kacctime_timespace_tab[i].ref.tv_sec,
			  kacctime_timespace_tab[i].dec.tv_sec);
      strncat(tmp_buf, space_str, line_len);
      rt += line_len;
      i++;
    }

  i = 0;
  while (i < KACCTIME_MAX_SPACE)
    {
      int	i_prog = 0;
      char	progname_str[KACCTIME_MAX_LEN];
      int	len = 0;
      
      if (kacctime_timespace_tab[i].multi != 0)
	{
	  len = snprintf(progname_str, KACCTIME_MAX_LEN-1,
			 "%d", i);
	  while (i_prog < kacctime_progname_list[i].len)
	    {
	      len += snprintf(progname_str, KACCTIME_MAX_LEN-1-len,
			      "%s:%s", progname_str,
			      kacctime_progname_list[i].tab[i_prog]);
	      i_prog++;
	    }
	  len += snprintf(progname_str, KACCTIME_MAX_LEN-1-len, "%s\n",
			  progname_str);
	  strncat(tmp_buf, progname_str, len);
	  rt += len;
	}
      i++;
    }

  retval = copy_to_user(buf, tmp_buf, rt);
  if (retval != 0)
    {
      kacctime_log_level(0, "copy failed!\n");
      return (retval);
    }
  if (proclst_rest_read)
    {
      proclst_rest_read = 0;
      return (rt);
    }
  proclst_rest_read = 1;
  return (0);
}

/**
 * kacctime_procsta_read - affiche les statistiques du module
 */
static int	kacctime_procsta_read(struct seq_file *m, void *v)
{
  int		i = 0;
  struct s_kacctime_stats_comm	*itr;
  int		bkt;
  
  seq_printf(m, "select encours : %d\t\t\tsleep encours : %d\n\
poll encours : %d\t\t\tepoll encours : %d\n",
	     atomic_read(&kacctime_counter_select),
	     atomic_read(&kacctime_counter_sleep),
	     atomic_read(&kacctime_counter_poll),
	     atomic_read(&kacctime_counter_epoll));
  seq_printf(m,
	     "i:\
------time\
------gtod\
------nslp\
------cgti\
------siti\
------giti\
------alar\
----select\
------poll\
-----epoll\
-----futex\
-----semti\
------ti32\
------gd32\
------ns32\
------cg32\
------st32\
------gt32\
------al32\
----sele32\
----poll32\
---epoll32\n");
  while (i < KACCTIME_MAX_SPACE+1)
    {
      seq_printf(m, "% d% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld\
% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld\
% 10ld% 10ld% 10ld% 10ld% 10ld% 10ld\n",
		 i-1,
		 kacctime_stats_space_tab[i].cmpt_time,
		 kacctime_stats_space_tab[i].cmpt_gettimeofday,
		 kacctime_stats_space_tab[i].cmpt_nanosleep,
		 kacctime_stats_space_tab[i].cmpt_clock_gettime,
		 kacctime_stats_space_tab[i].cmpt_setitimer,
		 kacctime_stats_space_tab[i].cmpt_getitimer,
		 kacctime_stats_space_tab[i].cmpt_alarm,
		 kacctime_stats_space_tab[i].cmpt_select,
		 kacctime_stats_space_tab[i].cmpt_poll,
		 kacctime_stats_space_tab[i].cmpt_epoll,
		 kacctime_stats_space_tab[i].cmpt_futex,
		 kacctime_stats_space_tab[i].cmpt_semtimedop,
		 kacctime_stats_space_tab[i].cmpt_time_32,
		 kacctime_stats_space_tab[i].cmpt_gettimeofday_32,
		 kacctime_stats_space_tab[i].cmpt_nanosleep_32,
		 kacctime_stats_space_tab[i].cmpt_clock_gettime_32,
		 kacctime_stats_space_tab[i].cmpt_setitimer_32,
		 kacctime_stats_space_tab[i].cmpt_getitimer_32,
		 kacctime_stats_space_tab[i].cmpt_alarm_32,
		 kacctime_stats_space_tab[i].cmpt_select_32,
		 kacctime_stats_space_tab[i].cmpt_poll_32,
		 kacctime_stats_space_tab[i].cmpt_epoll_32
		 );
      i++;
    }
  
  hash_for_each(kacctime_hstats, bkt, itr, hash_list)
    {
      seq_printf(m,
		 "% d [%15s] (%13s)->% 20d\n",
		 itr->space,
		 itr->name,
		 tab_fct[itr->fct],
		 itr->cpt);
      kacctime_log_level(6, "space:%d ; comm=%s ; fct=%d ; cpt=%d\n",
			 itr->space,
			 itr->name,
			 itr->fct,
			 itr->cpt);
    }  
  return (0);
}

static int	kacctime_procsta_open(struct inode *inode, struct file *file)
{
  return (single_open(file, kacctime_procsta_read, NULL));
}

/**
 * kacctime_procadd_write - permet de configurer un espace
 */
static ssize_t kacctime_procadd_write(struct file *file, const char *buf,
				      size_t count, loff_t *offs)
{
	char copy_buf[KACCTIME_BUF_SIZE];
	char *tmp_buf;
	int space;
	char *seek;
	mm_segment_t oldfs;
	struct timespec	now;
	long retval;
	
	if (copy_from_user(copy_buf, buf, count)) {
		kacctime_log_level(0, "user copy !\n");
		return (-EFAULT);
	}
	
	copy_buf[count] = CHAR_END_STR;
	tmp_buf = copy_buf;
	
	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtoint(seek, 10, &space) != 0)
		goto out_bad_space;
	if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
		kacctime_log_level(1,"space out of MAX_SPACE !\n");
		return (-1);
	}
	
	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtoint(seek, 10, &(kacctime_timespace_tab[space].multi)) != 0)
		goto out_bad_space;
	if (kacctime_timespace_tab[space].multi < 0) {
		kacctime_log_level(0, "multi negatif !\n");
		return (-1);
	}

	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtoint(seek, 10, &(kacctime_timespace_tab[space].divid)) != 0)
		goto out_bad_space;
	
	if (kacctime_timespace_tab[space].divid <= 0) {
		kacctime_log_level(0, "divid negatif/Zero !\n");
		return (-1);
	}
		
	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtol(seek, 10, &(kacctime_timespace_tab[space].ref.tv_sec))
	    != 0) {
		kacctime_log_level(0, "bad ref format !\n");
		return (-1);
	}

	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtol(seek, 10, &(kacctime_timespace_tab[space].dec.tv_sec))
	    != 0) {
		kacctime_log_level(0, "bad dec number !\n");
		return (-1);
	}
	
	oldfs = get_fs();
	set_fs (KERNEL_DS);
	retval = (*original_clock_gettime)(CLOCK_MONOTONIC, &now);
	set_fs(oldfs);
	
#warning "À refaire"
	kacctime_timespace_tab[space].mono_delta.tv_sec =
		kacctime_timespace_tab[space].dec.tv_sec +
		kacctime_timespace_tab[space].ref.tv_sec - now.tv_sec - 1;
	kacctime_timespace_tab[space].mono_delta.tv_nsec = 1000000000L -
		now.tv_nsec;
	
	if (kacctime_timespace_tab[space].mono_delta.tv_sec < 1)
		kacctime_timespace_tab[space].mono_delta.tv_sec = 1;
	
	kacctime_log_level(0, "add space %d -> [%d/%d ; %ld ; %ld ; (%ld, %ld)]\n",
			   space,
			   kacctime_timespace_tab[space].multi,
			   kacctime_timespace_tab[space].divid,
			   kacctime_timespace_tab[space].ref.tv_sec,
			   kacctime_timespace_tab[space].dec.tv_sec,
			   kacctime_timespace_tab[space].mono_delta.tv_sec,
			   kacctime_timespace_tab[space].mono_delta.tv_nsec);
	
	return (count);
	
 out_bad_format:
	kacctime_log_level(0, "bad space number !\n");
	return (-1);
	
 out_bad_space:
	kacctime_log_level(0, "bad space format ! \n");
	return (-1);
}

/**
 * kacctime_procadd_prgnam_write - permet d'ajouter un nom à la liste
 * des processus accélerés sans la variable d'environnement 
 */
static ssize_t		kacctime_procadd_prgnam_write(struct file *file,
						      const char *buf,
						      size_t count, loff_t *offs)
{
  char			tmp_buf[KACCTIME_BUF_SIZE];
  char			*space_s;
  int			 space;
  char			*seek;
  char			*cmd;

  if (copy_from_user(tmp_buf, buf, count))
    {
      kacctime_log_level(0, "user copy !\n");
      return (-EFAULT);
    }
  tmp_buf[count] = CHAR_END_STR;
  space_s = tmp_buf;
  seek = strchr(tmp_buf, KACCTIME_CODE_SEP);
  if (seek)
    {
      *seek = CHAR_END_STR;
      if (kstrtoint(space_s, 10, &space))
	{
	  kacctime_log_level(0, "bad space number !\n");
	  return (-1);
	}
    }
  else
    {
      kacctime_log_level(0, "bad space format ! \n");
      return (-1);
    }
  if ((space < 0) || (space >= KACCTIME_MAX_SPACE))
    {
      kacctime_log_level(0,"space out of MAX_SPACE !\n");
      return (-1);
    }      

  cmd = seek + 1;
  seek = strchr(cmd, CHAR_NEW_LINE);
  if (seek)
    {
      *seek = CHAR_END_STR;
    }
  if (kacctime_progname_list[space].len == KACCTIME_PROGNAME_LIST_SIZE)
    {
      kacctime_log_level(0, "ERR : [%d:%s] -> tab full\n", space, cmd);
      return (count);
    }  
  strncpy(kacctime_progname_list[space].tab[kacctime_progname_list[space].len],
	  cmd, TASK_COMM_LEN-1);
  kacctime_log_level(0, "[%d:(%d):%s]\n", space,
		     kacctime_progname_list[space].len,
		     kacctime_progname_list[space].tab[kacctime_progname_list[space].len]);
  kacctime_progname_list[space].len++;
  return (count);
}

/**
 * kacctime_procupd_write - modifie le facteur d'un espace
 */
static ssize_t kacctime_procupd_write(struct file *file, const char *buf,
				      size_t count, loff_t *offs)
{
	char copy_buf[KACCTIME_BUF_SIZE];
	char *tmp_buf;
	int space;
	char *seek;
	int retval;
	struct timeval now;
	int old_multi;
	int old_divid;
	struct timespec old_dec;
	struct timespec old_ref;
	mm_segment_t oldfs;
  
	if (copy_from_user(copy_buf, buf, count)) {
		kacctime_log_level(0, "user copy !\n");
		return (-EFAULT);
	}
	copy_buf[count] = CHAR_END_STR;
	tmp_buf = copy_buf;

	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_space;
	if (kstrtoint(seek, 10, &space) != 0)
		goto out_bad_space;	
	if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
		kacctime_log_level(0, "space out of MAX_SPACE !\n");
		return (-1);
	}
	
	old_multi = kacctime_timespace_tab[space].multi;
	old_divid = kacctime_timespace_tab[space].divid;
	old_ref.tv_sec = kacctime_timespace_tab[space].ref.tv_sec;
	old_ref.tv_nsec = kacctime_timespace_tab[space].ref.tv_nsec;
	old_dec.tv_sec = kacctime_timespace_tab[space].dec.tv_sec;
	old_dec.tv_nsec = kacctime_timespace_tab[space].dec.tv_nsec;
	
	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtoint(seek, 10, &(kacctime_timespace_tab[space].multi)) != 0)
		goto out_bad_space;
	if (kacctime_timespace_tab[space].multi < 0) {
		kacctime_log_level(0, "multi negatif !\n");
		return (-1);
	}

	seek = strsep(&tmp_buf, KACCTIME_CODE_SEP_STR);
	if (seek == NULL)
		goto out_bad_format;
	if (kstrtoint(seek, 10, &(kacctime_timespace_tab[space].divid)) != 0)
		goto out_bad_space;
	
	if (kacctime_timespace_tab[space].divid <= 0) {
		kacctime_log_level(0, "divid negatif/Zero !\n");
		return (-1);
	}

	kacctime_log_level(0, "upd space %d :: %d/%d -> %d/%d\n", space,
			   old_multi, old_divid,
			   kacctime_timespace_tab[space].multi,
			   kacctime_timespace_tab[space].divid);
	
	oldfs = get_fs();
	set_fs (KERNEL_DS);
	retval = (*original_gettimeofday)(&now, 0);
	set_fs(oldfs);      
	
	if (retval == -1) {
		kacctime_log_level(0, "bad time !\n");
		return (-1);
	}

#warning "à vérifier & améliorer"
	kacctime_timespace_tab[space].ref.tv_sec = now.tv_sec;
	kacctime_timespace_tab[space].dec.tv_sec = old_ref.tv_sec +
		old_dec.tv_sec +
		(now.tv_sec-old_ref.tv_sec) * (old_multi/old_divid) -
		now.tv_sec + (now.tv_usec/1000 * (old_multi/old_divid)) / 1000;
	
	return (count);

 out_bad_format:
	kacctime_log_level(0, "bad space number !\n");
	return (-1);

 out_bad_space:
	kacctime_log_level(0, "bad space format ! \n");
	return (-1);
}

/**
 * kacctime_procdel_write - supprime un espace
 */
static ssize_t kacctime_procdel_write(struct file *file, const char *buf,
				      size_t count, loff_t *offs)
{
	char tmp_buf[KACCTIME_BUF_SIZE];
	int space;

	if (copy_from_user(tmp_buf, buf, count)) {
		kacctime_log_level(0, "user copy !\n");
		return (-EFAULT);
	}
	tmp_buf[count] = CHAR_END_STR;
	if (kstrtoint(tmp_buf, 10, &space)) {
		kacctime_log_level(0, "bad space number !\n");
		return (-1);
	}
	if ((space < 0) || (space >= KACCTIME_MAX_SPACE)) {
		kacctime_log_level(0, "space out of MAX_SPACE !\n");
		return (-1);
	}
	kacctime_timespace_tab[space].multi = 0;
	kacctime_timespace_tab[space].divid = 1;
	kacctime_timespace_tab[space].ref.tv_sec = 0;
	kacctime_timespace_tab[space].ref.tv_nsec = 0;
	kacctime_timespace_tab[space].dec.tv_sec = 0;
	kacctime_timespace_tab[space].dec.tv_nsec = 0;
	kacctime_log_level(5, "del space %d\n", space);
	return (count);
}

/**
 * kacctime_procdel_prgnam_write - supprime un nom de la liste des processus 
 * accélérés sans variable
 */
static ssize_t	kacctime_procdel_prgnam_write(struct file *file,
					      const char *buf,
					      size_t count, loff_t *offs)
{
  char			tmp_buf[KACCTIME_BUF_SIZE];
  char			*space_s;
  int			 space;
  char			*seek;
  char			*cmd;
  int			i_prog;

  if (copy_from_user(tmp_buf, buf, count))
    {
      kacctime_log_level(0, "user copy !\n");
      return (-EFAULT);
    }
  tmp_buf[count] = CHAR_END_STR;
  space_s = tmp_buf;
  seek = strchr(tmp_buf, KACCTIME_CODE_SEP);
  if (seek)
    {
      *seek = CHAR_END_STR;
      if (kstrtoint(space_s, 10, &space))
	{
	  kacctime_log_level(0, "bad space number !\n");
	  return (-1);
	}
    }
  else
    {
      kacctime_log_level(0, "bad space format ! \n");
      return (-1);
    }
  if ((space < 0) || (space >= KACCTIME_MAX_SPACE))
    {
      kacctime_log_level(0,"space out of MAX_SPACE !\n");
      return (-1);
    }      

  cmd = seek + 1;
  seek = strchr(cmd, CHAR_NEW_LINE);
  if (seek)
    {
      *seek = CHAR_END_STR;
    }

  kacctime_log_level(0, "DEL : [%d:%s]\n", space, cmd);
  i_prog = 0;
  while (i_prog < kacctime_progname_list[space].len)
    {
      if (!strcmp(kacctime_progname_list[space].tab[i_prog], cmd))
	{
	  kacctime_log_level(0, "DEL trouvé : %d, %d; %s\n", space, i_prog, cmd);
	  if (kacctime_progname_list[space].len == 1)
	    {
	      memset(kacctime_progname_list[space].tab[i_prog], 0,
		     TASK_COMM_LEN);
	      kacctime_progname_list[space].len = 0;
	    }
	  else
	    {
	      strcpy(kacctime_progname_list[space].tab[i_prog],
		     kacctime_progname_list[space].tab[kacctime_progname_list[space].len-1]);
	      kacctime_progname_list[space].len--;
	    }
	  break;
	}
      i_prog++;
    }
  return (count);
}

/**
 * kacctime_proc_set_callback - position les callback pour /proc/kAccTime
 */
static int	kacctime_proc_set_callback(void)
{

  kacctime_procadd_fops.write = kacctime_procadd_write;
  kacctime_procadd = proc_create(KACCTIME_PROCADD, 0666, kacctime_procdir,
				 &kacctime_procadd_fops);
  if (kacctime_procadd == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCADD);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCADD);

  kacctime_procadd_prgnam_fops.write = kacctime_procadd_prgnam_write;
  kacctime_procadd_prgnam = proc_create(KACCTIME_PROCADD_PRGNAM, 0666,
					kacctime_procdir,
					&kacctime_procadd_prgnam_fops);
  if (kacctime_procadd_prgnam == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCADD_PRGNAM);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCADD_PRGNAM);
  
  kacctime_procupd_fops.write = kacctime_procupd_write;
  kacctime_procupd = proc_create(KACCTIME_PROCUPD, 0666, kacctime_procdir,
			     &kacctime_procupd_fops);
  if (kacctime_procupd == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCUPD);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCUPD);
  
  kacctime_procdel_fops.write = kacctime_procdel_write;
  kacctime_procdel = proc_create(KACCTIME_PROCDEL, 0666, kacctime_procdir,
			     &kacctime_procdel_fops);
  if (kacctime_procdel == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCDEL);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCDEL);

  kacctime_procdel_prgnam_fops.write = kacctime_procdel_prgnam_write;
  kacctime_procdel_prgnam = proc_create(KACCTIME_PROCDEL_PRGNAM, 0666,
					kacctime_procdir,
					&kacctime_procdel_prgnam_fops);
  if (kacctime_procdel_prgnam == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCDEL_PRGNAM);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCDEL_PRGNAM);

  kacctime_proclst_fops.read = kacctime_proclst_read;
  kacctime_proclst = proc_create(KACCTIME_PROCLST, 0666, kacctime_procdir,
			     &kacctime_proclst_fops);
  if (kacctime_proclst == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCLST);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCLST);
  
  kacctime_procsta_fops.open = kacctime_procsta_open;
  kacctime_procsta_fops.llseek = seq_lseek;
  kacctime_procsta_fops.read = seq_read;
  kacctime_procsta_fops.release = single_release;
  kacctime_procsta = proc_create(KACCTIME_PROCSTA, 0666, kacctime_procdir,
			     &kacctime_procsta_fops);
  if (kacctime_procsta == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCSTA);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		       KACCTIME_PROCSTA);
return (0);
}

/**
 * kacctime_init_stats - initialise la table de hash
 */
static int	kacctime_init_stats(void)
{
  hash_init(kacctime_hstats);
  return (0);
}

/**
 * kacctime_poach_syscall_table - trouve et surcharge les syscalls
 */
static int kacctime_poach_syscall_table(void)
{
	kacctime_syscall_table = (unsigned long *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_level(0, "sym addr : [%pK]\n", kacctime_syscall_table);
	kacctime_syscall_table_32 = (unsigned long *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM32);
	kacctime_log_level(0, "sym addr32 : [%pK]\n", kacctime_syscall_table_32);

	if (kacctime_replace_syscall() == -1) {
		kacctime_log_level(0, "remplacement syscall raté !\n");
		return (-1);
	}
	if (kacctime_proc_set_callback() == -1) {
		kacctime_log_level(0, "callback /proc raté !\n");
		return (-1);
	}
	if (kacctime_init_stats() == -1) {
		kacctime_log_level(0, "init des stats raté !\n");
		return (-1);
	}
	return (0);
}

/*
static int	kacctime_poach_syscall_table_obsol(void)
{
  char		kern_path_file[KACCTIME_MAX_LEN];

  kacctime_get_version(kern_path_file);
  kacctime_log_level(0, "kern path : [%s]\n", kern_path_file);
  if (kacctime_get_syscall_table(kern_path_file) == -1)
    {
      kacctime_log_level(0, "recherche syscall raté !\n");
      return (-1);
    }
  if (kacctime_replace_syscall() == -1)
    {
      kacctime_log_level(0, "remplacement syscall raté !\n");
      return (-1);
    }
  if (kacctime_proc_set_callback() == -1)
    {
      kacctime_log_level(0, "callback /proc raté !\n");
      return (-1);
    }
  if (kacctime_init_stats() == -1)
    {
      kacctime_log_level(0, "init des stats raté !\n");
      return (-1);
    }
  return (0);
}
*/

/**
 * takeover syscall
 * NOTE : il faudrait changer les returns pour être cohérent
 */
static int	kacctime_init_takeover_syscall(void)
{
  int		retval;
  
  kacctime_log_level(0, "init takerover\n");
  retval = kacctime_poach_syscall_table();
  if (retval == -1)
    {
      kacctime_log_level(0, "syscall takeover raté !\n");
      return (0);
    }
  return (1);
}

/**
 * kacctime_procprm_read - affiche les paramétres du module
 */
static int	kacctime_procprm_read(struct seq_file *m, void *v)
{
  seq_printf(m, "kacctime_nb_space (ro) = %d\n", KACCTIME_MAX_SPACE);
  
  seq_printf(m, "kacctime_log_mode = %ld\n", kacctime_log_mode);

  seq_printf(m, "fct_state_time = %d\n", fct_state_time);
  seq_printf(m, "fct_state_gettimeofday = %d\n", fct_state_gettimeofday);
  seq_printf(m, "fct_state_nanosleep = %d\n", fct_state_nanosleep);
  seq_printf(m, "fct_state_clock_gettime = %d\n", fct_state_clock_gettime);
  seq_printf(m, "fct_state_setitimer = %d\n", fct_state_setitimer);
  seq_printf(m, "fct_state_getitimer = %d\n", fct_state_getitimer);
  seq_printf(m, "fct_state_alarm = %d\n", fct_state_alarm);
  seq_printf(m, "fct_state_select = %d\n", fct_state_select);
  seq_printf(m, "fct_state_poll = %d\n", fct_state_poll);
  seq_printf(m, "fct_state_epoll = %d\n", fct_state_epoll);
  seq_printf(m, "fct_state_futex = %d\n", fct_state_futex);
  seq_printf(m, "fct_state_semtimedop = %d\n", fct_state_semtimedop);

  seq_printf(m, "kacctime_sleep_frac_interval = %d\n",
	     kacctime_sleep_frac_interval);
  seq_printf(m, "kacctime_sleep_frac_cycle = %d\n",
	     kacctime_sleep_frac_cycle);

  return (0);
}

static int	kacctime_procprm_open(struct inode *inode, struct file *file)
{
  return (single_open(file, kacctime_procprm_read, NULL));
}

/**
 * kacctime_procprm_write - modifie les paramétres du module
 */
static ssize_t	kacctime_procprm_write(struct file *file, const char *buf,
				       size_t count, loff_t *offs)
{
  char		delim[] = " =";
  char		*token;
  char		key[KACCTIME_MAX_LEN];
  char		val[KACCTIME_MAX_LEN];
  char		tmp_buf[KACCTIME_MAX_LEN];
  char		*b;
  int		err;
  
  memset(tmp_buf, 0, KACCTIME_MAX_LEN);
  if (copy_from_user(tmp_buf, buf, count))
    {
      kacctime_log_level(0, "user copy !\n");
      return (-EFAULT);
    }
  if (tmp_buf[count-1] == '\n')
    tmp_buf[count-1] = '\0';
  b = tmp_buf;
  token = strsep(&b, delim);
  strcpy(key, token);
  while ((token = strsep(&b, delim)))
    {
      if (strcmp(token, "") != 0)
	strcpy(val, token);
      else
	strcpy(val, "ARRG");	
    }

  if (strcmp(key, "kacctime_log_mode") == 0)
    {
      int	i_val = 0;

      kacctime_log_level(0, "key=(%s),val=(%s)\n", key, val);

      err = kstrtoint(val, 10, &i_val);
      if (err < 0)
	goto val_err;
      kacctime_log_mode = i_val;
      goto out;
    }

  if (strcmp(key, "kacctime_sleep_frac_interval") == 0)
    {
      int	i_val = 0;

      kacctime_log_level(0, "key=(%s),val=(%s)\n", key, val);

      err = kstrtoint(val, 10, &i_val);
      if (err < 0)
	goto val_err;
      kacctime_sleep_frac_interval = i_val;
      goto out;
    }

  if (strcmp(key, "kacctime_sleep_frac_cycle") == 0)
    {
      int	i_val = 0;

      kacctime_log_level(0, "key=(%s),val=(%s)\n", key, val);

      err = kstrtoint(val, 10, &i_val);
      if (err < 0)
	goto val_err;
      kacctime_sleep_frac_cycle = i_val;
      goto out;
    }

  kacctime_log_level(0, "NON TRAITEE->key=(%s),val=(%s)\n", key, val);

 out:
  return (count);
  
 val_err:
  kacctime_log_level(0, "kstroint err !!\n");
  return (err);
}

/**
 * kacctime_procctl_read
 */
static int	kacctime_temp_ctl_read = KACCTIME_MAX_LEN;
static ssize_t	kacctime_procctl_read(struct file *file, char *buf,
				      size_t count, loff_t *offs)
{
  char		msg[KACCTIME_MAX_LEN];
  int		retval;

  kacctime_log_level(0, "init OK)\n");
  if (count > kacctime_temp_ctl_read)
    count = kacctime_temp_ctl_read;
  kacctime_temp_ctl_read -= count;
  snprintf(msg, KACCTIME_MAX_LEN, "%s [%d]\n", KACCTIME_CUSTOMER,
	   kacctime_initstate);
  retval = copy_to_user(buf, msg, strlen(msg));
  if (retval != 0)
    {
      kacctime_log_level(0, "copy failed!\n");
      return (-EFAULT);
    }
  kacctime_log_level(1, "CTL READ (%zu)/ count=%zu\n", strlen(msg), count);
  if (count == 0)
    kacctime_temp_ctl_read = strlen(msg);
  return (count);
}

/**
 *
 */
int		syracuse(long int premier, char *code)
{
  long int	nb_iter = 10;
  int		cur_iter = 1;

  while (cur_iter < nb_iter)
    {
      if (!(premier % 2))
	{
	  premier = premier / 2;
	}
      else
	{
	  premier = 3 * premier + 1;
	}
      sprintf(code, "%s%ld", code, premier);
      cur_iter++;
    }
  return (0);
}

/**
 *
 */
static ssize_t		kacctime_procctl_write(struct file *file,
					       const char *buf,
					       size_t count,
					       loff_t *offs)
{
  char			kacctime_tmp_buf[KACCTIME_MAX_LEN];
  char			*customer;
  char			*challenge_s;
  char			*idxsep;
  struct timeval	now;
  int			test_code = 1;
  int			test_cust = 1;
  char			code[KACCTIME_MAX_LEN];
 
  kacctime_log_level(1, "ctl write\n");
  if (kacctime_initstate)
    return (count);
  memset(kacctime_tmp_buf, 0, KACCTIME_MAX_LEN);
  if (copy_from_user(kacctime_tmp_buf, buf, count))
    return (-EFAULT);
  kacctime_tmp_buf[count] = 0;
  customer = kacctime_tmp_buf;
  idxsep = strchr(kacctime_tmp_buf, KACCTIME_CODE_SEP);
  if (idxsep == NULL)
    return (-1);
  challenge_s = idxsep+1;
  *idxsep = 0;
  do_gettimeofday(&now);
  memset(code, 0, KACCTIME_MAX_LEN);
  syracuse(now.tv_sec/KACCTIME_CODE_FREQ, code);
  test_cust = strcmp(customer, KACCTIME_CUSTOMER);
  test_code = strcmp(code, challenge_s);
  kacctime_log_level(1, "ctl : [%d::%d] \n", test_code, test_cust);
  if (!test_cust && !test_code)
    kacctime_initstate = kacctime_init_takeover_syscall();
  else
    kacctime_log_level(0, "acces denied -- please contact ITS\n");  
  return (count);
}

/**
 * kacctime_param_check
 */
static void	kacctime_param_check(void)
{
  if (strcmp(kacctime_fct_state, FCT_STATE_FLAG_ALL) == 0)
    {
      kacctime_log_level(0, "fct state : [%s] -> activation totale\n",
			 kacctime_fct_state);
      fct_state_time	      = FCT_ACTIF;
      fct_state_gettimeofday  = FCT_ACTIF;
      fct_state_nanosleep     = FCT_ACTIF;
      fct_state_clock_gettime = FCT_ACTIF;
      fct_state_setitimer     = FCT_ACTIF;
      fct_state_getitimer     = FCT_ACTIF;
      fct_state_alarm	      = FCT_ACTIF;
      fct_state_select	      = FCT_ACTIF;
      fct_state_poll	      = FCT_ACTIF;
      fct_state_epoll	      = FCT_ACTIF;
      fct_state_futex	      = FCT_ACTIF;
      fct_state_semtimedop    = FCT_ACTIF;
    }
  if (strcmp(kacctime_fct_state, FCT_STATE_FLAG_TIME) == 0)
    {
      kacctime_log_level(0, "fct state : [%s] -> activation : \
time|gettimeofday|nanosleep|clock_gettime|setitimer|getitimer|alarm\n",
			 kacctime_fct_state);
      fct_state_time	      = FCT_ACTIF;
      fct_state_gettimeofday  = FCT_ACTIF;
      fct_state_nanosleep     = FCT_ACTIF;
      fct_state_clock_gettime = FCT_ACTIF;
      fct_state_setitimer     = FCT_ACTIF;
      fct_state_getitimer     = FCT_ACTIF;
      fct_state_alarm	      = FCT_ACTIF;
      fct_state_select	      = FCT_INACTIF;
      fct_state_poll	      = FCT_INACTIF;
      fct_state_epoll	      = FCT_INACTIF;
      fct_state_futex	      = FCT_INACTIF;
      fct_state_semtimedop    = FCT_INACTIF;
    }

  kacctime_log_level(0, "kacctime_log_mode=%d\n", kacctime_log_mode);  
  return;
}

/**
 * kacctime_init_control_module
 */
static int	kacctime_init_control_module(void)
{
  int		retval = 0;
  int		idx = 0;
  
  kacctime_log_level(0, "init control\n");
  kacctime_log_level(0, "max timespace : %d\n", KACCTIME_MAX_SPACE);

  kacctime_param_check();

  memset(kacctime_timespace_tab, 0, sizeof(kacctime_timespace_tab));
  while (idx < KACCTIME_MAX_SPACE) {
	  kacctime_timespace_tab[idx].divid = 1;
	  idx++;
  }
  
  memset(kacctime_stats_space_tab, 0, sizeof(kacctime_stats_space_tab));

  memset(kacctime_progname_list, 0, KACCTIME_MAX_SPACE *
	 sizeof(struct s_kacctime_prgnam_list));

  kacctime_log_level(0, "prgname list size : %d\n",
		     KACCTIME_MAX_SPACE *
		     sizeof(struct s_kacctime_prgnam_list));

  kacctime_procdir = proc_mkdir(KACCTIME_PROCDIR, NULL);
  if (kacctime_procdir == NULL)
    {
      kacctime_log_level(0, "ERR création de /proc/%s\n", KACCTIME_PROCDIR);
      retval = -ENOMEM;
    }
  kacctime_log_level(0, "création de /proc/%s\n", KACCTIME_PROCDIR);

  kacctime_procctl_fops.write = kacctime_procctl_write;
  kacctime_procctl_fops.read = kacctime_procctl_read;
  kacctime_procctl = proc_create(KACCTIME_PROCCTL, 0666, kacctime_procdir,
			     &kacctime_procctl_fops);
  if (kacctime_procctl == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCCTL);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCCTL);

  kacctime_procprm_fops.open = kacctime_procprm_open;
  kacctime_procprm_fops.llseek = seq_lseek;
  kacctime_procprm_fops.read = seq_read;
  kacctime_procprm_fops.release = single_release;
  kacctime_procprm_fops.write = kacctime_procprm_write;
  kacctime_procprm = proc_create(KACCTIME_PROCPRM, 0666, kacctime_procdir,
			     &kacctime_procprm_fops);
  if (kacctime_procprm == NULL)
    {
      kacctime_log_level(0, "ERR création do /proc/%s/%s\n",
			 KACCTIME_PROCDIR, KACCTIME_PROCPRM);
      return (-ENOMEM);
    }
  kacctime_log_level(0, "création de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCPRM);

  if (KACCTIME_NO_SECU)
    kacctime_initstate = kacctime_init_takeover_syscall();
  return (retval);
}

static int	__init kacctime_init(void)
{
  int		retval;
  
  kacctime_log_level(0, "init : %ld\n", kacctime_log_mode);
  kacctime_log_level(0, "stats : %ld\n", kacctime_stats_flag);
  retval = kacctime_init_control_module();
  if (retval == -ENOMEM)
    return (-ENOMEM);
  return (0);
}

/**
 * kacctime_get_back_orig_syscall - repositionne les syscalls originaux
 *
 * Cette fonction attend la fin de tous les "sleep" pour sortir afin
 * d'éviter le crash du kernel.
 */
static void	kacctime_get_back_orig_syscall(void)
{
  unsigned long cr0;

  if (!kacctime_initstate)
    return;

  cr0 = read_cr0();
  write_cr0(cr0 & ~X86_CR0_WP);

  if (fct_state_nanosleep == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_nanosleep] =
		  (unsigned long)original_nanosleep;
	  kacctime_syscall_table_32[__NR_nanosleep_32] =
		  (unsigned long)original_nanosleep_32;
	  kacctime_log_level(0, "getback : nanosleep\n");
  }
  
  if (fct_state_clock_gettime == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_clock_gettime] =
		  (unsigned long)original_clock_gettime;
	  kacctime_syscall_table_32[__NR_clock_gettime_32] =
		  (unsigned long)original_clock_gettime_32;
	  kacctime_log_level(0, "getback : clock_gettime\n");
  }
  
  if (fct_state_time == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_time] = (unsigned long)original_time;
	  kacctime_syscall_table_32[__NR_time_32] =
		  (unsigned long)original_time_32;
	  kacctime_log_level(0, "getback : time\n");
  }
  
  if (fct_state_gettimeofday == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_gettimeofday] =
		  (unsigned long)original_gettimeofday;
	  kacctime_syscall_table_32[__NR_gettimeofday_32] =
		  (unsigned long)original_gettimeofday_32;
	  kacctime_log_level(0, "getback : gettimeofday\n");
  }

  if (fct_state_setitimer == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_setitimer] =
		  (unsigned long)original_setitimer;
	  kacctime_syscall_table_32[__NR_setitimer_32] =
		  (unsigned long)original_setitimer_32;
	  kacctime_log_level(0, "getback : setitimer\n");
  }
  
  if (fct_state_getitimer == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_getitimer] =
		  (unsigned long)original_getitimer;
	  kacctime_syscall_table_32[__NR_getitimer_32] =
		  (unsigned long)original_getitimer_32;
	  kacctime_log_level(0, "getback : getitimer\n");
  }
  
  if (fct_state_alarm == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_alarm] = (unsigned long)original_alarm;
	  kacctime_syscall_table_32[__NR_alarm_32] =
		  (unsigned long)original_alarm_32;
	  kacctime_log_level(0, "getback : alarm\n");
  }

  if (fct_state_select == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_select] = (unsigned long)original_select;
	  kacctime_syscall_table_32[__NR_select_32] =
		  (unsigned long)original_select_32;
	  kacctime_log_level(0, "getback : select\n");
  }
  
  if (fct_state_poll == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_poll] = (unsigned long)original_poll;
	  kacctime_syscall_table_32[__NR_poll_32] =
		  (unsigned long)original_poll_32;
	  kacctime_log_level(0, "getback : poll\n");      
  }
  
  if (fct_state_epoll == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_epoll_wait] =
		  (unsigned long)original_epoll_wait;
	  kacctime_syscall_table_32[__NR_epoll_wait_32] =
		  (unsigned long)original_epoll_wait_32;
	  kacctime_log_level(0, "getback : epoll\n");      
  }

  if (fct_state_futex == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_futex] = (unsigned long)original_futex;
	  kacctime_syscall_table_32[__NR_futex_32] =
		  (unsigned long)original_futex_32;
	  kacctime_log_level(0, "getback : futex\n");      
  }

  if (fct_state_semtimedop == FCT_ACTIF) {
	  kacctime_syscall_table[__NR_semtimedop] =
		  (unsigned long)original_semtimedop;
	  kacctime_log_level(0, "getback : semtimedop\n");      
  }

  write_cr0(cr0);

  while (atomic_read(&kacctime_counter_sleep) > 0) {
	  kacctime_log_level(0, "counter_nano=%d\n",
			     atomic_read(&kacctime_counter_sleep));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_nano=%d\n",
		     atomic_read(&kacctime_counter_sleep));

  while (atomic_read(&kacctime_counter32_sleep) > 0) {
	  kacctime_log_level(0, "counter32_nano=%d\n",
			     atomic_read(&kacctime_counter32_sleep));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_nano32=%d\n",
		     atomic_read(&kacctime_counter32_sleep));
  
  while (atomic_read(&kacctime_counter_select) > 0) {
	  kacctime_log_level(0, "counter_select=%d\n",
			     atomic_read(&kacctime_counter_select));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_select=%d\n",
		     atomic_read(&kacctime_counter_select));
  
  while (atomic_read(&kacctime_counter_poll) > 0) {
	  kacctime_log_level(0, "counter_select=%d\n",
			     atomic_read(&kacctime_counter_poll));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_poll=%d\n",
		     atomic_read(&kacctime_counter_poll));
  
  while (atomic_read(&kacctime_counter_epoll) > 0) {
	  kacctime_log_level(0, "counter_epoll=%d\n",
			     atomic_read(&kacctime_counter_epoll));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_select=%d\n",
		     atomic_read(&kacctime_counter_select));
  
  while (atomic_read(&kacctime_counter32_select) > 0) {
	  kacctime_log_level(0, "counter_select32=%d\n",
			     atomic_read(&kacctime_counter32_select));
	  ssleep(1);
  }
  kacctime_log_level(0, "counter_select32=%d\n",
		     atomic_read(&kacctime_counter32_select));
  
  printk("exit\n");
  return;
}

static void	kacctime_cleanup_proc(void)
{
  remove_proc_entry(KACCTIME_PROCCTL, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCCTL);
  remove_proc_entry(KACCTIME_PROCPRM, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCPRM);
  remove_proc_entry(KACCTIME_PROCADD, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCADD);
  remove_proc_entry(KACCTIME_PROCADD_PRGNAM, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCADD_PRGNAM);
  remove_proc_entry(KACCTIME_PROCLST, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCLST);
  remove_proc_entry(KACCTIME_PROCSTA, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCSTA);
  remove_proc_entry(KACCTIME_PROCUPD, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCUPD);
  remove_proc_entry(KACCTIME_PROCDEL, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCDEL);
  remove_proc_entry(KACCTIME_PROCDEL_PRGNAM, kacctime_procdir);
  kacctime_log_level(0, "suppression de /proc/%s/%s\n", KACCTIME_PROCDIR,
		     KACCTIME_PROCDEL_PRGNAM);
  remove_proc_entry(KACCTIME_PROCDIR, NULL);
  kacctime_log_level(0, "suppression de /proc/%s\n", KACCTIME_PROCDIR);
  return;
}

static void	__exit kacctime_exit(void)
{
  kacctime_cleanup_proc();
  kacctime_get_back_orig_syscall();
  kacctime_log_level(0, "exit\n");
  return;
}

module_init(kacctime_init);
module_exit(kacctime_exit);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <flav@itsgroup.com>");
MODULE_DESCRIPTION("AccTime LKM");
