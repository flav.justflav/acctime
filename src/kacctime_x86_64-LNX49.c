/**
 * kacctime kernel module
 * alter the time perception of user processus
 * 2014-2018 Flavien ASTRAUD <fastraud@itsgroup.fr>
 * 2014-2018 Jean-Philippe ROZÉ <jproze@itsgroup.fr>
 * 2018-     Luc Plisson <lplisson@itsgroup.fr>
 *
 * v0.412 -> adding 'sleep' slicing
 * v0.412 -> adding "starting code disabled" 
 * v0.412 -> logs by levels 
 * v0.4124 -> stats in /proc & logs
 * v0.41245 -> select, alarm, poll, epoll_wait
 * v0.412454 -> adding clock_monolitic & futex
 * v0.412454 -> adding semtimedop
 * v0.4124540 -> acc by process name
 * v0.5 -> sleep bias corretion
 * 2017-06-06 : v0.6 -> 32bit functions impl./corrections
 * 2017-09-25 : v0.65 -> seek_spacetime bug correction
 * 2017-12-11 : v0.7 -> slowing code implementation beginning
 * 2018-04-19 : v.8 -> lnx 4 version beginning 
 */

#include <linux/module.h>
#include <linux/kallsyms.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/utsname.h>
#include <linux/hrtimer.h>
#include <linux/cgroup.h>

MODULE_VERSION("490-2018-05-03 13:49:30-0.8");

static long     kacctime_log_mode = 0;

module_param(kacctime_log_mode, long, 0);
MODULE_PARM_DESC(kacctime_log_mode, "flag for logging");

/**
 * DEFINE
 */
#define KACCTIME_MAX_SPACE      4
#define KACCTIME_MAX_LEN        256
#define KACCTIME_BUF_SIZE       ((KACCTIME_MAX_SPACE+1) * KACCTIME_MAX_LEN)
#define KACCTIME_CODE_FREQ      300

#define KACCTIME_BITS           6

//#define KACCTIME_KERN_PATH      "/boot/System.map-"
#define KACCTIME_KERN_PATH      "/proc/kallsyms"
#define KACCTIME_SYSCALL_SYM    "sys_call_table"
#define KACCTIME_SYSCALL_SYM32  "ia32_sys_call_table"
#define KACCTIME_SYSFS_BASENAME "kts"
#define KACCTIME_SYSFS_BASEDIR	"kts_vec"
#define CHAR_NEW_LINE           '\n'
#define CHAR_SPACE              ' '
#define CHAR_END_STR            '\0'
#define CHAR_EQUAL              '='

#define KACCTIME_FLAV_TIME	"FLAV_TIME="
#define KACCTIME_FLAV_TIME_SIZE	sizeof(KACCTIME_FLAV_TIME)


#define SLEEP_FRAC_CYCLE_DEFAULT	10
#define	SLEEP_FRAC_INTERVAL_DEFAULT	4

static int kacctime_sleep_frac_cycle = SLEEP_FRAC_CYCLE_DEFAULT;
static int kacctime_sleep_frac_interval = SLEEP_FRAC_INTERVAL_DEFAULT;


/*
 * log macro
 */
#define MSG_ARCH                "LNX"
#define INTERNAL_NAME           "kAccTime"              /*Virtual Time*/
#define TMP_BUF_MAX_SIZE        1024

#define kacctime_log_level(...)	kacctime_log_func(__func__, __LINE__,	\
						  current->comm,	\
						  current->pid,		\
						  __VA_ARGS__)

/**
 * kacctime_log_func - log function by level
 * @func: logging function name 
 * @line: logging line 
 * @comm: process name
 * @pid: process ID
 * @level: log : see kacctime_log_mode variable
 * @fmt: format string
 * @...: argument to replace in string 
 *
 * Function parameters are preset by macro : kacctime_log_level.
 */
static __inline__ void kacctime_log_func(const char *func, int line, char *comm,
					 int pid, int level, char *fmt, ...)
{
	va_list ap;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int nb_car;
	
	va_start(ap, fmt);
	if (kacctime_log_mode < level)
		//  if (0 != level) /* POUR LES TESTS */
		return;
	
	nb_car = snprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			  "["MSG_ARCH"/"INTERNAL_NAME"/%s:%d->%s/%d] ",
			  comm, pid, func, line);
	printk(tmp_buf);
	nb_car = vsnprintf(tmp_buf, TMP_BUF_MAX_SIZE,
			   fmt, ap);
	printk(KERN_CONT "%s", tmp_buf);
	return;
}

/*
** macro de calcul du temps

#define NSEC_PER_SEC 1000000000LL
#define USEC_PER_SEC 1000000LL
*/

#define kacctime_timespec_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) + \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) - \
			(fl_op2.tv_sec * NSEC_PER_SEC + fl_op2.tv_nsec); \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_mul(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) * f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_div(fl_op1, f, fl_res)			\
	do {								\
		long long int inter;					\
	  								\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec) / f; \
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timespec_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * NSEC_PER_SEC + fl_op1.tv_nsec)	\
			* n / d;					\
		fl_res.tv_nsec = inter % NSEC_PER_SEC;			\
		fl_res.tv_sec = inter / NSEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_add(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) + \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_sub(fl_op1, fl_op2, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) - \
			(fl_op2.tv_sec * USEC_PER_SEC + fl_op2.tv_usec); \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_mul(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) * f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_div(fl_op1, f, fl_res)				\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec) / f; \
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

#define kacctime_timeval_frac(fl_op1, n, d, fl_res)			\
	do {								\
		long long int inter;					\
      									\
		inter = (fl_op1.tv_sec * USEC_PER_SEC + fl_op1.tv_usec)	\
			* n / d;					\
		fl_res.tv_usec = inter % USEC_PER_SEC;			\
		fl_res.tv_sec = inter / USEC_PER_SEC;			\
	}								\
	while (0)

/*
** fin macro de calcul du temps
*/

long (*original_nanosleep)(const struct timespec *req, struct timespec *rem);
long (*original_clock_gettime)(clockid_t clk_id, struct timespec *tp);

/**
 * syscall tables pointers  
 */
static unsigned long *kacctime_syscall_table = NULL;
static unsigned long *kacctime_syscall_table_32 = NULL;

/**
 * struct kacctime_timespace - définie un espace de temps
 * @multi: facteur d'accélération
 * @divid: facteur de ralentissement 
 * @ref: date (en seconde) de référence à partir de laquelle on applique
 *       le facteur d'accélération.
 * @dec: décalage (en seconde) ajouté au temps après accélération
 * @mono_delta: temps de référence pour le calcul CLOCK_MONOLITIC
 */
struct kacctime_timespace {
	int multi;
	int divid;
	struct timespec	ref;
	struct timespec dec;
	struct timespec	mono_delta;
};

/* Prevert crash exiting module */
static atomic_t  kacctime_counter_sleep = ATOMIC_INIT(0);

struct kacctime_obj {
	struct kobject *kobj;
	struct kacctime_timespace vec[KACCTIME_MAX_SPACE];
};

static struct kobj_attribute kts_attribute[KACCTIME_MAX_SPACE];
static struct attribute *kts_attrs[KACCTIME_MAX_SPACE+1];
static struct attribute_group kts_attr_group;
static struct kacctime_obj kts;

static struct kobject kacctime_module_kobj;

static ssize_t kts_show(struct kobject *kobj, struct kobj_attribute *attr,
                        char *buf)
{
	int idx = 0;
	int ret = 0;
	
        while (idx < KACCTIME_MAX_SPACE) {
		char tmp_buf[sizeof(KACCTIME_SYSFS_BASENAME)+2];

		snprintf(tmp_buf, sizeof(KACCTIME_SYSFS_BASENAME)+2,
			 "%s%d", KACCTIME_SYSFS_BASENAME, idx);
		if (strcmp(attr->attr.name, tmp_buf) == 0) {
			break;
		}
	       idx++;
	}
	ret = sprintf(buf,"space=%d\nmulti=%d\ndivid=%d\nref=%ld\ndec=%ld\n\
mono_delta=[%ld,%ld]\n",
		      idx,
		      kts.vec[idx].multi,
		      kts.vec[idx].divid,
		      kts.vec[idx].ref.tv_sec,
		      kts.vec[idx].dec.tv_sec,
		      kts.vec[idx].mono_delta.tv_sec,
		      kts.vec[idx].mono_delta.tv_nsec);
        return (ret);
}

static __inline__ void kacctime_update_ref_dec(int idx, int new_multi,
					       int new_divid)
{
	int old_multi;
	int old_divid;
	struct timespec old_dec;
	struct timespec old_ref;
	struct timespec tmp;

	old_multi = kts.vec[idx].multi;
	old_divid = kts.vec[idx].divid;
	old_ref.tv_sec = kts.vec[idx].ref.tv_sec;
	old_ref.tv_nsec = kts.vec[idx].ref.tv_nsec;
	old_dec.tv_sec = kts.vec[idx].dec.tv_sec;
	old_dec.tv_nsec = kts.vec[idx].dec.tv_nsec;

	/* QUID de MONO DELTA */

	kts.vec[idx].multi = new_multi;
	kts.vec[idx].divid = new_divid;

	getnstimeofday(&(kts.vec[idx].ref));

	tmp.tv_sec = kts.vec[idx].ref.tv_sec;
	tmp.tv_nsec = kts.vec[idx].ref.tv_nsec;
	
	kacctime_timespec_sub(tmp, old_ref, tmp);
	kacctime_timespec_frac(tmp, old_multi, old_divid, tmp);
	kacctime_timespec_add(tmp, old_ref, tmp);
	kacctime_timespec_add(tmp, old_dec, tmp);

	printk("tmp[%ld / %ld]\n", tmp.tv_sec,
	       tmp.tv_nsec);
	

	kacctime_timespec_sub(tmp, kts.vec[idx].ref, kts.vec[idx].dec);
	printk("dec[%ld / %ld]\n", kts.vec[idx].dec.tv_sec,
	       kts.vec[idx].dec.tv_nsec);
	return;
}

static ssize_t kts_store(struct kobject *kobj, struct kobj_attribute *attr,
                         const char *buf, size_t count)
{
        int idx = 0;
        int ret;
	int multi, divid;
	time_t ref, dec;

        while (idx < KACCTIME_MAX_SPACE) {
		char tmp_kts[sizeof(KACCTIME_SYSFS_BASENAME)+2];

		snprintf(tmp_kts, sizeof(KACCTIME_SYSFS_BASENAME)+2, "%s%d",
			 KACCTIME_SYSFS_BASENAME, idx);
		if (strcmp(attr->attr.name, tmp_kts) == 0)
			break;
		idx++;
        }
	if (idx == KACCTIME_MAX_SPACE)
		return -EIO;
	ret = sscanf(buf, "%d:%d:%ld:%ld", &multi, &divid, &ref, &dec);
	kacctime_log_level(0, "%d->%d:%d:%d:%d\n", ret, multi, divid, ref, dec);
	switch (ret) {
	case 1 :
		kacctime_update_ref_dec(idx, multi, 1);
		break;
	case 2:
		kacctime_update_ref_dec(idx, multi, divid);
		break;
	case 3:
		kts.vec[idx].multi = multi;
		kts.vec[idx].divid = divid;
		kts.vec[idx].ref.tv_sec = ref;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = 0;
		kts.vec[idx].dec.tv_nsec = 0;
		break;
	case 4:
		kts.vec[idx].multi = multi;
		kts.vec[idx].divid = divid;
		kts.vec[idx].ref.tv_sec = ref;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = dec;
		kts.vec[idx].dec.tv_nsec = 0;
		break;
	default:
		kacctime_log_level(0, "error format !");
		return -EIO;
	}
	get_monotonic_boottime(&(kts.vec[idx].mono_delta));
	kacctime_log_level(0, "%d->%d:%d:%d:%d:%d:%d\n", idx,
			   kts.vec[idx].multi,
			   kts.vec[idx].divid,
			   kts.vec[idx].ref.tv_sec,
			   kts.vec[idx].dec.tv_sec,
			   kts.vec[idx].mono_delta.tv_sec,
			   kts.vec[idx].mono_delta.tv_nsec);
	return count;
}

/**
 * kacctime_seek_timespace - cherche l'espace du process en cours
 *
 * Parcours les listes de programmes (/proc/kAccTime/) à accélerer
 * ou
 * Parcours de l'environnement du processus, si la variable FLAV_TIME
 * existe, elle doit contenir le numéro de l'espace (s'il est valide)
 * 
 * Return:
 *   %d (int) l'espace trouvé
 *   -1 si le facteur est négatif ou nul ou l'espace est invalide
 * 
 */
static int kacctime_seek_timespace(void)
{
	char *value;
	char *p;
	char *buf;
	int retval;
	int buf_len;
	char tmp_buf[TMP_BUF_MAX_SIZE];
	int n;

	/*
	  retval = kacctime_search_prog_acc_by_name();
	  if (retval != -1)
	  return (retval);
	*/
	
	n = task_cgroup_path(current, tmp_buf, TMP_BUF_MAX_SIZE-1);
	//	printk(KERN_CONT "cgroup:[%s]\n", tmp_buf);

	
	buf_len = current->mm->env_end - current->mm->env_start;
	buf = kmalloc(buf_len, GFP_KERNEL);
	if (buf == NULL) {
		kacctime_log_level(0, "Mem !\n");
		return (-1);
	}
	retval = copy_from_user(buf, (char *) current->mm->env_start,
				buf_len);
	if (retval != 0) {
		kacctime_log_level(0, "Copy from user !\n");
		kfree(buf);
		return (-1);
	}
	value = p = buf;
	while (p < buf+buf_len) {
		if (*p == CHAR_END_STR) {
			int	space;
			int	space_found;
			
			space_found = strncmp(value, KACCTIME_FLAV_TIME,
					      KACCTIME_FLAV_TIME_SIZE-1);
			if (space_found == 0) {
				while (*value != CHAR_EQUAL)
					value += (unsigned long int) 1;
				value += (unsigned long int) 1;
				if (kstrtoint(value, 10, &space) < 0) {
					kacctime_log_level(0, "Bad space format !\n");
					kfree(buf);
					return (-1);
				}
				if ((space < 0) ||
				    (space >= KACCTIME_MAX_SPACE)) {
					kacctime_log_level(0, "space out of MAX_SPACE (%d) !\n", space);
					kfree(buf);
					return (-1);
				}
				kacctime_log_level(5, "space -> [%d]\n", space);
				kfree(buf);
				if (kts.vec[space].multi <= 0)
					return (-1);
				return (space);
			}
			value = p + (unsigned long int) 1;
		}
		p += (unsigned long int) 1;
	}
	kfree(buf);
	if (kts.vec[0].multi == 0)    
		return (-1);
	return (0);
}

/**
 * time functions
 */

static long kacctime_clock_gettime(clockid_t clk_id, struct timespec *tp)
{
	int err;
	int space;
	long retval;	
	struct timespec ktp;
	struct timespec ktp_sav;
	int multi;
	int divid;
	struct timespec dec;
	struct timespec ref;

	retval = (*original_clock_gettime)(clk_id, tp);
	if (retval != 0)
		goto out;
	
	space = kacctime_seek_timespace();
	/* kacctime_stats_space_tab[space+1].cmpt_clock_gettime++; */
	/* kacctime_add_stats(space, */
	/* 		   KACCTIME_NB_CLOCK_GETTIME, */
	/* 		   kacctime_stats_space_tab[space+1].cmpt_clock_gettime); */
	if (space < 0)
		goto out;

	if (clk_id == CLOCK_MONOTONIC) {
		retval = (*original_clock_gettime)(CLOCK_REALTIME, tp);      
		if (retval != 0)
			goto out;
	}
  
	err = copy_from_user(&ktp, tp, sizeof(*tp));

	ktp_sav.tv_sec = ktp.tv_sec;
	ktp_sav.tv_nsec = ktp.tv_nsec;
	if (err)
		goto err_mem;
	
	if (clk_id > CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n",
				   clk_id, ktp.tv_sec, ktp.tv_nsec);
		goto out;
	}
  
	multi = kts.vec[space].multi;
	divid = kts.vec[space].divid;
	dec = kts.vec[space].dec;
	ref = kts.vec[space].ref;
  
	kacctime_timespec_sub(ktp, ref, ktp);
	kacctime_timespec_frac(ktp, multi, divid, ktp);
	kacctime_timespec_add(ktp, ref, ktp);
	kacctime_timespec_add(ktp, dec, ktp);
	
	kacctime_log_level(3,
			   "ID=%d, [os=%ld ; ons=%ld] [ns=%ld ; nns=%ld]\n",
			   clk_id, ktp_sav.tv_sec, ktp_sav.tv_nsec,
			   ktp.tv_sec, ktp.tv_nsec);
  
	if (clk_id == CLOCK_MONOTONIC) {
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
		kacctime_timespec_sub(ktp,
				      kts.vec[space].mono_delta,
				      ktp);
		kacctime_log_level(1, "CLOCK_%d : (%ld, %ld)\n", clk_id,
				   ktp.tv_sec, ktp.tv_nsec);
	}

	err = copy_to_user(tp, &ktp, sizeof(*tp));
	if (err)
		goto err_mem;
	
 out: 
	return (retval);
	
 err_mem:
	kacctime_log_level(0, "Mem !\n");      
	return (-1);  
}

/**
 * nanosleep -- suppend le proc appelant (voir le man)
 * Si le process est accéléré alors :
 * le temps est divisé par le facteur
 * puis la durée est divisé en intervalle pour pouvoir modifier le
 * rapport durant l'attente
 */
static long kacctime_nanosleep(const struct timespec *rqtp,
			       struct timespec *rmtp)
{
	mm_segment_t oldfs;
	struct timespec tu;
	struct timespec kreq;	
	int space = -1;
	long retval;
	struct timespec t_zero;
	struct timespec t_un;
	int multi;
	int divid;
	int cycle = 1;
	long int duration = 0;

	atomic_inc(&kacctime_counter_sleep);
	space = kacctime_seek_timespace();

	kacctime_log_level(0, "(%d)-%d\n", space,
			   atomic_read(&kacctime_counter_sleep));

	kacctime_log_level(0, "NANOSLEEP (%ld,%ld)\n", rqtp->tv_sec,
			   rqtp->tv_nsec);

	if (space < 0) {
		retval = (*original_nanosleep)(rqtp, rmtp);
		goto out;
	}

	if (copy_from_user(&tu, rqtp, sizeof(tu))) {
		retval = -EFAULT;
		goto out;
	}
	
	if (!timespec_valid(&tu)) {
		retval = -EINVAL;
		goto out;
	}

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_zero);
	if (retval != 0) {
		kacctime_log_level(0, "pb clock_gettime !\n");
		goto out_fs;
	}		

	while (true) {
		multi = kts.vec[space].multi;
		divid = kts.vec[space].divid;

		if (tu.tv_sec <= kacctime_sleep_frac_interval) {
			kacctime_timespec_frac(tu, divid, multi, kreq);
			kacctime_log_level(0,
					   "[%s - (%d) %d] f:%d/%d / ori [%ld,%ld] -> \
new [%ld,%ld]\n",
					   current->comm, current->cred->uid.val,
					   current->pid, multi, divid,
					   tu.tv_sec, tu.tv_nsec,
					   kreq.tv_sec, kreq.tv_nsec);
			retval = (*original_nanosleep)(&kreq, rmtp);
			if (retval < 0)
				goto out_fs;
			break;
		}
		if ((cycle % kacctime_sleep_frac_cycle) == 0) {
			struct timespec d_reel;
			time_t biais;
			
			retval = kacctime_clock_gettime(CLOCK_REALTIME, &t_un);
			if (retval != 0) {
				kacctime_log_level(0, "pb clock_gettime !\n");
				goto out_fs;
			}
			kacctime_timespec_sub(t_un, t_zero, d_reel);
			biais = d_reel.tv_sec - duration;
			duration += biais;
			if ((biais < 0) ||
			    (biais > kacctime_sleep_frac_interval)) {
				kacctime_log_level(1,
						   "biais:(%d)-dur:(%d) / t0 [%ld,%ld] -> t1 [%ld,%ld]\n",
						   biais, duration,
						   t_zero.tv_sec, t_zero.tv_nsec,
						   t_un.tv_sec, t_un.tv_nsec);
			}
			if (tu.tv_sec >= biais)
				tu.tv_sec -= biais;
			else
				tu.tv_sec = 0;
			if (tu.tv_sec <= kacctime_sleep_frac_interval)
				continue;
		}
		kreq.tv_sec = kacctime_sleep_frac_interval;
		kreq.tv_nsec = 0;
		kacctime_timespec_div(kreq, multi, kreq);
		kacctime_timespec_mul(kreq, divid, kreq);
		
		kacctime_log_level(2,"[%s - (%d) %d] f:%d/%d / ori [%ld,%ld]\
 -> new [%ld,%ld]\n",
				   current->comm, current->cred->uid.val,
				   current->pid, multi, divid, tu.tv_sec,
				   0L,
				   kreq.tv_sec, kreq.tv_nsec);
		retval = (*original_nanosleep)(&kreq, rmtp);
		if (retval < 0)
			goto out_fs;
		tu.tv_sec -= kacctime_sleep_frac_interval;
		duration += kacctime_sleep_frac_interval;
		cycle++;
	}
	
	//	ret = (*original_nanosleep)(&tu, rmtp);
	
 out_fs:
	set_fs(oldfs);
	
 out:
	atomic_dec(&kacctime_counter_sleep);
	return (retval);
}

/**
 * replace syscall
 */
static __inline__ void replace_nanosleep(void)
{
	original_nanosleep = (void *)kacctime_syscall_table[__NR_nanosleep];
	kacctime_syscall_table[__NR_nanosleep] =
		(unsigned long) kacctime_nanosleep;
	return;
}

static __inline__ void getback_nanosleep(void)
{
	kacctime_syscall_table[__NR_nanosleep] =
		(unsigned long)original_nanosleep;
	kacctime_log_level(0, "getback : nanosleep\n");
	return;
}

static __inline__ void replace_clock_gettime(void)
{
	original_clock_gettime = (void *)kacctime_syscall_table[__NR_clock_gettime];
	kacctime_syscall_table[__NR_clock_gettime] =
		(unsigned long) kacctime_clock_gettime;
	return;
}

static __inline__ void getback_clock_gettime(void)
{
	kacctime_syscall_table[__NR_clock_gettime] =
		(unsigned long)original_clock_gettime;
	kacctime_log_level(0, "getback : clock_gettime\n");
	return;
}

static void kacctime_getback_syscall(void)
{
	unsigned long cr0;
	
	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	getback_nanosleep();
	getback_clock_gettime();

	write_cr0(cr0);
	return;
}

static void kacctime_replace_syscall(void)
{
	unsigned long	cr0;

	kacctime_log_level(0, "kacctime_replace_syscall\n");

	cr0 = read_cr0();
	write_cr0(cr0 & ~X86_CR0_WP);

	replace_nanosleep();
	replace_clock_gettime();

 	write_cr0(cr0);
	return;
}

/**
 * kacctime_poach_syscall_table - find and overhide syscalls
 */
static int kacctime_poach_syscall_table(void)
{
	kacctime_syscall_table = (unsigned long *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM);
	kacctime_log_level(0, "sym addr : [%pK]\n", kacctime_syscall_table);
	kacctime_syscall_table_32 = (unsigned long *)
		kallsyms_lookup_name(KACCTIME_SYSCALL_SYM32);
	kacctime_log_level(0, "sym addr32 : [%pK]\n", kacctime_syscall_table_32);
	kacctime_replace_syscall();
	return (0);
}

/**
 * takeover syscall
 * NOTE : return codes needs to be changed for consistency 
 */
static int kacctime_init_takeover_syscall(void)
{
	int retval;
	
	kacctime_log_level(0, "init take over\n");
	retval = kacctime_poach_syscall_table();
	if (retval == -1) {
		kacctime_log_level(0, "syscall takeover failed !\n");
		return (retval);
	}
	return (0);
}

static __inline__ void kacctime_init_timespaces(void)
{
	int idx = 0;
	char *tmp_buf;
	int nb_car;

  	while (idx < KACCTIME_MAX_SPACE) {
		kts.vec[idx].multi = 0;
		kts.vec[idx].divid = 1;
		kts.vec[idx].ref.tv_sec = 0;
		kts.vec[idx].ref.tv_nsec = 0;
 		kts.vec[idx].dec.tv_sec = 0;
		kts.vec[idx].dec.tv_nsec = 0;
		kts.vec[idx].mono_delta.tv_sec = 0;
		kts.vec[idx].mono_delta.tv_nsec = 0;
		tmp_buf = kmalloc(5, GFP_KERNEL);
		nb_car = snprintf(tmp_buf, 5, "%s%d", KACCTIME_SYSFS_BASENAME,
				  idx);
		kts_attribute[idx].attr.name = tmp_buf;
		kts_attribute[idx].attr.mode = VERIFY_OCTAL_PERMISSIONS(0664);
		kts_attribute[idx].show = kts_show;
		kts_attribute[idx].store = kts_store;
		kts_attrs[idx] = &(kts_attribute[idx].attr);
		idx++;
  	}
	kts_attrs[idx] = NULL;
	kts_attr_group.attrs = kts_attrs;

	return;
}

static int kacctime_sysfs_init(void)
{
	int retval;
	
	kacctime_init_timespaces();
	kacctime_module_kobj = (((struct module *)(THIS_MODULE))->mkobj).kobj;	
	kts.kobj = kobject_create_and_add(KACCTIME_SYSFS_BASEDIR,
					  &kacctime_module_kobj);
	if (!kts.kobj)
		return -ENOMEM;
	retval = sysfs_create_group(kts.kobj, &kts_attr_group);
	if (retval)
		kobject_put(kts.kobj);

	return retval;
}

static int __init kacctime_init(void)
{
	int ret = 0;
	
	kacctime_log_level(0, "init : %ld\n", kacctime_log_mode);

	ret = kacctime_sysfs_init();
	ret = kacctime_init_takeover_syscall();
	
	return (ret);
}

static void __exit kacctime_exit(void)
{
	kacctime_log_level(0, "exit\n");
	kacctime_getback_syscall();
	return;
}

module_init(kacctime_init);
module_exit(kacctime_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Flavien <flav> ASTRAUD <fastraud@itsgroup.fr>");
MODULE_DESCRIPTION("AccTime LKM");
